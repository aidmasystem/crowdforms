<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

$factory->define(\App\Models\User::class, function (Faker $faker) {
    return [
        'code' => $faker->numerify('AA#####'),
        'first_name' => Str::random(5),
        'last_name' => Str::random(5),
        'first_name_furi' => Str::random(5),
        'last_name_furi' => Str::random(5),
        'email' => $faker->email,
        'password' => Hash::make(Str::random(10)),
        'role' => $faker->randomElement($array = array(1, 2, 3)),
        'type' => 1
    ];
});
