<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCustomerAnswerNullableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_answers', function (Blueprint $table) {
            $table->dropColumn('customer_survey_id');
            $table->integer('customer_id')->nullable()->change();
            $table->text('free_result')->nullable()->change();
            $table->integer('question_package_id')->after('question_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_answers', function (Blueprint $table) {
            $table->integer('customer_survey_id');
            $table->dropColumn('question_package_id');
        });
    }
}
