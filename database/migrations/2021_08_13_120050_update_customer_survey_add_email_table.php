<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCustomerSurveyAddEmailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_survey', function (Blueprint $table) {
            $table->string('full_name')->after('send_type');
            $table->string('email')->after('send_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_survey', function (Blueprint $table) {
            $table->dropColumn(['full_name', 'email']);
        });
    }
}
