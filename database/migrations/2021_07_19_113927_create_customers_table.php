<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('client_id');
            $table->string('full_name');
            $table->string('full_name_furi');
            $table->string('company_name')->nullable();
            $table->string('department_name')->nullable();
            $table->string('position')->nullable();
            $table->string('tel', 20);
            $table->string('email');
            $table->tinyInteger('can_duplicate')->nullable()->comment('1. Duplicate');
            $table->string('post_code', 20);
            $table->string('address');
            $table->text('note')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
