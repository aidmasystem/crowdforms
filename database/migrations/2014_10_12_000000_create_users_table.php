<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 20);
            $table->string('first_name');
            $table->string('last_name');
            $table->string('first_name_furi');
            $table->string('last_name_furi');
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('client_id')->nullable();
            $table->tinyInteger('role')->nullable()->comment('1. 管理者, 2. 一般社員, 3.バイト');
            $table->tinyInteger('type')->default(1)->comment('1. Account AA, 2. Account CE');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
