<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerSurveyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_survey', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('customer_id');
            $table->integer('question_package_id');
            $table->integer('ce_account_id');
            $table->tinyInteger('send_type')->default(1)->comment('1. Email');
            $table->string('title');
            $table->text('message');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_surveies');
    }
}
