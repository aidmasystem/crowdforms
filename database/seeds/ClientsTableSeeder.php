<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->insert([
            'code' => 'CA00001',
            'name' => '株式会社アイドマ・ホールディングス',
            'name_furi' => 'アイドマ・ホールディングス',
            'first_name' => '太郎',
            'last_name' => '田中',
            'first_name_furi' => 'タロウ',
            'last_name_furi' => 'タナカ',
            'email' => 'miura@aidma-hd.jp',
            'tel' => '03-0000-0000',
            'fax' => '03-0000-0001',
            'address' => '123 - 4567 東京都豊島区南池袋1-2-3 〇〇ビル2F',
            'hp' => 'https://www.aidma-hd.jp/',
            'note' => '備考が長い場合備考が長い場合備考が長い場合備考が長い…',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
