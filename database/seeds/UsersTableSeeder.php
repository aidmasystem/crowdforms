<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'code' => 'AA00001',
                'first_name' => '太郎',
                'last_name' => '田中',
                'first_name_furi' => 'タロウ',
                'last_name_furi' => 'タナカ',
                'email' => 'tanaka@aidma-hd.jp',
                'password' => Hash::make('abcd1234'),
                'role' => 1,
                'type' => 1,
                'client_id' => null,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'code' => 'CE00001',
                'first_name' => '大翔',
                'last_name' => '山本',
                'first_name_furi' => 'ヒロト',
                'last_name_furi' => 'やまもと',
                'email' => 'yamamoto@aidma-hd.jp',
                'password' => Hash::make('abcd5678'),
                'role' => 2,
                'type' => 2,
                'client_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ]);
//        factory(\App\Models\User::class, 100)->create();
    }
}
