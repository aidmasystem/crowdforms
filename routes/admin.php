<?php

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::namespace('Admin')->group(function () {
    Route::get('/login', 'Auth\LoginController@login')->name('admin.login');
    Route::post('/check-login', 'Auth\LoginController@checkLogin')->name('admin.checkLogin');
    Route::get('/logout', 'Auth\LoginController@logout')->name('admin.logout');
    Route::get('/forgot-password', 'Auth\LoginController@forgot')->name('admin.forgot_pass');
    Route::post('/recover-pass', 'Auth\LoginController@recoverPass')->name('admin.recover_pass');
    Route::get('/activate', 'Auth\LoginController@updatePass')->name('admin.update_pass');
    Route::post('/reset-new-pass', 'Auth\LoginController@resetPass')->name('admin.reset_pass');
    Route::get('/contact', 'Auth\LoginController@contact')->name('admin.contact');

    Route::group(['middleware' => ['checkLogin:admin', 'checkAdmin'],  'as' => 'admin.'], function () {
        Route::get('/', 'AdminController@index')->name('index');
        Route::get('/create', 'AdminController@create')->name('create');
        Route::post('/store', 'AdminController@store')->name('store');
        Route::get('edit/{id}', 'AdminController@edit')->name('edit');
        Route::put('update/{id}', 'AdminController@update')->name('update');
        Route::delete('destroy/{id}', 'AdminController@destroy')->name('destroy');
        Route::group(['prefix' => 'client'], function () {
            Route::get('/', 'ClientController@index')->name('client.index');
            Route::get('create', 'ClientController@create')->name('client.create');
            Route::post('store', 'ClientController@store')->name('client.store');
            Route::get('edit/{id}', 'ClientController@edit')->name('client.edit');
            Route::put('update/{id}', 'ClientController@update')->name('client.update');
            Route::delete('destroy/{id}', 'ClientController@destroy')->name('client.destroy');

            Route::get('{client_id}/employee', 'EmployeeController@index')->name('employee.index');
            Route::get('{client_id}/employee/create', 'EmployeeController@create')->name('employee.create');
            Route::post('{client_id}/employee/store', 'EmployeeController@store')->name('employee.store');
            Route::get('{client_id}/employee/edit/{id}', 'EmployeeController@edit')->name('employee.edit');
            Route::put('{client_id}/employee/update/{id}', 'EmployeeController@update')->name('employee.update');
            Route::delete('{client_id}/employee/destroy/{id}', 'EmployeeController@destroy')->name('employee.destroy');
            Route::any('{slug}', function()
            {
                return view('errors.404');
            });
        });
        Route::any('{slug}', function()
        {
            return view('errors.404');
        });
    });
});

Route::fallback(function() {
    if (Auth::check() && Auth::user()->type == User::TYPE_ACCOUNT_AA){
        return redirect()->route('admin.index');
    }else {
        return redirect()->route('admin.login');
    }
});
