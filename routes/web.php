<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

//Route::get('/', function () {
//    return view('template/home');
//})->name('home')->middleware('auth:client');
//
//Route::get('/', function () {
//    return view('template/home');
//});
//Route::get('/admin', function () {
//    return view('template/users/index');
//});

// Route::get('/admin/create-user', 'TemplateController@createUser');
//Route::get('/admin/manage', 'TemplateController@manage');
//Route::get('/admin/create-client', 'TemplateController@createClient');
// Route::get('/admin/create-ce', 'TemplateController@createCE');

//Route::get('/login', 'TemplateController@login');
//Route::get('/forgot-password', 'TemplateController@forgot');
//Route::get('/contact', 'TemplateController@contact');
//Route::get('/reset-password', 'TemplateController@resetPwd');

// Route::get('/create-question', 'TemplateController@createQuestion');
// Route::get('/question-list', 'TemplateController@questionList');
// Route::get('/preview', 'TemplateController@preview');
// Route::get('/address-list', 'TemplateController@addressList');
// Route::get('/create-address', 'TemplateController@createAddress');
// Route::get('/confirm-address', 'TemplateController@confirmAddress');
// Route::get('/create-csv', 'TemplateController@createCsv');
// Route::get('/csv-detail', 'TemplateController@csvDetail');
// Route::get('/statistic-list', 'TemplateController@statisticList');
// Route::get('/statistic-detail', 'TemplateController@statisticDetail');
