<?php

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/login', 'Client\Auth\LoginController@login')->name('login');
Route::post('/check-login', 'Client\Auth\LoginController@checkLogin')->name('checkLogin');
Route::get('/logout', 'Client\Auth\LoginController@logout')->name('logout');
Route::get('/forgot-password', 'Client\Auth\LoginController@forgot')->name('forgot_pass');
Route::post('/recover-pass', 'Client\Auth\LoginController@recoverPass')->name('recover_pass');
Route::get('/activate', 'Client\Auth\LoginController@updatePass')->name('update_pass');
Route::post('/reset-new-pass', 'Client\Auth\LoginController@resetPass')->name('reset_pass');
Route::get('/contact', 'Client\Auth\LoginController@contact')->name('contact');
// Show survey of customer
Route::get('/answer-survey', 'Client\SurveyController@surveyOfCustomer')->name('survey.customer');
Route::post('/submit-survey', 'Client\SurveyController@customerSubmit')->name('survey.customerSubmit');
// Log email
Route::post('check-email','Client\CheckEmailController@index')->name('email.checkEmail');

Route::group(['middleware' => ['checkLogin', 'checkClient'], 'as' => 'client.', 'namespace' => 'Client'], function () {
    Route::group(['prefix' => 'survey'], function () {
        Route::get('/', 'SurveyController@index')->name('survey.index');
        Route::get('show/{id}', 'SurveyController@show')->name('survey.show');
        Route::get('create', 'SurveyController@create')->name('survey.create');
        Route::get('form-question', 'SurveyController@formQuestion')->name('survey.formQuestion');
        Route::get('add-form', 'SurveyController@addForm')->name('survey.addForm');
        Route::post('copy-form', 'SurveyController@addFormCopy')->name('survey.copyForm');
        Route::post('store', 'SurveyController@store')->name('survey.store');
        Route::post('preview', 'SurveyController@preview')->name('survey.preview');
        Route::get('edit/{id}', 'SurveyController@edit')->name('survey.edit');
        Route::put('update/{id}', 'SurveyController@update')->name('survey.update');
        Route::delete('destroy/{id}', 'SurveyController@destroy')->name('survey.destroy');
        Route::any('{slug}', function()
        {
            return view('errors.404');
        });
    });
    Route::group(['prefix' => 'customer'], function () {
        Route::get('/', 'CustomerController@index')->name('customer.index');
        Route::get('create', 'CustomerController@create')->name('customer.create');
        Route::post('create_confirm', 'CustomerController@createConfirm')->name('customer.create_confirm');
        Route::post('store', 'CustomerController@store')->name('customer.store');
        Route::get('edit/{id}', 'CustomerController@edit')->name('customer.edit');
        Route::put('update/{id}', 'CustomerController@update')->name('customer.update');
        Route::delete('destroy/{id}', 'CustomerController@destroy')->name('customer.destroy');
        Route::get('/create-csv', 'CustomerController@createCsv')->name('customer.createCsv');
        Route::get('/template-csv', 'CustomerController@templateCsv')->name('customer.templateCsv');
        Route::post('/preview-csv', 'CustomerController@previewFileUpload')->name('customer.previewFileUpload');
        Route::get('/remove-item-csv', 'CustomerController@deleteErrorCsv')->name('customer.deleteErrorCsv');
        Route::post('/submit-csv', 'CustomerController@submitImportCsv')->name('customer.submitImportCsv');
        Route::any('{slug}', function()
        {
            return view('errors.404');
        });
    });
    Route::group(['prefix' => 'send-address'], function () {
        Route::get('/', 'CustomerSurveyController@modalSend')->name('send-address.modalSend');
        Route::get('/get-survey', 'CustomerSurveyController@getSurvey')->name('send-address.getSurvey');
        Route::post('/confirm', 'CustomerSurveyController@modalConfirm')->name('send-address.confirm');
        Route::post('/send-submit', 'CustomerSurveyController@submitSendMail')->name('send-address.submitSendMail');
        Route::get('/resend-email', 'CustomerSurveyController@resendEmail')->name('send-address.resendEmail');
    });
    Route::group(['prefix' => 'summary'], function () {
        Route::get('/modal', 'SummaryController@modalSummary')->name('summary.modalSummary');
        Route::get('/modal-search', 'SummaryController@searchModalSummary')->name('summary.searchModalSummary');
    });
    Route::group(['prefix' => 'employee'], function() {
        Route::get('/', 'EmployeeController@index')->name('employee.index');
        Route::get('create', 'EmployeeController@create')->name('employee.create');
        Route::post('store', 'EmployeeController@store')->name('employee.store');
        Route::get('edit/{id}', 'EmployeeController@edit')->name('employee.edit');
        Route::put('update/{id}', 'EmployeeController@update')->name('employee.update');
        Route::delete('destroy/{id}', 'EmployeeController@destroy')->name('employee.destroy');
        Route::any('{slug}', function()
        {
            return view('errors.404');
        });
    });
    Route::group(['prefix' => 'statistic'], function () {
        Route::get('/','SummaryController@indexStatistic')->name('statistic.index');
        Route::get('detail/{id}', 'SummaryController@detailStatistic')->name('statistic.detail');
        Route::any('{slug}', function()
        {
            return view('errors.404');
        });
    });
});

Route::fallback(function() {
    if (Auth::check() && Auth::user()->type == User::TYPE_ACCOUNT_CE){
        return redirect()->route('client.survey.index');
    }else {
        return redirect()->route('login');
    }
});
