<?php

namespace App\Jobs;

use App\Models\Customer;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportCustomer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    protected $check;

    /**
     * Create a new job instance.
     *
     * @param array $data
     * @param $check
     */
    public function __construct($data, $check)
    {
        $this->data = $data;
        $this->check = $check;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!$this->check) {
            foreach ($this->data as $key => $row) {
                $dataCheckColumn = [];
                $customer = Customer::where(['email' => $row['email'], 'client_id' => $row['client_id']])->first();
                if (empty($customer)) continue;
                else {
                    if ($customer->company_name == '' && $row['company_name'] != '')
                        $dataCheckColumn = array_merge($dataCheckColumn, ['company_name' => $row['company_name']]);
                    if ($customer->department_name == '' && $row['department_name'] != '')
                        $dataCheckColumn = array_merge($dataCheckColumn, ['department_name' => $row['department_name']]);
                    if ($customer->position == '' && $row['position'] != '')
                        $dataCheckColumn = array_merge($dataCheckColumn, ['position' => $row['position']]);
                    if ($customer->note == '' && $row['note'] != '')
                        $dataCheckColumn = array_merge($dataCheckColumn, ['note' => $row['note']]);

                    if (!empty($dataCheckColumn)) {
                        $dataCheckColumn = array_merge($dataCheckColumn, [
                            'updated_at' => Carbon::now(),
                            'updated_by' => $row['created_by'],
                        ]);
                        $customer->update($dataCheckColumn);
                    }
                    unset($this->data[$key]);
                }
            }
        }

        if (!empty($this->data)) {
            foreach (array_chunk($this->data, 500) as $value) {
                Customer::insert($value);
            }
        }
    }
}
