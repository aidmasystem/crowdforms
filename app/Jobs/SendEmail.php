<?php

namespace App\Jobs;

use App\Models\SendTime;
use App\Traits\MailHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, MailHelper;

    protected $content;
    protected $receiveAddress;
    protected $surveyId;
    protected $route;
    protected $subject;
    protected $timeId;
    public $senderAddress;
    public $senderName;

    /**
     * Create a new job instance.
     *
     * @param $content
     * @param $receiveAddress
     * @param $surveyId
     * @param $route
     * @param $subject
     * @param null $senderAddress
     * @param null $senderName
     * @param null $timeId
     */
    public function __construct($content, $receiveAddress, $surveyId, $route, $subject, $senderAddress = null, $senderName = null, $timeId = null)
    {
        $this->content = $content;
        $this->receiveAddress = $receiveAddress;
        $this->surveyId = $surveyId;
        $this->route = $route;
        $this->subject = $subject;
        $this->timeId = $timeId;
        $this->senderAddress = $senderAddress;
        $this->senderName = $senderName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->sendMail($this->content, $this->receiveAddress, $this->surveyId, $this->route, $this->subject, $this->senderAddress, $this->senderName);
        $endSend = SendTime::find($this->timeId);
        if ($endSend != '') {
            $endSend->update(['end_time' => date('Y-m-d H:i:s')]);
        }
    }
}
