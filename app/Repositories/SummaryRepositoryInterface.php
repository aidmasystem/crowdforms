<?php

namespace App\Repositories;

interface SummaryRepositoryInterface
{
    public function listSummaryInModal($params);
}
