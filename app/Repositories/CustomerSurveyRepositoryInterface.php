<?php

namespace App\Repositories;

interface CustomerSurveyRepositoryInterface
{
    public function renderData($id);

    public function submitMail($data);
}
