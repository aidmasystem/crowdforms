<?php

namespace App\Repositories;

interface SurveyRepositoryInterface
{
    public function listSurvey($param);

    public function storeSurvey($request);

    public function updateSurvey($id, array $request);
}
