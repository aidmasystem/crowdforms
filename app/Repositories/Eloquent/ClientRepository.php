<?php

namespace App\Repositories\Eloquent;

use App\Models\Client;
use App\Repositories\ClientRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ClientRepository extends BaseRepository implements ClientRepositoryInterface
{
    public function __construct(Client $_model)
    {
        parent::__construct($_model);
    }

    /**
     * @param $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Pagination\LengthAwarePaginator|object
     */
    public function listClient($params)
    {
        $params = array_merge($params, ['order' => true, 'order_by' => @$params['order_by'] ? @$params['order_by'] : 'code']);
        return $this->get($params, [], function ($q) use ($params) {
            if (@$params['search'] != '') $q->where(function ($qr) use ($params) {
                $qr->orWhere('code', 'LIKE', '%' . $params['search'] . '%')
                    ->orWhere('name', 'LIKE', '%' . $params['search'] . '%')
                    ->orWhere('address', 'LIKE', '%' . $params['search'] . '%')
                    ->orWhere('tel', 'LIKE', '%' . $params['search'] . '%')
                    ->orWhere('hp', 'LIKE', '%' . $params['search'] . '%')
                    ->orWhereRaw("CONCAT(last_name, ' ', first_name) LIKE ?", array('%' . $params['search'] . '%'));
            });

            return $q;
        });
    }

    /**
     *
     * Get the latest code
     *
     * @return string
     */
    public function getLatestCode()
    {
        $code = 'CA00001';
        $client = $this->_model->withTrashed()->select('id', 'code')->orderBy('id', 'DESC')->first();
        if ($client != '') {
            $codeCurrent = $client->code;
            $numberCode = substr($codeCurrent, -5);
            $numberCode = (int)$numberCode + 1;
            $code = 'CA' . sprintf("%'.05d", $numberCode);
        }

        return $code;
    }

    /**
     *
     * Create client CA
     *
     * @param $data
     * @return \Exception
     */
    public function store($data)
    {
        $userId = Auth::user()->id;
        $dataSave = [
            'code' => $data['code'],
            'name' => $data['name'],
            'name_furi' => $data['name_furi'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'first_name_furi' => $data['first_name_furi'],
            'last_name_furi' => $data['last_name_furi'],
            'email' => $data['email'],
            'post_code' => $data['post_code_1'] . $data['post_code_2'],
            'address' => $data['address'],
            'hp' => $data['hp'],
            'note' => $data['note'],
            'created_by' => $userId,
        ];
        if ($data['tel_1'] != '' && $data['tel_2'] != '' && $data['tel_3'] != '') {
            $dataSave['tel'] = $data['tel_1'] . $data['tel_2'] . $data['tel_3'];
        }
        if ($data['fax_1'] != '' && $data['fax_2'] != '' && $data['fax_3'] != '') {
            $dataSave['fax'] = $data['fax_1'] . $data['fax_2'] . $data['fax_3'];
        }

        DB::beginTransaction();
        try {
            if (!empty($dataSave)) $this->create($dataSave);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            return $exception;
        }
    }

    /**
     * @param $id
     * @return array
     */
    public function edit($id)
    {
        $client = $this->_model->findOrFail($id);

        $postCode = $client->post_code;
        if ($postCode != '') {
            $data['post_code_1'] = substr($postCode, 0, 3);
            $data['post_code_2'] = substr($postCode, 3, 4);
        }
        $tel = $client->tel;
        if ($tel != '') {
            $formatTel = formatPhone($tel);
            $exTel = explode('-', $formatTel);
            foreach ($exTel as $key => $row) {
                $data['tel_' . ($key + 1)] = $row;
            }
        }
        $fax = $client->fax;
        if ($fax != '') {
            $formatFax = formatPhone($fax);
            $exFax = explode('-', $formatFax);
            foreach ($exFax as $key => $row) {
                $data['fax_' . ($key + 1)] = $row;
            }
        }
        $data['client'] = $client;

        return $data;
    }

    /**
     * @param $id
     * @param array $data
     * @return bool|mixed|void
     */
    public function updateClient($id, array $data)
    {
        $userId = Auth::user()->id;
        $dataUpdate = [
            'name' => $data['name'],
            'name_furi' => $data['name_furi'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'first_name_furi' => $data['first_name_furi'],
            'last_name_furi' => $data['last_name_furi'],
            'email' => $data['email'],
            'post_code' => $data['post_code_1'] . $data['post_code_2'],
            'address' => $data['address'],
            'hp' => $data['hp'],
            'note' => $data['note'],
            'updated_by' => $userId,
        ];
        if ($data['tel_1'] != '' && $data['tel_2'] != '' && $data['tel_3'] != '') {
            $dataUpdate['tel'] = $data['tel_1'] . $data['tel_2'] . $data['tel_3'];
        } elseif ($data['tel_1'] == null && $data['tel_2'] == null && $data['tel_3'] == null) {
            $dataUpdate['tel'] = null;
        }
        if ($data['fax_1'] != '' && $data['fax_2'] != '' && $data['fax_3'] != '') {
            $dataUpdate['fax'] = $data['fax_1'] . $data['fax_2'] . $data['fax_3'];
        } elseif ($data['fax_1'] == null && $data['fax_2'] == null && $data['fax_3'] == null) {
            $dataUpdate['fax'] = null;
        }

        DB::beginTransaction();
        try {
            if (!empty($dataUpdate)) $this->update($id, $dataUpdate);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            return $exception;
        }
    }
}
