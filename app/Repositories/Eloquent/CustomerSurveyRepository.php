<?php

namespace App\Repositories\Eloquent;

use App\Jobs\SendEmail;
use App\Models\Customer;
use App\Models\CustomerSurvey;
use App\Models\FailedEmail;
use App\Models\SendTime;
use App\Models\Summary;
use App\Models\Survey;
use App\Models\User;
use App\Repositories\CustomerSurveyRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CustomerSurveyRepository extends BaseRepository implements CustomerSurveyRepositoryInterface
{
    public function __construct(CustomerSurvey $_model)
    {
        parent::__construct($_model);
    }

    public function renderData($customerId)
    {
        $user = Auth::user();
        $clientId = $user->client_id;
        $data['id'] = $user->id;
        $data['sendType'] = CustomerSurvey::$sendType;
        $data['survey'] = Survey::query()->where('client_id', $clientId)->pluck('title', 'id')->toArray();
        if (!empty($customerId)) {
            $customer = Customer::whereIn('id', $customerId)->pluck('full_name', 'id')->toArray();
            if (!empty($customer)) {
                $countCustomer = count($customer);
                if ($countCustomer == 1) $data['customerName'] = current($customer);
                else $data['customerName'] = current($customer) . ',他' . ($countCustomer - 1) . '件の送信先';
            }
            $data['customerId'] = json_encode($customerId);
        }

        return $data;
    }

    /**
     * Send mail Customer
     *
     * @param $data
     * @return bool
     */
    public function submitMail($data)
    {
        try {
            DB::beginTransaction();

            $userId = Auth::user()->id;
            $fullName = $data['full_name'];
            $route = route('survey.customer');
            $mailFrom = Config::get('mail.from.address');
            $customerId = $data['customer_id'] != '' ? json_decode($data['customer_id']) : [];
            if (!empty($customerId)) {
                $customerEmail = Customer::whereIn('id', $customerId)->pluck('email', 'id')->toArray();
                $dataEmail = $this->filterValidListEmail($customerEmail);
                if ($dataEmail['successMail'] == 0) return false; // Mail success = 0

                $dataSave = [
                    'customer_id' => $data['customer_id'],
                    'question_package_id' => $data['question_package_id'],
                    'ce_account_id' => $userId,
                    'send_type' => $data['send_type'],
                    'full_name' => $fullName,
                    'email' => $mailFrom,
                    'title' => $data['title'],
                    'message' => $data['message'],
                    'email_fail' => !empty($dataEmail['inValidMail']) ? json_encode($dataEmail['inValidMail']) : null,
                    'created_by' => $userId,
                ];
                $customerSurvey = $this->_model->create($dataSave);

                if ($data['send_type'] == CustomerSurvey::SEND_TYPE_EMAIL) {
                    $time = SendTime::create([
                        'customer_survey_id' => $customerSurvey->id,
                        'total_email' => $dataEmail['successMail'],
                        'start_time' => date('Y-m-d H:i:s'),
                    ]);
                    $sendMail = new SendEmail($data['message'], $dataEmail['validMail'], $data['question_package_id'], $route, $data['title'], $mailFrom, $fullName, $time->id);
                    dispatch($sendMail);
                }

                // summaries
                $allMailSend = $dataEmail['allMail'];
                $successMail = $dataEmail['successMail'];
                $failMail = $allMailSend - $successMail;

                $dataSummary = [
                    'customer_survey_id' => $customerSurvey->id,
                    'question_package_id' => $data['question_package_id'],
                    'send_amount' => $allMailSend,
                    'success_amount' => $successMail,
                    'fail_amount' => $failMail,
                    'created_by' => $userId,
                ];
                Summary::create($dataSummary);

                DB::commit();
            }

            return true;
        } catch (\Exception $e) {
            Log::info('Error!');
            return false;
        }
    }

    public function resendEmail($surveyId)
    {
        try {
            DB::beginTransaction();

            $emailFail = [];
            $route = route('survey.customer');
            $customerSurvey = $this->_model->select('id', 'question_package_id', 'email_fail')
                ->where('question_package_id', $surveyId)->get();
            if ($customerSurvey->isNotEmpty()) {
                foreach ($customerSurvey as $row) {
                    if ($row->email_fail != '') {
                        $arrFail = is_array(json_decode($row->email_fail)) ? json_decode($row->email_fail) : [];
                        $emailFail = array_merge($emailFail, $arrFail);
                    }
                }
            }
            $infoSend = $this->_model->select('id', 'customer_id', 'question_package_id', 'send_type', 'full_name',
                'email', 'title', 'message', 'created_at')->where('question_package_id', $surveyId)
                ->latest()->first();

            if (!empty($emailFail) && $infoSend != '') {
                $dataEmail = $this->filterValidListEmail($emailFail);
                if ($dataEmail['successMail'] == 0)
                    return ['status' => false, 'fail_email' => $dataEmail['inValidMail']];

                if ($infoSend->send_type == CustomerSurvey::SEND_TYPE_EMAIL) {
                    $time = SendTime::create([
                        'total_email' => $dataEmail['successMail'],
                        'start_time' => date('Y-m-d H:i:s'),
                    ]);
                    $sendMail = new SendEmail($infoSend->message, $dataEmail['validMail'], $infoSend->question_package_id, $route, $infoSend->title, $infoSend->email, $infoSend->full_name, $time->id);
                    dispatch($sendMail);

                    $summary = Summary::where('question_package_id', $surveyId)->latest()->first();
                    if ($summary != '') {
                        if (!empty($dataEmail['successMail'])) {
                            $summary->success_amount = $summary->success_amount + count($dataEmail['successMail']);
                            $summary->fail_amount = $summary->fail_amount - count($dataEmail['successMail']);
                            $summary->save();
                        }
                    }
                }
                DB::commit();

                if (!empty($dataEmail['inValidMail']))
                    return ['status' => false, 'fail_email' => $dataEmail['inValidMail']];
                return ['status' => true];
            }

            return ['status' => false];
        } catch (\Exception $e) {
            Log::info('Error!');
            return ['status' => false];
        }
    }

    public function filterValidListEmail($listEmail)
    {
        $data = [];
        $validMail = [];
        $inValidMail = [];
        $allMail = count($listEmail);
        $successMail = 0;
        $listEmailFailed = FailedEmail::query()->pluck('email')->toArray();
        foreach ($listEmail as $mail) {
            if (in_array($mail, $listEmailFailed)) {
                $inValidMail[] = $mail;
                continue;
            }
            if (!checkValidDomainMail($mail)) {
                $inValidMail[] = $mail;
                continue;
            }
            $validMail[] = $mail;
            $successMail++;
        }

        $data = [
            'validMail' => $validMail,
            'inValidMail' => $inValidMail,
            'allMail' => $allMail,
            'successMail' => $successMail,
        ];

        return $data;
    }
}
