<?php

namespace App\Repositories\Eloquent;

use App\Models\Answer;
use App\Models\CustomerAnswer;
use App\Models\EmailReply;
use App\Models\Question;
use App\Models\Summary;
use App\Models\Survey;
use App\Models\User;
use App\Repositories\SurveyRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SurveyRepository extends BaseRepository implements SurveyRepositoryInterface
{
    public function __construct(Survey $_model)
    {
        parent::__construct($_model);
    }

    public function listSurvey($params)
    {
        $survey = $this->_model->select('question_packages.id', 'question_packages.title', 'question_packages.client_id',
            'question_packages.descriptions', 'question_packages.created_at', 'question_packages.updated_at',
            DB::raw("CONCAT(userCe.last_name, ' ', userCe.first_name) AS full_name_create, CONCAT(userUp.last_name, ' ', userUp.first_name) AS full_name_update"))
            ->leftJoin('users as userCe', 'userCe.id', '=', 'question_packages.created_by')
            ->leftJoin('users as userUp', 'userUp.id', '=', 'question_packages.updated_by')
            ->where('question_packages.client_id', self::getIdClient());
        if (@$params['search']['keyword'] != '') $survey->where(function ($qr) use ($params) {
            $qr->orWhere('question_packages.title', 'LIKE', '%' . $params['search']['keyword'] . '%')
                ->orWhere('question_packages.descriptions', 'LIKE', '%' . $params['search']['keyword'] . '%')
                ->orWhere('question_packages.created_at', 'LIKE', '%' . $params['search']['keyword'] . '%')
                ->orWhere('question_packages.updated_at', 'LIKE', '%' . $params['search']['keyword'] . '%')
                ->orWhereRaw("CONCAT(userCe.last_name, ' ', userCe.first_name) LIKE ?", array('%' . $params['search']['keyword'] . '%'))
                ->orWhereRaw("CONCAT(userUp.last_name, ' ', userUp.first_name) LIKE ?", array('%' . $params['search']['keyword'] . '%'));
        });
        if (@$params['search']['from_date'] != '')
            $survey->where('question_packages.created_at', '>', @$params['search']['from_date'] . ' 00:00:00');
        if (@$params['search']['to_date'] != '')
            $survey->where('question_packages.created_at', '<', @$params['search']['to_date'] . ' 23:59:59');
        if (@$params['search']['group_author']) $survey->whereIn('question_packages.created_by', $params['search']['group_author']);
        if (@$params['order_by'] != '' && @$params['order_sort'] != '') {
            if ($params['order_by'] == 'title') $survey->orderBy('question_packages.title', $params['order_sort']);
            if ($params['order_by'] == 'createdAt') $survey->orderBy('question_packages.created_at', $params['order_sort']);
            if ($params['order_by'] == 'updatedAt') $survey->orderBy('question_packages.updated_at', $params['order_sort']);
            if ($params['order_by'] == 'nameCreate') $survey->orderBy('userCe.first_name', $params['order_sort'])->orderBy('question_packages.updated_at', 'desc');
            if ($params['order_by'] == 'nameUpdate') $survey->orderBy('userUp.first_name', $params['order_sort'])->orderBy('question_packages.updated_at', 'desc');
        }
        $survey = $survey->paginate($params['per_page']);

        return $survey;
    }

    /**
     * get author in client
     *
     * @return mixed
     */
    public function listAuthor()
    {
        $author = User::select('id', 'first_name', 'last_name', DB::raw("CONCAT(last_name, ' ', first_name) as full_name"))
            ->where('client_id', self::getIdClient())->get();

        return $author;
    }

    public function getIdClient()
    {
        return Auth::user()->client_id;
    }

    public function show($id)
    {
        $survey = $this->_model->select('id', 'client_id', 'title', 'descriptions')
            ->with('question.answer')
            ->where(['id' => $id])
            ->firstOrFail();

        return $survey;
    }

    /**
     * Create Survey
     *
     * @param $data
     * @return \Exception
     */
    public function storeSurvey($data)
    {
        $userId = Auth::user()->id;
        DB::beginTransaction();

        try {
            $dataAnswer = [];
            $dataSurvey = [
                'client_id' => self::getIdClient(),
                'title' => $data['title'],
                'descriptions' => $data['descriptions'],
                'created_by' => $userId
            ];
            $survey = $this->_model->create($dataSurvey);
            if ($survey != '' && !empty($data['question'])) {
                foreach ($data['question'] as $row) {
                    $dataQuestion = [
                        'question_package_id' => $survey->id,
                        'content' => $row['content'],
                        'type' => $row['type'],
                        'is_obligatory' => @$row['is_obligatory'],
                        'created_by' => $userId,
                    ];
                    $question = Question::create($dataQuestion);
                    if ($question->type == Question::TYPE_ESSAY) continue;
                    if (!empty($row['answer'])) {
                        foreach ($row['answer'] as $item) {
                            if ($item === 'is_other_answer') {
                                $dataAnswer[] = [
                                    'question_id' => $question->id,
                                    'content' => 'その他',
                                    'is_other' => Answer::ANSWER_OTHER,
                                    'created_at' => Carbon::now(),
                                    'created_by' => $userId,
                                ];
                            } else {
                                $dataAnswer[] = [
                                    'question_id' => $question->id,
                                    'content' => $item,
                                    'is_other' => null,
                                    'created_at' => Carbon::now(),
                                    'created_by' => $userId,
                                ];
                            }
                        }
                    }
                }
                $answer = Answer::insert($dataAnswer);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return $e;
        }
    }

    public function copyQuestionOfSurvey($params, $keyQuestion)
    {
        $data = [];
        if (!empty($params) && is_array($params)) {
            foreach ($params as $key => $row) {
                if ($key == $keyQuestion) {
                    $data['content'] = $row['content'];
                    $data['typeQuestion'] = $row['type'];
                    $data['isObligatory'] = @$row['is_obligatory'];
                    if (!empty($row['answer']) && is_array($row['answer'])) {
                        foreach ($row['answer'] as $item) {
                            if ($item == 'is_other_answer') $data['isOther'] = Answer::ANSWER_OTHER;
                            $data['answer'][] = $item;
                        }
                    }
                }
            }
        }

        return $data;
    }

    /**
     * Get value edit survey
     *
     * @param $id
     * @return array
     */
    public function edit($id)
    {
        $data = [];
        $survey = $this->show($id);

        if (!empty($survey)) {
            $data['id'] = $survey->id;
            $data['title'] = $survey->title;
            $data['descriptions'] = $survey->descriptions;
            if ($survey->question->isNotEmpty()) {
                $data['question'] = [];
                foreach ($survey->question as $key => $row) {
                    $data['question'][$key] = [
                        'id' => $row->id,
                        'content' => $row->content,
                        'type' => $row->type,
                        'is_obligatory' => $row->is_obligatory,
                    ];
                    if ($row->answer->isNotEmpty()) {
                        $data['question'][$key]['answer'] = [];
                        foreach ($row->answer as $item) {
                            if (!array_key_exists($item->id, $data['question'][$key]['answer'])) {
                                if ($item->is_other) $data['question'][$key]['is_other'] = $item->is_other;
                                $data['question'][$key]['answer'][] = [
                                    'id' => $item->id,
                                    'content' => $item->content,
                                    'is_other' => $item->is_other,
                                ];
                            }
                        }
                    }
                }
            }
        }

        return $data;
    }

    /**
     * Update survey
     *
     * @param $id
     * @param array $data
     * @return \Exception
     */
    public function updateSurvey($id, array $data)
    {
        $userId = Auth::user()->id;
        DB::beginTransaction();

        try {
            $dataAnswer = [];
            $dataUpdate = [
                'title' => $data['title'],
                'descriptions' => $data['descriptions'],
                'updated_at' => Carbon::now(),
                'updated_by' => $userId
            ];
            $survey = $this->update($id, $dataUpdate);
            Question::where('question_package_id', $id)->update(['updated_by' => $userId]);
            Question::where('question_package_id', $id)->delete();
            if ($survey != '' && !empty($data['question'])) {
                $questionId = [];
                foreach ($data['question'] as $row) {
                    if (@$row['id']) array_push($questionId, $row['id']);
                    $dataQuestion = [
                        'question_package_id' => $survey->id,
                        'content' => $row['content'],
                        'type' => $row['type'],
                        'is_obligatory' => @$row['is_obligatory'],
                        'created_by' => $userId,
                    ];
                    $question = Question::create($dataQuestion);
                    if ($question->type == Question::TYPE_ESSAY) continue;
                    if (!empty($row['answer'])) {
                        foreach ($row['answer'] as $item) {
                            if ($item === 'is_other_answer') {
                                $dataAnswer[] = [
                                    'question_id' => $question->id,
                                    'content' => 'その他',
                                    'is_other' => Answer::ANSWER_OTHER,
                                    'created_at' => Carbon::now(),
                                    'created_by' => $userId,
                                ];
                            } else {
                                $dataAnswer[] = [
                                    'question_id' => $question->id,
                                    'content' => $item,
                                    'is_other' => null,
                                    'created_at' => Carbon::now(),
                                    'created_by' => $userId,
                                ];
                            }
                        }
                    }
                }
                Answer::whereIn('question_id', $questionId)->delete();
                $answer = Answer::insert($dataAnswer);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return $e;
        }
    }

    /**
     * Delete survey
     *
     * @param $id
     * @return \Exception
     */
    public function destroy($id)
    {
        $userId = Auth::user()->id;
        DB::beginTransaction();
        try {
            $survey = $this->findOrFail($id);
            if ($survey != '') {
                $survey->update(['updated_by' => $userId]);
                $survey->delete();

//                Delete question in survey
                $arrQuestion = Question::where('question_package_id', $id)->pluck('id')->toArray();
                Question::where('question_package_id', $id)->update(['updated_by' => $userId]);
                Question::where('question_package_id', $id)->delete();
//                Delete answer
                if (!empty($arrQuestion)) Answer::whereIn('question_id', $arrQuestion)->delete();
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return $e;
        }
    }

    /**
     * @param $params
     * @return \Exception
     */
    public function customerSubmitSurvey($params)
    {
        $data = [];
        DB::beginTransaction();
        try {
            $dataReply = [
                'question_package_id' => $params['question_package_id'],
                'email' => $params['email'],
            ];
            $type = Question::where('question_package_id', $params['question_package_id'])->pluck('type', 'id')->toArray();
            if (!empty($type)) {
                if (!empty($params['question']) && is_array($params['question'])) {
                    foreach ($params['question'] as $key => $row) {
                        $result = null;
                        if ($type[$key] != Question::TYPE_ESSAY) {
                            if ($type[$key] == Question::TYPE_CHECKBOX) $result = json_encode($row);
                            else $result = $row;
                        }
                        $data[] = [
                            'question_id' => $key,
                            'question_package_id' => $params['question_package_id'],
                            'result' => $result,
                            'free_result' => $type[$key] == Question::TYPE_ESSAY ? $row : null,
                            'created_at' => Carbon::now(),
                        ];
                    }
                    if (!empty($data)) {
                        CustomerAnswer::insert($data);
                        EmailReply::create($dataReply);
                        $summary = Summary::where('question_package_id', $params['question_package_id'])->latest()->first();
                        if ($summary) {
                            $reply = $summary->reply_amount;
                            $replyNew = $reply != '' ? ($reply + 1) : 1;
                            $summary->update(['reply_amount' => $replyNew]);
                        }
                        DB::commit();
                    }
                }
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $e;
        }
    }
}
