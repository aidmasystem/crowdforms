<?php

namespace App\Repositories\Eloquent;

use App\Components\Core\Utilities\Helpers;
use App\Repositories\BaseRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;

abstract class BaseRepository implements BaseRepositoryInterface
{
    protected $_model;

    /**
     * RepositoryEloquent constructor.
     */
    public function __construct($_model)
    {
        $this->_model = $_model;
    }

    public function makeSimpleQuery($select)
    {
        return $this->_model->select($select);
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function firstOrCreate(array $maps, array $attributes)
    {
        return $this->_model->firstOrCreate($maps, $attributes);
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->_model->all();
    }

    /**
     * @param array $params
     * @param array $with
     * @param callable $callable
     * @return LengthAwarePaginator|\Illuminate\Contracts\Pagination\LengthAwarePaginator|object
     */
    public function get($params = [], $with = [], callable $callable = null)
    {
        $q = $this->_model->select($params['select'] ? $params['select'] : '*')->with($with);

        if (!Helpers::hasValue($params['order']) || $params['order'] !== 'no') $q->orderBy($params['order_by'] ? $params['order_by'] : 'id', $params['order_sort'] ? $params['order_sort'] : 'desc');

        if (is_callable($callable)) $q = call_user_func_array($callable, [&$q]);

        // if per page is -1, we don't need to paginate at all, but we still return the paginated
        // data structure to our response. Let's just put the biggest number we can imagine.
        if (Helpers::hasValue($params['per_page']) && ($params['per_page'] == -1)) $params['per_page'] = 999999999999;
        if (Helpers::hasValue($params['trashed']) && ($params['trashed'] == 'yes')) $q->withTrashed();

        // if don't want any pagination
        if (Helpers::hasValue($params['paginate']) && ($params['paginate'] == 'no')) {
            if (Helpers::hasValue($params['more']) && $params['more'] == 'no') return $q->first();
            if (Helpers::hasValue($params['limit'])) $q->limit($params['limit']);
            if (Helpers::hasValue($params['offset'])) $q->offset($params['offset']);
            return $q->get();
        }

        return $q->paginate($params['per_page'] ? $params['per_page'] : 10);
    }

    /**
     * @param $mapping
     * @param array $params
     * @return mixed
     */
    public function updateOrCreate($mapping, array $attributes)
    {
        return $this->_model->updateOrCreate($mapping, $attributes);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        $result = $this->_model->find($id);
        return $result;
    }

    /**
     * @param $condition
     * @return mixed
     */
    public function findByCondition($condition)
    {
        $result = $this->_model->where($condition)->first();
        return $result;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findOrFail($id)
    {
        $result = $this->_model->findOrFail($id);
        return $result;
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes)
    {
        return $this->_model->create($attributes);
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function insert(array $attributes)
    {
        return $this->_model->insert($attributes);
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function insertGetId(array $attributes)
    {
        return $this->_model->insertGetId($attributes);
    }

    /**
     * @param $ids
     * @param array $attributes
     * @return mixed
     */
    public function updateInIds($ids, array $attributes)
    {
        return $this->_model->whereIn('id', $ids)->update($attributes);
    }

    /**
     * @param $id
     * @param array $attributes
     * @return bool|mixed
     */
    public function update($id, array $attributes)
    {
        $result = $this->find($id);
        if ($result) {
            $result->update($attributes);
            return $result;
        }
        return false;
    }

    public function updateByCondition(array $condition, array $attributes)
    {
        return $this->_model->where($condition)->update($attributes);
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        $result = $this->find($id);
        if ($result) {
            $isDel = $result->delete();
            return $isDel;
        }

        return false;
    }

    /**
     * @param null $limit
     * @param array $columns
     * @param string $method
     * @return mixed
     */
    public function paginate($limit = null, $columns = ['*'], $method = "paginate")
    {
        $limit = is_null($limit) ? config('repository.pagination.limit', 15) : $limit;
        $results = $this->_model->{$method}($limit, $columns);
        return $results->appends(app('request')->query());
    }

    /**
     * Retrieve all data of repository, simple paginated
     *
     * @param null $limit
     * @param array $columns
     *
     * @return mixed
     */
    public function simplePaginate($limit = null, $columns = ['*'])
    {
        return $this->paginate($limit, $columns, "simplePaginate");
    }

    public function getByParentId($parentKey, $parentId, $select = ['*'])
    {
        return $this->_model->select($select)->where($parentKey, $parentId)->get();
    }

    public function getByFieldLike($field, $value, $select = ['*'])
    {
        return $this->_model->select($select)->where($field, 'LIKE', "%$value%")->get();
    }
}
