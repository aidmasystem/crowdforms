<?php

namespace App\Repositories\Eloquent;

use App\Jobs\ImportCustomer;
use App\Models\Customer;
use App\Repositories\CustomerRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Reader\Csv;

class CustomerRepository extends BaseRepository implements CustomerRepositoryInterface
{
    public function __construct(Customer $_model)
    {
        parent::__construct($_model);
    }

    public function listCustomer($params)
    {
        $params = array_merge($params, ['order_by' => @$params['order_by'] ? @$params['order_by'] : 'updated_at']);
        return $this->get($params, [], function ($q) use ($params) {
            if (@$params['search']['keyword'] != '') $q->where(function ($qr) use ($params) {
                $qr->orWhere('full_name', 'LIKE', '%' . $params['search']['keyword'] . '%')
                    ->orWhere('company_name', 'LIKE', '%' . $params['search']['keyword'] . '%')
                    ->orWhere('department_name', 'LIKE', '%' . $params['search']['keyword'] . '%')
                    ->orWhere('position', 'LIKE', '%' . $params['search']['keyword'] . '%')
                    ->orWhere('created_at', 'LIKE', '%' . $params['search']['keyword'] . '%');
            });
            if (@$params['search']['from_date'] != '') $q->where('created_at', '>', @$params['search']['from_date'] . ' 00:00:00');
            if (@$params['search']['to_date'] != '') $q->where('created_at', '<', @$params['search']['to_date'] . ' 23:59:59');
            $q->where('client_id', self::getIdClient());
            return $q;
        });
    }

    public function listIdCustomer($filter)
    {
        $data = $this->_model->where('client_id', self::getIdClient());
        if (@$filter['keyword'] != '') $data->where(function ($qr) use ($filter) {
            $qr->orWhere('full_name', 'LIKE', '%' . $filter['keyword'] . '%')
                ->orWhere('company_name', 'LIKE', '%' . $filter['keyword'] . '%')
                ->orWhere('department_name', 'LIKE', '%' . $filter['keyword'] . '%')
                ->orWhere('position', 'LIKE', '%' . $filter['keyword'] . '%')
                ->orWhere('created_at', 'LIKE', '%' . $filter['keyword'] . '%');
        });
        if (@$filter['from_date'] != '') $data->where('created_at', '>', @$filter['from_date'] . ' 00:00:00');
        if (@$filter['to_date'] != '') $data->where('created_at', '<', @$filter['to_date'] . ' 23:59:59');
        $data = $data->pluck('id')->toArray();

        return $data;
    }

    public function getIdClient()
    {
        return Auth::user()->client_id;
    }

    public function storeCustomer($data)
    {
        $userId = Auth::user()->id;
        $clientId = self::getIdClient();
        $dataSave = [
            'client_id' => $clientId,
            'full_name' => $data['full_name'],
            'full_name_furi' => $data['full_name_furi'],
            'email' => $data['email'],
            'company_name' => $data['company_name'],
            'department_name' => $data['department_name'],
            'position' => $data['position'],
            'tel' => $data['tel'],
            'post_code' => $data['post_code_1'] . $data['post_code_2'],
            'address' => $data['address'],
            'note' => $data['note'],
            'created_by' => $userId,
            'can_duplicate' => $data['can_duplicate']
        ];

        DB::beginTransaction();
        try {
            if ($dataSave['can_duplicate'] == Customer::CAN_DUPLICATE) {
                $this->create($dataSave);
            } else {
                $mailSaveExist = Customer::where(['email' => $dataSave['email'], 'client_id' => $clientId])->first();
                $dataSaveMailExist = [];
                if ($mailSaveExist == '') {
                    $this->create($dataSave);
                } else {
                    if ($mailSaveExist['company_name'] == '' && $data['company_name'] != '')
                        $dataSaveMailExist = array_merge($dataSaveMailExist, ['company_name' => $data['company_name']]);
                    if ($mailSaveExist['department_name'] == '' && $data['department_name'] != '')
                        $dataSaveMailExist = array_merge($dataSaveMailExist, ['department_name' => $data['department_name']]);
                    if ($mailSaveExist['position'] == '' && $data['position'] != '')
                        $dataSaveMailExist = array_merge($dataSaveMailExist, ['position' => $data['position']]);
                    if ($mailSaveExist['note'] == '' && $data['note'] != '')
                        $dataSaveMailExist = array_merge($dataSaveMailExist, ['note' => $data['note']]);

                    if (!empty($dataSaveMailExist)) {
                        $dataSaveMailExist = array_merge($dataSaveMailExist, [
                            'updated_by' => $userId,
                            'updated_at' => Carbon::now(),
                        ]);
                        $this->update($mailSaveExist['id'], $dataSaveMailExist);
                    }
                }
            }

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            return $exception;
        }
    }

    /**
     * @param $id
     * @return array
     */
    public function edit($id)
    {
        $customer = $this->_model->findOrFail($id);

        $postCode = $customer->post_code;
        if ($postCode != '') {
            $data['post_code_1'] = substr($postCode, 0, 3);
            $data['post_code_2'] = substr($postCode, 3, 4);
        }
        $data['customer'] = $customer;

        return $data;
    }

    /**
     * @param $id
     * @param array $data
     * @return bool|mixed|void
     */
    public function updateCustomer($id, array $data)
    {
        $userId = Auth::user()->id;
        $dataUpdate = [
            'full_name' => $data['full_name'],
            'full_name_furi' => $data['full_name_furi'],
            'email' => $data['email'],
            'company_name' => $data['company_name'],
            'department_name' => $data['department_name'],
            'position' => $data['position'],
            'tel' => $data['tel'],
            'post_code' => $data['post_code_1'] . $data['post_code_2'],
            'address' => $data['address'],
            'note' => $data['note'],
            'updated_at' => Carbon::now(),
            'updated_by' => $userId,
            'can_duplicate' => $data['can_duplicate']
        ];

        DB::beginTransaction();
        try {
            if ($dataUpdate['can_duplicate'] == Customer::CAN_DUPLICATE) {
                $this->update($id, $dataUpdate);
            } else {
                $customer = $this->find($id);
                if ($customer->email == $data['email']) {
                    $this->update($id, $dataUpdate);
                } else {
                    $mailUpdateExist = Customer::where(['email' => $data['email'], 'client_id' => self::getIdClient()])->first();
                    $dataUpdateMailExist = [];
                    if ($mailUpdateExist == '') {
                        $this->update($id, $dataUpdate);
                    } else {
                        if ($mailUpdateExist['company_name'] == '' && $data['company_name'] != '')
                            $dataUpdateMailExist = array_merge($dataUpdateMailExist, ['company_name' => $data['company_name']]);
                        if ($mailUpdateExist['department_name'] == '' && $data['department_name'] != '')
                            $dataUpdateMailExist = array_merge($dataUpdateMailExist, ['department_name' => $data['department_name']]);
                        if ($mailUpdateExist['position'] == '' && $data['position'] != '')
                            $dataUpdateMailExist = array_merge($dataUpdateMailExist, ['position' => $data['position']]);
                        if ($mailUpdateExist['note'] == '' && $data['note'] != '')
                            $dataUpdateMailExist = array_merge($dataUpdateMailExist, ['note' => $data['note']]);

                        if (!empty($dataUpdateMailExist)) {
                            $dataUpdateMailExist = array_merge($dataUpdateMailExist, [
                                'updated_at' => Carbon::now(),
                                'updated_by' => $userId,
                            ]);

                            $this->update($mailUpdateExist['id'], $dataUpdateMailExist);
                            $customer->update(['updated_by' => $userId]);
                            $customer->delete();
                        }
                    }
                }
            }

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            return $exception;
        }
    }

    public function getDataCsv($request)
    {
        $msg = [];
        $files = $request['file_upload'];
        $check = $request['duplicateEmail'];
        $userId = Auth::user()->id;
        $keyStart = [];
        $selectTitle = [];
        if (!empty($request['start'])) {
            foreach ($request['start'] as $row) {
                array_push($keyStart, $row);
            }
        }
        if (!empty($request['selectTitle'])) {
            foreach ($request['selectTitle'] as $row) {
                array_push($selectTitle, $row);
            }
        }
        $data = [];
        try {
            if (!empty($files)) {
                foreach ($files as $key => $file) {
                    $ext = $file->getClientOriginalExtension();
                    $exceptType = "csv";
                    if (strtolower($ext) == $exceptType) {
                        if ($file->getSize() <= 10485760) { // 10Mb
                            $result = $this->dataInCsv($file->getRealPath(), $check, $userId, @$selectTitle[$key], @$keyStart[$key]);
                            if (!empty($result['log'])) {
                                $data[$key]['error'] = $result['error'];
                                $data[$key]['log'] = $result['log'];
                            }
                            $data[$key]['success'] = $result['success'];
                            $data[$key]['data'] = $result['data'];
                            $data[$key]['check'] = $check;
                            $data[$key]['fileName'] = $file->getClientOriginalName();
                        }
                    }
                }
            }

            return $data;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Get data from Csv
     *
     * @param $path
     * @param $check
     * @param $userId
     * @param $selectTitle
     * @param $start
     * @return array[]
     */
    public function dataInCsv($path, $check, $userId, $selectTitle, $start)
    {
        $log = [];
        $data = [];
        $error = [];
        $success = [];
        $kName = $kFuri = $kCompany = $kDepartment = $kPosition = $kTel = $kEmail = $kPostCode = $kAddress = $kNote = 0;

        $reader = new Csv();
        $reader->setInputEncoding('SJIS');
        $reader->setDelimiter(',');
        $reader->setSheetIndex(0);
        $reader->setReadDataOnly(true);
        $sheet = $reader->load($path)->getActiveSheet();
        $highestRow = $sheet->getHighestRow();

        foreach ($selectTitle as $key => $value) {
            if ($value == null) continue;
            if ($value == Customer::COLUMN_FULL_NAME) $kName = $key + 1;
            if ($value == Customer::COLUMN_FULL_NAME_FURI) $kFuri = $key + 1;
            if ($value == Customer::COLUMN_COMPANY_NAME) $kCompany = $key + 1;
            if ($value == Customer::COLUMN_DEPARTMENT_NAME) $kDepartment = $key + 1;
            if ($value == Customer::COLUMN_POSITION) $kPosition = $key + 1;
            if ($value == Customer::COLUMN_TEL) $kTel = $key + 1;
            if ($value == Customer::COLUMN_EMAIL) $kEmail = $key + 1;
            if ($value == Customer::COLUMN_POST_CODE) $kPostCode = $key + 1;
            if ($value == Customer::COLUMN_ADDRESS) $kAddress = $key + 1;
            if ($value == Customer::COLUMN_NOTE) $kNote = $key + 1;
        }

        for ($row = $start ?? 2; $row <= $highestRow; $row++) {
            $fullName = trim($sheet->getCell(Coordinate::stringFromColumnIndex($kName) . $row)->getValue());
            $fullNameFuri = trim($sheet->getCell(Coordinate::stringFromColumnIndex($kFuri) . $row)->getValue());
            $companyName = $kCompany > 0 ? trim($sheet->getCell(Coordinate::stringFromColumnIndex($kCompany) . $row)->getValue()) : '';
            $departmentName = $kDepartment > 0 ? trim($sheet->getCell(Coordinate::stringFromColumnIndex($kDepartment) . $row)->getValue()) : '';
            $position = $kPosition > 0 ? trim($sheet->getCell(Coordinate::stringFromColumnIndex($kPosition) . $row)->getValue()) : '';
            $tel = trim($sheet->getCell(Coordinate::stringFromColumnIndex($kTel) . $row)->getValue());
            $email = trim($sheet->getCell(Coordinate::stringFromColumnIndex($kEmail) . $row)->getValue());
            $postCode = trim($sheet->getCell(Coordinate::stringFromColumnIndex($kPostCode) . $row)->getValue());
            $address = trim($sheet->getCell(Coordinate::stringFromColumnIndex($kAddress) . $row)->getValue());
            $note = $kNote > 0 ? trim($sheet->getCell(Coordinate::stringFromColumnIndex($kNote) . $row)->getValue()) : '';
            $canDuplicate = $check;
            $tel = convertNumberCsv($tel);
            $postCode = convertNumberCsv($postCode);

            if (trim($fullName) == '') $log[$row]['full_name'] = '氏名を入力してください';
            if (trim($fullNameFuri) == '') $log[$row]['full_name_furi'] = 'フリガナを入力してください';
            if (trim($tel) == '') $log[$row]['tel'] = '電話番号を入力してください';
            if (trim($email) == '') $log[$row]['email'] = 'メールアドレスを入力してください';
            if (trim($postCode) == '') $log[$row]['post_code'] = '郵便番号を入力してください';
            if (trim($tel) != '') {
                if (!preg_match("/^(0)[0-9]{9}+$/", $tel)) $log[$row]['tel'] = '電話番号を正しく形式で入力してください';
            }
            if (trim($email) != '' && !filter_var($email, FILTER_VALIDATE_EMAIL))
                $log[$row]['email'] = 'メールアドレスを正しく形式で入力してください';
            if (trim($postCode) != '' && !preg_match("/^[0-9]{7}+$/", $postCode))
                $log[$row]['post_code'] = '郵便番号を正しく形式で入力してください';
            if (trim($address) == '') $log[$row]['address'] = '住所を入力してください';

            if (array_key_exists($row, $log)) {
                $error[$row] = [
                    'full_name' => $fullName,
                    'full_name_furi' => $fullNameFuri,
                    'company_name' => $companyName,
                    'department_name' => $departmentName,
                    'position' => $position,
                    'tel' => $tel,
                    'email' => $email,
                    'post_code' => $postCode,
                    'address' => $address,
                    'note' => $note,
                ];
            } else {
                $success[$row] = [
                    'client_id' => self::getIdClient(),
                    'full_name' => $fullName,
                    'full_name_furi' => $fullNameFuri,
                    'company_name' => $companyName,
                    'department_name' => $departmentName,
                    'position' => $position,
                    'tel' => $tel,
                    'email' => $email,
                    'post_code' => $postCode,
                    'address' => $address,
                    'note' => $note,
                    'can_duplicate' => $canDuplicate == Customer::CAN_DUPLICATE ? Customer::CAN_DUPLICATE : null,
                    'created_by' => $userId,
                ];
            }
            $data[] = [
                'full_name' => $fullName,
                'full_name_furi' => $fullNameFuri,
                'company_name' => $companyName,
                'department_name' => $departmentName,
                'position' => $position,
                'tel' => $tel,
                'email' => $email,
                'post_code' => $postCode,
                'address' => $address,
                'note' => $note,
            ];
        }

        return [
            'log' => $log,
            'error' => $error,
            'success' => $success,
            'data' => $data,
        ];
    }

    /**
     * Get data error csv, update data
     *
     * @param $data
     * @param $params
     * @return mixed
     */
    public function convertDataErrorCsv($data, $params)
    {
        $log = [];
        $error = [];
        $userId = auth()->user()->id;

        foreach ($data as $key => $row) {
            if (is_array($row) && !empty($row)) {
                $log[$key] = [];
                foreach ($row as $k => $item) {
                    if ($item['full_name'] == '') $log[$key][$k]['full_name'] = '氏名を入力してください';
                    if ($item['full_name_furi'] == '') $log[$key][$k]['full_name_furi'] = 'フリガナを入力してください';
                    if ($item['tel'] == '') $log[$key][$k]['tel'] = '電話番号を入力してください';
                    if ($item['email'] == '') $log[$key][$k]['email'] = 'メールアドレスを入力してください';
                    if ($item['post_code'] == '') $log[$key][$k]['post_code'] = '郵便番号を入力してください';
                    if ($item['address'] == '') $log[$key][$k]['address'] = '住所を入力してください';
                    if ($item['tel'] != '') {
                        if (!preg_match("/^(0)[0-9]{9}+$/", $item['tel'])) $log[$key][$k]['tel'] = '電話番号を正しく形式で入力してください';
                    }
                    if ($item['email'] != '' && !filter_var($item['email'], FILTER_VALIDATE_EMAIL))
                        $log[$key][$k]['email'] = 'メールアドレスを正しく形式で入力してください';
                    if ($item['post_code'] != '' && !preg_match("/^[0-9]{7}+$/", $item['post_code']))
                        $log[$key][$k]['post_code'] = '郵便番号を正しく形式で入力してください';

                    if (array_key_exists($k, $log[$key])) {
                        $error[$key][$k] = [
                            'full_name' => $item['full_name'],
                            'full_name_furi' => $item['full_name_furi'],
                            'company_name' => $item['company_name'],
                            'department_name' => $item['department_name'],
                            'position' => $item['position'],
                            'tel' => $item['tel'],
                            'email' => $item['email'],
                            'post_code' => $item['post_code'],
                            'address' => $item['address'],
                            'note' => $item['note'],
                        ];
                    } else {
                        unset($params[$key]['error'][$k]);
                        unset($params[$key]['log'][$k]);
                        $success = [
                            'client_id' => $this->getIdClient(),
                            'full_name' => $item['full_name'],
                            'full_name_furi' => $item['full_name_furi'],
                            'company_name' => $item['company_name'],
                            'department_name' => $item['department_name'],
                            'position' => $item['position'],
                            'tel' => $item['tel'],
                            'email' => $item['email'],
                            'post_code' => $item['post_code'],
                            'address' => $item['address'],
                            'note' => $item['note'],
                            'can_duplicate' => $params[$key]['check'] == Customer::CAN_DUPLICATE ? Customer::CAN_DUPLICATE : null,
                            'created_by' => $userId,
                        ];
                        $params[$key]['success'][$k] = $success;
                    }
                }
            }
        }

        return $params;
    }

    public function importCsv($param)
    {
        $dataSave = [];
        $countImport = 0;
        $check = false;
        if (!empty($param)) {
            foreach ($param as $key => $row) {
                if (!empty($row['success'])) {
                    ksort($row['success']);
                    foreach ($row['success'] as $k => $item) {
                        $item['created_at'] = date('Y-m-d H:i:s');
                        $item['updated_at'] = date('Y-m-d H:i:s');
                        $dataSave[] = $item;
                    }
                }
                if ($row['check']) $check = true;
            }
        }

        DB::beginTransaction();
        try {
            if (!empty($dataSave)) {
                if (!$check) {
                    $emailCus = Customer::where('client_id', self::getIdClient())->pluck('email')->toArray();
                    foreach ($dataSave as $key => $row) {
                        if (!in_array($row['email'], $emailCus)) $countImport++;
                    }
                }

                dispatch(new ImportCustomer($dataSave, $check));
            }
            DB::commit();

            return $countImport;
        } catch (\Exception $exception) {
            Log::info($exception->getMessage());
            DB::rollBack();
            return $exception;
        }
    }
}
