<?php

namespace App\Repositories\Eloquent;

use App\Models\Client;
use App\Models\FailedEmail;
use App\Models\User;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    public function __construct(User $_model)
    {
        parent::__construct($_model);
    }

    /**
     * @param $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Pagination\LengthAwarePaginator|object
     */
    public function listAdmin($params)
    {
        $params = array_merge($params, ['order' => true, 'order_by' => @$params['order_by'] ? @$params['order_by'] : 'code']);
        return $this->get($params, [], function ($q) use ($params) {
            if (@$params['search'] != '') $q->where(function ($qr) use ($params) {
                $qr->orWhere('code', 'LIKE', '%' . $params['search'] . '%')
                    ->orWhere('email', 'LIKE', '%' . $params['search'] . '%')
                    ->orWhereRaw("CONCAT(last_name, ' ', first_name) LIKE ?", array('%' . $params['search'] . '%'))
                    ->orWhereRaw("CONCAT(last_name_furi, ' ', first_name_furi) LIKE ?", array('%' . $params['search'] . '%'));
                if (str_contains(User::$roleAdmin[User::ROLE_AA_MANAGER], $params['search'])) $qr->orWhere('role', User::ROLE_AA_MANAGER);
                if (str_contains(User::$roleAdmin[User::ROLE_AA_STAFF], $params['search'])) $qr->orWhere('role', User::ROLE_AA_STAFF);
                if (str_contains(User::$roleAdmin[User::ROLE_AA_PART], $params['search'])) $qr->orWhere('role', User::ROLE_AA_PART);
            });
            $q->where('type', User::TYPE_ACCOUNT_AA);
            return $q;
        });
    }

    public function listEmployee($params)
    {
        $params = array_merge($params, ['order' => true, 'order_by' => @$params['order_by'] ? @$params['order_by'] : 'code']);
        return $this->get($params, [], function ($q) use ($params) {
            if (@$params['search'] != '') $q->where(function ($qr) use ($params) {
                $qr->orWhere('code', 'LIKE', '%' . $params['search'] . '%')
                    ->orWhere('email', 'LIKE', '%' . $params['search'] . '%')
                    ->orWhereRaw("CONCAT(last_name, ' ', first_name) LIKE ?", array('%' . $params['search'] . '%'))
                    ->orWhereRaw("CONCAT(last_name_furi, ' ', first_name_furi) LIKE ?", array('%' . $params['search'] . '%'));
            });
            $q->where('type', User::TYPE_ACCOUNT_CE)->where('client_id', $params['client_id']);
            return $q;
        });
    }

    public function getLatestCode()
    {
        $code = 'AA00001';
        $admin = $this->_model->withTrashed()->select('id', 'code')->where('type', User::TYPE_ACCOUNT_AA)->orderBy('id', 'DESC')->first();
        if ($admin != '') {
            $codeCurrent = $admin->code;
            $numberCode = substr($codeCurrent, -5);
            $numberCode = (int)$numberCode + 1;
            $code = 'AA' . sprintf("%'.05d", $numberCode);
        }

        return $code;
    }

    public function getCodeEmployee()
    {
        $code = 'CE00001';
        $employee = $this->_model->withTrashed()->select('id', 'code')->where('type', User::TYPE_ACCOUNT_CE)->orderBy('id', 'DESC')->first();
        if ($employee != '') {
            $codeCurrent = $employee->code;
            $numberCode = substr($codeCurrent, -5);
            $numberCode = (int)$numberCode + 1;
            $code = 'CE' . sprintf("%'.05d", $numberCode);
        }

        return $code;
    }

    public function getCodeCaByCe($client_id)
    {
        $code_ca = '';
        $client = Client::findOrFail($client_id);
        if ($client) {
            $code_ca = $client->code;
        }
        return $code_ca;
    }

    /**
     *
     * Create AA
     *
     * @param $data
     * @return \Exception
     */
    public function store($data)
    {
        $userId = Auth::user()->id;
        $dataSave = [
            'code' => $data['code'],
            'password' => Hash::make($data['password_admin']),
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'first_name_furi' => $data['first_name_furi'],
            'last_name_furi' => $data['last_name_furi'],
            'email' => $data['email'],
            'role' => (int)$data['role'],
            'type' => User::TYPE_ACCOUNT_AA,
            'created_by' => $userId,
        ];

        DB::beginTransaction();
        try {
            if (!empty($dataSave)) $this->create($dataSave);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            return $exception;
        }
    }

    /**
     *
     * Create CE
     *
     * @param $data
     * @return \Exception
     */
    public function storeEmployee($data)
    {
        $userId = Auth::user()->id;
        $dataSave = [
            'code' => $data['code'],
            'client_id' => $data['client_id'],
            'password' => Hash::make($data['password_employee']),
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'first_name_furi' => $data['first_name_furi'],
            'last_name_furi' =>$data['last_name_furi'],
            'email' =>$data['email'],
            'type' => User::TYPE_ACCOUNT_CE,
            'created_by' => $userId,
        ];

        DB::beginTransaction();
        try {
            if (!empty($dataSave))
                $this->create($dataSave);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            return $exception;
        }
    }

    /**
     *
     * Edit AA
     *
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $admin = $this->_model->findOrFail($id);
        return $admin;
    }

    /**
     *
     * Edit CE
     *
     * @param $id
     * @return mixed
     */
    public function editEmployee($id)
    {
        $employee = $this->_model->findOrFail($id);
        return $employee;
    }

    /**
     * @param $id
     * @param array $data
     * @return bool|mixed|void
     */
    public function updateAdmin($id, array $data)
    {
        $userId = Auth::user()->id;

        $dataUpdate = [
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'first_name_furi' => $data['first_name_furi'],
            'last_name_furi' => $data['last_name_furi'],
            'email' => $data['email'],
            'role' => (int)$data['role'],
            'type' => User::TYPE_ACCOUNT_AA,
            'updated_by' => $userId,
        ];

        DB::beginTransaction();
        try {
            if (!empty($dataUpdate)) $this->update($id, $dataUpdate);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            return $exception;
        }
    }

    /**
     * @param $id
     * @param array $data
     * @return bool|mixed|void
     */
    public function updateEmployee($id, array $data)
    {
        $userId = Auth::user()->id;

        $dataUpdate = [
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'first_name_furi' => $data['first_name_furi'],
            'last_name_furi' =>$data['last_name_furi'],
            'email' =>$data['email'],
            'type' => User::TYPE_ACCOUNT_CE,
            'updated_by' => $userId,
        ];

        DB::beginTransaction();
        try {
            if (!empty($dataUpdate)) $this->update($id, $dataUpdate);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            return $exception;
        }
    }

    /**
     * Check email valid
     *
     * @param $email
     * @return |null
     */
    public function checkValidEmail($email)
    {
        $checkFail = FailedEmail::query()->where('email', $email)->first();
        $checkDomain = checkValidDomainMail($email);
        if ($checkFail || !$checkDomain) return null;

        return $email;
    }
}
