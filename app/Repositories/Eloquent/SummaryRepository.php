<?php

namespace App\Repositories\Eloquent;

use App\Models\CustomerAnswer;
use App\Models\Question;
use App\Models\Summary;
use App\Models\Survey;
use App\Repositories\SummaryRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SummaryRepository extends BaseRepository implements SummaryRepositoryInterface
{
    public function __construct(Summary $_model)
    {
        parent::__construct($_model);
    }

    public function listSummaryInModal($params)
    {
        $summary = $this->_model->select('summaries.id', 'summaries.updated_at', 'survey.id as survey_id', 'survey.title', 'survey.client_id',
            DB::raw('SUM(summaries.send_amount) as total_send, SUM(summaries.success_amount) as total_success, SUM(summaries.fail_amount) as total_fail'), 'summaries.question_package_id')
            ->join('question_packages as survey', 'summaries.question_package_id', '=', 'survey.id')
            ->where('survey.client_id', auth()->user()->client_id)
            ->groupBy('summaries.question_package_id');
        if (@$params['search'] != '') {
            $summary->where(function ($qr) use ($params) {
                $qr->orWhere('survey.title', 'LIKE', '%' . $params['search'] . '%');
                if (str_contains(Summary::TEXT_SEND_DONE, $params['search'])) $qr->orWhere(DB::raw('summaries.fail_amount'), Summary::SEND_MAIL_DONE);
                if (str_contains(Summary::TEXT_SEND_ERROR, $params['search'])) $qr->orWhere(DB::raw('summaries.fail_amount'), '>', Summary::SEND_MAIL_DONE);
            });
        }
        if (@$params['order_by'] != '' && @$params['order_sort'] != '') {
            if ($params['order_by'] == 'title') $summary->orderBy('survey.title', $params['order_sort']);
            if ($params['order_by'] == 'send') $summary->orderBy('total_fail', $params['order_sort']);
            if ($params['order_by'] == 'total') $summary->orderBy('total_send', $params['order_sort']);
            if ($params['order_by'] == 'updated_at') $summary->orderBy('summaries.updated_at', $params['order_sort']);
        }
        $summary = $summary->get();

        return $summary;
    }

    /**
     * @param $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Pagination\LengthAwarePaginator|object
     */


    public function listStatistic($params)
    {
        $params = array_merge($params, ['order' => true, 'order_by' => @$params['order_by'] ? $params['order_by'] : 'updated_at']);
        $statistic = $this->_model->select('summaries.updated_at', 'survey.id as survey_id', 'survey.title as title',
            'survey.descriptions as detail', 'TotalQuestion.total_question', DB::raw('SUM(summaries.send_amount) as total_send, SUM(summaries.success_amount) as total_success,
            SUM(summaries.fail_amount) as total_fail, SUM(summaries.reply_amount) as total_reply'))
            ->join('question_packages as survey', 'summaries.question_package_id', '=', 'survey.id')
            ->joinSub(Question::select('questions.question_package_id as survey_id', DB::raw('COUNT(questions.id) as total_question'))
                ->whereNull('questions.deleted_at')
                ->groupBy('questions.question_package_id'), 'TotalQuestion',
                function ($join) {
                    $join->on('summaries.question_package_id', '=', 'TotalQuestion.survey_id');
                })
            ->groupBy('summaries.question_package_id')
            ->where('survey.client_id', auth()->user()->client_id);
        if (@$params['search'])
            $statistic->whereIn('summaries.question_package_id', $params['search']);

        if (@$params['order_by'] != '' && @$params['order_sort'] != '') {
            if ($params['order_by'] == 'send') $statistic->orderBy('total_send', $params['order_sort']);
            if ($params['order_by'] == 'success') $statistic->orderBy('total_success', $params['order_sort']);
            if ($params['order_by'] == 'fail') $statistic->orderBy('total_fail', $params['order_sort']);
            if ($params['order_by'] == 'reply') $statistic->orderBy('total_reply', $params['order_sort']);
            if ($params['order_by'] == 'date') $statistic->orderBy('summaries.updated_at', $params['order_sort']);
            if ($params['order_by'] == 'question') $statistic->orderBy('total_question', $params['order_sort']);
        }
        return $statistic;
    }

    public function detailStatistic($question_package)
    {
        return Survey::select('id', 'title', 'descriptions as detail')
            ->where('id', $question_package)
            ->with('question')
            ->first();
    }

    public function getTotalReply($question_package)
    {
        return $this->_model->select('id', 'question_package_id', DB::raw('SUM(reply_amount) as total_reply'))
            ->groupBy('question_package_id')
            ->where('question_package_id', $question_package)
            ->first();
    }

    public function getEssayAnswer($essay)
    {
        $result = CustomerAnswer::select('customer_answers.question_id', 'customer_answers.free_result', 'questions.type')
            ->join('questions', 'customer_answers.question_id', 'questions.id')
            ->where([
                'customer_answers.question_package_id' => $essay['question_package'],
                'questions.type' => Question::TYPE_ESSAY,
            ])
            ->where('customer_answers.free_result', '!=', null);

        return $result->get();
    }
}
