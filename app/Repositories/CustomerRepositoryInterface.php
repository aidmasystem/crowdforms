<?php

namespace App\Repositories;

interface CustomerRepositoryInterface
{
    public function listCustomer($param);

    public function storeCustomer($request);
}
