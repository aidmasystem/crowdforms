<?php

namespace App\Providers;

use App\Repositories\CustomerRepositoryInterface;
use App\Repositories\CustomerSurveyRepositoryInterface;
use App\Repositories\Eloquent\CustomerRepository;
use App\Repositories\Eloquent\CustomerSurveyRepository;
use App\Repositories\Eloquent\SummaryRepository;
use App\Repositories\Eloquent\SurveyRepository;
use App\Repositories\SummaryRepositoryInterface;
use App\Repositories\SurveyRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            SurveyRepositoryInterface::class,
            SurveyRepository::class
        );
        $this->app->singleton(
            CustomerRepositoryInterface::class,
            CustomerRepository::class
        );
        $this->app->singleton(
            CustomerSurveyRepositoryInterface::class,
            CustomerSurveyRepository::class
        );
        $this->app->singleton(
            SummaryRepositoryInterface::class,
            SummaryRepository::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
