<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class RuleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('without_spaces', function ($attr, $value) {
            return preg_match('/^\S*$/u', $value);
        });
        Validator::extend('katakana', function ($attr, $value) {
            return preg_match('/^([\s゠ァアィイゥウェエォオカガキギクグケゲコゴサザシジスズセゼソゾタダチヂッツヅテデトドナニヌネノハバパヒビピフブプヘベペホボポマミムメモャヤュユョヨラリルレロヮワヰヱヲンヴヵヶヷヸヹヺ・ーヽヾヿ]+)$/u', $value);
        });
        Validator::extend('max_uploaded_file_size', function ($attribute, $value, $parameters, $validator) {
            $total_size = array_reduce($value, function ($sum, $item) {
                $sum += filesize($item->path());
                return $sum;
            });

            return $total_size < $parameters[0] * 1024;
        });
    }
}
