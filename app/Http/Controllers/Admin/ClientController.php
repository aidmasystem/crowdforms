<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClientRequest;
use App\Repositories\Eloquent\ClientRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class ClientController extends Controller
{
    private $clientRepository;
    private $dirView = 'admin.clients.';

    /**
     * ClientController constructor.
     *
     * @param ClientRepository $clientRepository
     */
    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keySearch = $request->has('keyword') && $request->keyword != '' ? $request->keyword : '';
        $page = @$request->page ?? 1;
        $sort = @$request->sortOrder ?? 'asc';
        $sortColumn = @$request->column ?? 'code';
        if (@$request->sortOrder == 'all') {
            $sort = 'asc';
            $sortColumn = 'code';
        }
        if (@$request->column == 'fullname') $sortColumn = 'first_name';
        $data['lists'] = $this->clientRepository->listClient([
            'select' => ['id', 'code', 'name', 'name_furi', 'first_name', 'last_name', 'email', 'tel', 'address', 'hp', 'note', 'updated_at'],
            'page' => $page,
            'order_sort' => $sort,
            'order_by' => $sortColumn,
            'search' => $keySearch,
            'per_page' => Config::get('const.ITEMS_PER_PAGE'),
        ]);
        $data['count'] = $data['lists']->total();

        if ($request->ajax()) {
            return response()->json([
                'paginate' => view('paginate')->with('lists', $data['lists'])->render(),
                'data' => view($this->dirView . 'box_table')->with('lists', $data['lists'])->render()
            ]);
        }

        return view($this->dirView . 'index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data['code'] = $this->clientRepository->getLatestCode();

        return view($this->dirView . 'create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ClientRequest $request)
    {
        try {
            $data = $request->all();
            $this->clientRepository->store($data);

            return redirect()->route('admin.client.index')->with('success', 'クライアントの登録が完了しました。');
        } catch (\Exception $exception) {
            return redirect()->route('admin.client.index')->with('error', $exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $data = $this->clientRepository->edit($id);

        return view($this->dirView . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ClientRequest $request, $id)
    {
        try {
            $data = $request->all();
            $this->clientRepository->updateClient($id, $data);

            return redirect()->route('admin.client.index')->with('success', 'クライアントの編集が完了しました。');
        } catch (\Exception $exception) {
            return redirect()->route('admin.client.index')->with('error', $exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $client = $this->clientRepository->findOrFail($id);
            $client->update(['updated_by' => auth()->user()->id]);
            $client->delete();

            return redirect()->route('admin.client.index')->with('success', 'クライアントの削除が完了しました。');
        } catch (\Exception $exception) {
            return redirect()->route('admin.client.index')->with('error', $exception->getMessage());
        }
    }
}
