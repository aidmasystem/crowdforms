<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ForgetPasswordRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Mail\ForgetPassword;
use App\Models\PasswordReset;
use App\Repositories\Eloquent\UserRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->middleware('guest:admin')->except('logout');
        $this->userRepository = $userRepository;
    }

    public function login()
    {
        return view('admin.auth.login');
    }

    public function checkLogin(LoginRequest $request)
    {
        $check = [
            'email' => $request->email,
            'password' => $request->password,
            'type' => User::TYPE_ACCOUNT_AA,
        ];

        if (Auth::attempt($check)) {
            return redirect()->route('admin.index');
        } else {
            return redirect()->route('admin.login')
                ->withErrors(['password' => 'パスワードが間違っています'])
                ->withInput();
        }
    }

    public function forgot()
    {
        return view('admin.auth.forgot_pass');
    }

    public function recoverPass(ForgetPasswordRequest $request)
    {
        $data = $request->all();
        $titleEmail = "【アンケートシステム窓口】パスワード再設定のお知らせ";
        $admin = User::where(['email' => $data['email'], 'type' => User::TYPE_ACCOUNT_AA])->first();
        if (!empty($admin) && $this->userRepository->checkValidEmail($data['email'])) {
            $token = Str::random(32);
            PasswordReset::insert([
                'email' => $data['email'],
                'token' => $token,
                'created_at' => Carbon::now()
            ]);

            $data['title_mail'] = $titleEmail;
            $data['link'] = url('admin/activate?token=' . $token);
            $data['view'] = "admin.auth.forget_pass_notify";
            Mail::to($data['email'])->queue(new ForgetPassword($data));

            return redirect()->back()->with('message', 'メールを成功に送信しました');
        }

        return redirect()->back()->with(['error' => 'メールアドレスに送信できません。再確認してください', 'email' => $data['email']]);
    }

    public function updatePass(Request $request)
    {
        $reset = PasswordReset::where('token', @$request->token)->first();
        if (empty($reset)) return view('admin.auth.success_reset_pw');
        if (time() > strtotime('+1 day', strtotime($reset->created_at))) {
            $data = [
                'title' => 'URLの有効期限が切れました',
            ];
            return view('admin.auth.expired_token', $data);
        }
        $data['email'] = $reset->email;

        return view('admin.auth.new_pass', $data);
    }

    public function resetPass(ResetPasswordRequest $request)
    {
        $data = $request->all();
        $admin = User::where(['email' => $data['email'], 'type' => User::TYPE_ACCOUNT_AA])->first();
        if (!empty($admin)) {
            $admin->password = Hash::make($data['password']);
            $admin->save();
            PasswordReset::where('email', $data['email'])->delete();

            return view('admin.auth.success_reset_pw');
        }

        return redirect()->back();
    }

    public function contact()
    {
        return view('admin.auth.contact');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('admin.login');
    }
}
