<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequest;
use App\Repositories\Eloquent\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class AdminController extends Controller
{
    private $adminRepository;
    private $dirView = 'admin.admins.';

    /**
     * AdminController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->adminRepository = $userRepository;
    }

    public function index(Request $request)
    {
        $keySearch = $request->has('keyword') && $request->keyword != '' ? $request->keyword : '';
        $page = @$request->page ?? 1;
        $sort = @$request->sortOrder ?? 'asc';
        $sortColumn = @$request->column ?? 'code';
        if (@$request->sortOrder == 'all') {
            $sort = 'asc';
            $sortColumn = 'code';
        }
        if (@$request->column == 'fullname') $sortColumn = 'first_name';
        if (@$request->column == 'furiname') $sortColumn = 'first_name_furi';
        $data['lists'] = $this->adminRepository->listAdmin([
            'select' => ['id', 'code', 'first_name', 'last_name', 'first_name_furi', 'last_name_furi', 'email', 'role', 'updated_at'],
            'page' => $page,
            'order_sort' => $sort,
            'order_by' => $sortColumn,
            'search' => $keySearch,
            'per_page' => Config::get('const.ITEMS_PER_PAGE'),
        ]);
        $data['roles'] = Config::get('const.roles');
        $data['count'] = $data['lists']->total();

        if ($request->ajax()) {
            return response()->json([
                'paginate' => view('paginate')->with('lists', $data['lists'])->render(),
                'data' => view($this->dirView . 'box_table', $data)->render()
            ]);
        }

        return view($this->dirView . 'index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data['code'] = $this->adminRepository->getLatestCode();
        $data['roles'] = Config::get('const.roles');

        return view($this->dirView . 'create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminRequest $request)
    {
        try {
            $data = $request->all();
            $this->adminRepository->store($data);
            return redirect()->route('admin.index')->with('success', 'AAアカウントの登録が完了しました。');
        } catch (\Exception $exception) {
            return redirect()->route('admin.index')->with('error', $exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $data['admin'] = $this->adminRepository->edit($id);
        $data['roles'] = Config::get('const.roles');

        return view($this->dirView . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminRequest $request, $id)
    {
        try {
            $data = $request->all();
            $this->adminRepository->updateAdmin($id, $data);
            return redirect()->route('admin.index')->with('success', 'AAアカウントの編集が完了しました。');
        } catch (\Exception $exception) {
            return redirect()->route('admin.index')->with('error', $exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $admin = $this->adminRepository->findOrFail($id);
            $admin->update(['updated_by' => auth()->user()->id]);
            $admin->delete();

            return redirect()->route('admin.index')->with('success', 'AAアカウントの削除が完了しました。');
        } catch (\Exception $exception) {
            return redirect()->route('admin.index')->with('error', $exception->getMessage());
        }
    }
}
