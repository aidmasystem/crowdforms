<?php

namespace App\Http\Controllers;

class TemplateController
{
    public function login()
    {
        return view('template.login');
    }
    public function forgot()
    {
        return view('template.forgot_password');
    }
    public function contact()
    {
        return view('template.contact');
    }
    public function resetPwd()
    {
        return view('template.reset_password');
    }
    public function createUser()
    {
        return view('template.users.create');
    }
    public function manage()
    {
        return view('template.manage.index');
    }
    public function createClient()
    {
        return view('template.manage.create');
    }
    public function createCE()
    {
        return view('template.users.create_ce');
    }
    public function createQuestion()
    {
        return view('template.questions.create');
    }
    public function questionList()
    {
        return view('template.questions.question_list');
    }
    public function preview()
    {
        return view('template.questions.preview');
    }
    public function addressList()
    {
        return view('template.address.index');
    }
    public function createAddress()
    {
        return view('template.address.create');
    }
    public function confirmAddress()
    {
        return view('template.address.confirm');
    }
    public function createCsv()
    {
        return view('template.address.create_csv');
    }
    public function csvDetail()
    {
        return view('template.address.csv_detail');
    }
    public function statisticList()
    {
        return view('template.statistic.index');
    }
    public function statisticDetail()
    {
        return view('template.statistic.detail');
    }
}