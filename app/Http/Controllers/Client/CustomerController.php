<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerCsvRequest;
use App\Http\Requests\CustomerRequest;
use App\Models\Client;
use App\Repositories\CustomerRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class CustomerController extends Controller
{
    private $dirView = 'client.customer.';
    private $customerRepository;

    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        Session::forget('valueCustomer');
        Session::forget('dataCsv');
        $filter = [
            'keyword' => $request->has('keyword') && $request->keyword != '' ? $request->keyword : '',
            'from_date' => $request->has('from_date') && $request->from_date != '' ? date('Y-m-d', strtotime($request->from_date)) : '',
            'to_date' => $request->has('to_date') && $request->to_date != '' ? date('Y-m-d', strtotime($request->to_date)) : '',
        ];
        $page = @$request->page ?? 1;
        $sort = @$request->sortOrder ?? 'desc';
        $sortColumn = @$request->column ?? 'updated_at';
        if (@$request->sortOrder == 'all') {
            $sort = 'desc';
            $sortColumn = 'updated_at';
        }

        $data['lists'] = $this->customerRepository->listCustomer([
            'select' => ['id', 'client_id', 'full_name', 'full_name_furi', 'company_name',
                'department_name', 'position', 'created_at', 'updated_at', 'created_by', 'updated_by'],
            'page' => $page,
            'order_sort' => $sort,
            'order_by' => $sortColumn,
            'search' => $filter,
            'per_page' => Config::get('const.ITEMS_PER_PAGE'),
        ]);
        $data['count'] = $data['lists']->total();
        $data['arrCustomer'] = $this->customerRepository->listIdCustomer($filter);

        if ($request->ajax()) {
            return response()->json([
                'paginate' => view('paginate')->with('lists', $data['lists'])->render(),
                'data' => view($this->dirView . 'box_table')->with('lists', $data['lists'])->render()
            ]);
        }

        return view($this->dirView . 'index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $arrayRequest = Session::get('valueCustomer') ?? [];

        return view($this->dirView . 'create', $arrayRequest);
    }

    public function createConfirm(CustomerRequest $request)
    {
        $data = $request->all();
        Session::put(['valueCustomer' => $data]);

        return view($this->dirView . 'create_confirm', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            $data = $request->all();
            $this->customerRepository->storeCustomer($data);

            return redirect()->route('client.customer.index')->with('success', '送信先の登録が完了しました。');
        } catch (\Exception $exception) {
            return redirect()->route('client.customer.index')->with('error', $exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $data = $this->customerRepository->edit($id);

        return view($this->dirView . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CustomerRequest $request, $id)
    {
        try {
            $data = $request->all();
            $this->customerRepository->updateCustomer($id, $data);

            return redirect()->route('client.customer.index')->with('success', '送信先の編集が完了しました。');
        } catch (\Exception $exception) {
            return redirect()->route('client.customer.index')->with('error', $exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $client = $this->customerRepository->findOrFail($id);
            $client->update(['updated_by' => auth()->user()->id]);
            $client->delete();

            return redirect()->route('client.customer.index')->with('success', '送信先の削除が完了しました。');
        } catch (\Exception $exception) {
            return redirect()->route('client.customer.index')->with('error', $exception->getMessage());
        }
    }

    public function createCsv(Request $request)
    {
        $msgCsv = @$request->cookie('msgCsv');
        return view($this->dirView . 'csv.index')->with('message', $msgCsv);
    }

    public function templateCsv()
    {
        $file = public_path('Import_CSV/Import_customer.csv');
        $clientName = @Client::find(auth()->user()->client_id)->name;
        $outputFile = $clientName . '_' . date('Ymd') . ".csv";
        $header = array(
            'Content-Type: application/csv; charset=Shift-JIS',
            'Content-Encoding: Shift-JIS',
            'Content-Disposition: attachment',
        );
        if (ob_get_contents()) ob_end_clean();

        return response()->download($file, $outputFile, $header);
    }

    public function previewFileUpload(CustomerCsvRequest $request)
    {
        $request = $request->all();
        $data = $this->customerRepository->getDataCsv($request);
        Session::put(['dataCsv' => $data]);

        return view($this->dirView . 'csv.detail')->with(['data' => $data]);
    }

    public function deleteErrorCsv(Request $request)
    {
        $key = $request->key;
        $item = $request->item;
        if ($key === '' && $item == '') return response()->json(['status' => false]);

        $params = Session::get('dataCsv');
        unset($params[$key]['error'][$item]);
        unset($params[$key]['log'][$item]);
        Session::put(['dataCsv' => $params]);

        return response()->json(['status' => true]);
    }

    public function submitImportCsv(Request $request)
    {
        try {
            $data = $request->all();
            $params = Session::get('dataCsv');

            if (!empty($data['customer'])) {
                $params = $this->customerRepository->convertDataErrorCsv($data['customer'], $params);
                Session::put(['dataCsv' => $params]);
            }
            $checkError = false;
            if (!empty($params))
                foreach ($params as $row) {
                    if (!empty($row['error']) || (empty($row['success']) && empty($row['error']))) {
                        $checkError = true;
                        break;
                    }
                }
            if ($checkError) return view($this->dirView . 'csv.error')->with(['data' => $params]);

            $import = $this->customerRepository->importCsv($params);
            Cookie::queue(cookie('msgCsv', '送信先の登録が完了しました。　[新規登録：' . @$import . '件]', 0.1));

            return response()->json(['status' => true, 'route' => route('client.customer.createCsv')]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'ex' => $e->getMessage()]);
        }
    }
}
