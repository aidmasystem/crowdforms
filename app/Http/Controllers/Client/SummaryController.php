<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Repositories\SummaryRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class SummaryController extends Controller
{
    private $summaryRepository;
    private $dirViewSummary = 'client.summary.';
    private $dirViewStatistic = 'client.statistic.';

    public function __construct(SummaryRepositoryInterface $summaryRepository)
    {
        $this->summaryRepository = $summaryRepository;
    }

    /**
     * List summary in modal send
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function modalSummary()
    {
        $data['summary'] = $this->summaryRepository->listSummaryInModal([
            'order_by' => 'updated_at',
            'order_sort' => 'desc',
        ]);

        return view($this->dirViewSummary . 'modal_index', $data);
    }

    public function searchModalSummary(Request $request)
    {
        $request = $request->all();
        $keyWord = @$request['keyword'] != '' ? $request['keyword'] : '';
        if (@$request['sortOrder'] == 'all') {
            $request['sortOrder'] = 'desc';
            $request['column'] = 'updated_at';
        }
        $data['summary'] = $this->summaryRepository->listSummaryInModal([
            'order_sort' => @$request['sortOrder'],
            'order_by' => @$request['column'],
            'search' => $keyWord,
        ]);

        return view($this->dirViewSummary . 'box_body_modal', $data);
    }

    public function indexStatistic(Request $request)
    {
        $search = $request->has('group_survey') && !empty($request->group_survey) ? explode(',', $request->group_survey) : [];
        $page = @$request->page ?? 1;
        $sort = @$request->sortOrder ?? 'desc';
        $sortColumn = @$request->column ?? 'updated_at';
        if (@$request->sortOrder == 'all') {
            $sort = 'desc';
            $sortColumn = 'updated_at';
        }
        $data['statistic'] = $this->summaryRepository->listStatistic([
            'page' => $page,
            'order_sort' => $sort,
            'order_by' => $sortColumn,
            'search' => $search,
        ]);
        //calculate sum header
        $send = 0;
        $success = 0;
        $fail = 0;
        $reply = 0;
        $data['sum'] = [];
        $data['survey'] = [];
        foreach ($data['statistic']->get() as $item) {
            $send += intval($item['total_send']);
            $success += intval($item['total_success']);
            $fail += intval($item['total_fail']);
            $reply += intval($item['total_reply']);
            array_push($data['survey'], [
                'survey_id' => $item['survey_id'],
                'title' => $item['title']
            ]);
        }
        $data['sum'] = array_merge($data['sum'], [
            'send' => $send,
            'success' => $success,
            'fail' => $fail,
            'reply' => $reply,
        ]);
        $data['statistic'] = $data['statistic']->paginate(Config::get('const.ITEMS_PER_PAGE'));
        $data['count'] = $data['statistic']->total();

        if ($request->ajax()) {
            return response()->json([
                'paginate' => view('paginate')->with('lists', $data['statistic'])->render(),
                'html' => view('client.statistic.box_table', $data)->render(),
                'count' => $data['count'],
                'sum' => $data['sum'],
            ]);
        }

        return view($this->dirViewStatistic . 'index', $data);
    }

    public function detailStatistic($question_package)
    {
        $data['survey'] = $this->summaryRepository->detailStatistic($question_package);
        if (!$data['survey']){
            return view('errors.404');
        }
        $data['count'] = $this->summaryRepository->getTotalReply($question_package);
        $essay = [
            'question_package' => $question_package
        ];
        $data['essay'] = $this->summaryRepository->getEssayAnswer($essay);
        return view($this->dirViewStatistic . 'detail', $data);
    }
}
