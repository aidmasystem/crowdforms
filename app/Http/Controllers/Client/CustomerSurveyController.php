<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerSurveyRequest;
use App\Models\Survey;
use App\Repositories\CustomerSurveyRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Str;

class CustomerSurveyController extends Controller
{
    private $customerSurveyRepository;
    private $dirView = 'client.customer_survey.';

    public function __construct(CustomerSurveyRepositoryInterface $customerSurveyRepository)
    {
        $this->customerSurveyRepository = $customerSurveyRepository;
    }

    /**
     * Render modal send mail
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function modalSend(Request $request)
    {
        $arrCustomer = $request->has('arrCustomer') && !empty($request->arrCustomer) ? $request->arrCustomer : [];
        $data = $this->customerSurveyRepository->renderData($arrCustomer);

        return view($this->dirView . 'form_send', $data);
    }

    public function getSurvey(Request $request)
    {
        $data = [];
        $id = $request->has('id') && $request->id != '' ? $request->id : '';
        $url = url('/answer-survey?id=' . base64_encode($id));
        if ($id) $data['url'] = $url;

        return view($this->dirView . 'message_mail', $data);
    }

    /**
     * Preview mail send
     *
     * @param CustomerSurveyRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function modalConfirm(CustomerSurveyRequest $request)
    {
        $data = $request->all();
        if ($data['question_package_id'] != '') {
            $survey = Survey::findOrFail($data['question_package_id']);
            if ($survey != '') $data['survey_name'] = $survey->title;
        }
        $data['message'] = $data['old_message'];

        return view($this->dirView . 'form_confirm', $data);
    }

    /**
     * Submit send email to customer
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function submitSendMail(Request $request)
    {
        $data = $request->all();
        $submitMail = $this->customerSurveyRepository->submitMail($data);
        if ($submitMail) {
            return view($this->dirView . 'success');
        }

        return view($this->dirView . 'error');
    }

    public function resendEmail(Request $request)
    {
        $id = $request->survey_id;
        try {
            $resend = $this->customerSurveyRepository->resendEmail($id);
            if ($resend['status']) return view($this->dirView . 'success');

            return view($this->dirView . 'error')->with('failEmail', @$resend['fail_email']);
        } catch (\Exception $e) {
            return redirect()->route('client.customer.index')->with('error', $e->getMessage());
        }
    }
}
