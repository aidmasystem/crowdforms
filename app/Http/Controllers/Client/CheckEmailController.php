<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\FailedEmail;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CheckEmailController extends Controller
{
    public function index()
    {
        $inputJSON = file_get_contents('php://input');
        error_log($inputJSON);
        $data = json_decode($inputJSON, TRUE);
        if ($data) {
            $dataSave = [];
            $eventError = ['bounce', 'spamreport', 'dropped', 'deferred'];
            foreach ($data as $key => $row) {
                if (in_array($row['event'], $eventError)) {
                    $checkExistsEmail = FailedEmail::query()->where(['email' => $row['email']])->first();
                    if (!empty($checkExistsEmail)) {
                        $checkExistsEmail->update(['time' => date('Y-m-d H:i:s', $row['timestamp'])]);
                    } else {
                        $dataSave[] = [
                            'email' => $row['email'],
                            'type' => $row['event'],
                            'time' => date('Y-m-d H:i:s', $row['timestamp']),
                            'created_at' => Carbon::now(),
                        ];
                    }
                }
            }
            if (!empty($dataSave)) FailedEmail::insert($dataSave);
        }
        return response()->json(['status' => true]);
    }
}
