<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerAnswerRequest;
use App\Http\Requests\SurveyRequest;
use App\Models\EmailReply;
use App\Models\Question;
use App\Repositories\SurveyRepositoryInterface;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;

class SurveyController extends Controller
{
    private $dirView = 'client.survey.';
    private $surveyRepository;

    public function __construct(SurveyRepositoryInterface $surveyRepository)
    {
        $this->surveyRepository = $surveyRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data['authors'] = $this->surveyRepository->listAuthor();
        $filter = [
            'keyword' => $request->has('keyword') && $request->keyword != '' ? $request->keyword : '',
            'from_date' => $request->has('from_date') && $request->from_date != '' ? date('Y-m-d', strtotime($request->from_date)) : '',
            'to_date' => $request->has('to_date') && $request->to_date != '' ? date('Y-m-d', strtotime($request->to_date)) : '',
            'group_author' => $request->has('group_author') && !empty($request->group_author) ? $request->group_author : [],
        ];
        if ($request->ajax()) $filter['group_author'] = !empty($request->group_author) ? json_decode($request->group_author) : [];
        $page = @$request->page ?? 1;
        $sort = @$request->sortOrder ?? 'desc';
        $sortColumn = @$request->column ?? 'updatedAt';
        if (@$request->sortOrder == 'all') {
            $sort = 'desc';
            $sortColumn = 'updatedAt';
        }

        $data['lists'] = $this->surveyRepository->listSurvey([
            'page' => $page,
            'order_sort' => $sort,
            'order_by' => $sortColumn,
            'search' => $filter,
            'per_page' => Config::get('const.ITEMS_PER_PAGE'),
        ]);
        $data['count'] = $data['lists']->total();
        $msgSurvey = @$request->cookie('msgSurvey');
        if ($request->ajax()) {
            return response()->json([
                'paginate' => view('paginate')->with('lists', $data['lists'])->render(),
                'data' => view($this->dirView . 'box_table')->with('lists', $data['lists'])->render()
            ]);
        }

        return view($this->dirView . 'index', $data)->with('message', $msgSurvey);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data['type'] = Question::$typeQuestion;
        return view($this->dirView . 'create', $data);
    }

    /**
     * Add form new question
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function formQuestion(Request $request)
    {
        $number = 1;
        if ($request->has('number') && $request->number != '' && is_numeric($request->number))
            $number = (int)$request->number + 1;
        $data['number'] = $number;
        $data['type'] = Question::$typeQuestion;
        return view($this->dirView . 'question.form_question', $data);
    }

    /**
     * Add form answer in question
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addForm(Request $request)
    {
        if ($request->has('type') && $request->type != '') {
            $type = $request->type;
            $data['key'] = $request->has('key') && $request->key != '' ? $request->key : 0;
            if ($type == Question::TYPE_RADIO) {
                return view($this->dirView . 'question.form_radio', $data);
            } elseif ($type == Question::TYPE_CHECKBOX) {
                return view($this->dirView . 'question.form_checkbox', $data);
            } elseif ($type == Question::TYPE_ESSAY) {
                return view($this->dirView . 'question.form_essay', $data);
            } elseif ($type == Question::TYPE_SELECT_BOX) {
                return view($this->dirView . 'question.form_select', $data);
            }
        }
    }

    /**
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addFormCopy(Request $request)
    {
        $data = $request->all();
        $number = 1;
        if (@$data['number'] != '') $number = (int)$data['number'] + 1;
        $dataCurrent = $this->surveyRepository->copyQuestionOfSurvey($data['question'], $data['keyCurrent']);
        $dataCurrent['key'] = $number;
        $dataCurrent['type'] = Question::$typeQuestion;

        return view($this->dirView . 'question.form_copy', $dataCurrent);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SurveyRequest $request)
    {
        $data = $request->all();
        $this->surveyRepository->storeSurvey($data);
        Cookie::queue(cookie('msgSurvey', 'アンケートの登録が完了しました。', 0.1));

        return response()->json(['status' => true, 'route' => route('client.survey.index')]);
    }

    /**
     * Preview create anketo
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function preview(SurveyRequest $request)
    {
        $data = $request->all();

        return view($this->dirView . 'preview', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $data['survey'] = $this->surveyRepository->show($id);

        return view($this->dirView . 'show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $data['survey'] = $this->surveyRepository->edit($id);

        $data['type'] = Question::$typeQuestion;
        $data['countQuestion'] = !empty($data['survey']['question']) ? count($data['survey']['question']) : 0;

        return view($this->dirView . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SurveyRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(SurveyRequest $request, $id)
    {
        $data = $request->all();
        $this->surveyRepository->updateSurvey($id, $data);
        Cookie::queue(cookie('msgSurvey', 'アンケートの編集が完了しました。', 0.1));

        return response()->json(['status' => true, 'route' => route('client.survey.index')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $this->surveyRepository->destroy($id);

        return redirect()->route('client.survey.index')->with('success', 'アンケートの削除が完了しました。');
    }

    public function surveyOfCustomer(Request $request)
    {
        try {
            $data = [];
            $token = $request->has('token') && $request->token != '' ? Crypt::decrypt($request->token) : [];
            $id = $request->has('id') && $request->id != '' ? base64_decode($request->id) : '';
            if ($id != '') {
                $data['preview'] = true;
                $data['survey'] = $this->surveyRepository->edit($id);
                return view('client.customer_survey', $data);
            }
            if (empty($token)) {
                return view('client.customer_survey')->with('success', 'アンケートの回答が完了しました。ありがとうございました。');
            }
            if ($token['email'] != '' && $token['survey_id'] != '') {
                $checkEmail = EmailReply::where(['email' => $token['email'], 'question_package_id' => $token['survey_id']])->first();
                if ($checkEmail != '') {
                    return view('client.customer_survey')->with('success', 'アンケートの回答が完了しました。ありがとうございました。');
                }
                $data['preview'] = false;
                $data['email'] = $token['email'];
                $data['survey'] = $this->surveyRepository->edit($token['survey_id']);
            }

            return view('client.customer_survey', $data);
        } catch (DecryptException $e) {
            return view('client.customer_survey')->with('error', 'アンケートを回答する」のURLをもう一度確認してください。');
        }
    }

    public function customerSubmit(CustomerAnswerRequest $request)
    {
        try {
            $params = $request->all();
            $this->surveyRepository->customerSubmitSurvey($params);

            return redirect()->route('survey.customer')->with('success', 'アンケートの回答が完了しました。ありがとうございました。');
        } catch (\Exception $exception) {
            return redirect()->route('survey.customer')->with('error', $exception->getMessage());
        }
    }
}
