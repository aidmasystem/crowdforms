<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeRequest;
use App\Repositories\Eloquent\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class EmployeeController extends Controller
{
    private $employeeRepository;
    private $dirView = 'client.employees.';

    /**
     * AdminController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->employeeRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $client_id = auth()->user()->client_id;
        $keySearch = $request->has('keyword') && $request->keyword != '' ? $request->keyword : '';
        $page = @$request->page ?? 1;
        $sort = @$request->sortOrder ?? 'asc';
        $sortColumn = @$request->column ?? 'code';
        if (@$request->sortOrder == 'all') {
            $sort = 'asc';
            $sortColumn = 'code';
        }
        if (@$request->column == 'fullname') $sortColumn = 'first_name';
        if (@$request->column == 'furiname') $sortColumn = 'first_name_furi';
        $data['lists'] = $this->employeeRepository->listEmployee([
            'select' => ['id', 'code', 'client_id', 'first_name', 'last_name', 'first_name_furi', 'last_name_furi', 'email', 'updated_at'],
            'page' => $page,
            'order_sort' => $sort,
            'order_by' => $sortColumn,
            'search' => $keySearch,
            'per_page' => Config::get('const.ITEMS_PER_PAGE'),
            'client_id' => $client_id
        ]);
        $data['count'] = $data['lists']->total();

        if ($request->ajax()) {
            return response()->json([
                'paginate' => view('paginate')->with('lists', $data['lists'])->render(),
                'data' => view($this->dirView . 'box_table')->with('lists', $data['lists'])->render()
            ]);
        }

        return view($this->dirView . 'index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $client_id = auth()->user()->client_id;
        $data['code'] = $this->employeeRepository->getCodeEmployee();
        $data['code_ca'] = $this->employeeRepository->getCodeCaByCe($client_id);
        $data['client_id'] = $client_id;

        return view($this->dirView . 'create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(EmployeeRequest $request)
    {
        try {
            $data = $request->all();
            $this->employeeRepository->storeEmployee($data);
            return redirect()->route('client.employee.index')->with('success', 'CEアカウントの登録が完了しました。');
        } catch (\Exception $exception) {
            return redirect()->route('client.employee.index')->with('error', $exception->getMessage());
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $client_id = auth()->user()->client_id;
        $data['employee'] = $this->employeeRepository->editEmployee($id);
        $data['code_ca'] = $this->employeeRepository->getCodeCaByCe($client_id);
        $data['client_id'] = $client_id;

        return view($this->dirView . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EmployeeRequest $request, $id)
    {
        try {
            $data = $request->all();
            $this->employeeRepository->updateEmployee($id, $data);
            return redirect()->route('client.employee.index')->with('success', 'CEアカウントの編集が完了しました。');
        } catch (\Exception $exception) {
            return redirect()->route('client.employee.index')->with('error', $exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $employee = $this->employeeRepository->findOrFail($id);
            $employee->update(['updated_by' => auth()->user()->id]);
            $employee->delete();

            return redirect()->route('client.employee.index')->with('success', 'CEアカウントの削除が完了しました。');
        } catch (\Exception $exception) {
            return redirect()->route('client.employee.index')->with('error', $exception->getMessage());
        }
    }
}
