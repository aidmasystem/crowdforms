<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIsLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guest() && ($request->is('admin') || $request->is('admin/*'))) {
            return redirect()->route('admin.login');
        }
        if (Auth::guest()) {
            return redirect()->route('login');
        }

        return $next($request);
    }
}
