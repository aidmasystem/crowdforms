<?php

namespace App\Http\Requests;

use App\Models\Customer;
use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function all($keys = NULL)
    {
        $input = parent::all();
        $input['full_name'] = str_replace('　', '', $input['full_name']);
        $input['full_name_furi'] = str_replace('　', '', $input['full_name_furi']);
        $input['email'] = str_replace('　', '', $input['email']);
        $input['company_name'] = str_replace('　', '', $input['company_name']);
        $input['department_name'] = str_replace('　', '', $input['department_name']);
        $input['position'] = str_replace('　', '', $input['position']);
        $input['tel'] = str_replace('　', '', $input['tel']);
        $input['post_code_1'] = str_replace('　', '', $input['post_code_1']);
        $input['post_code_2'] = str_replace('　', '', $input['post_code_2']);
        $input['address'] = str_replace('　', '', $input['address']);
        $input['note'] = str_replace('　', '', $input['note']);
        return $input;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'full_name' => 'required',
            'full_name_furi' => 'required|max:255|katakana',
            'post_code_1' => 'required|digits:3',
            'post_code_2' => 'required|digits:4',
            'address' => 'required',
            'tel' => 'required|digits_between:10,11|regex:/^(0)[0-9]{9}/',
            'email' => 'required|email',
        ];

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'full_name.required' => '氏名が未入力です',
            'full_name_furi.required' => 'フリガナが未入力です',
            'full_name_furi.katakana' => 'カタカナの正しい形式で入力してください',
            'tel.required' => '電話番号が未入力です',
            'tel.digits_between' => '電話番号を正しく形式で入力してください (0xxxxxxxxx|0xxxxxxxxxx)',
            'tel.regex' => '電話番号を正しく形式で入力してください (0xxxxxxxxx|0xxxxxxxxxx)',
            'address.required' => '住所が未入力です',
            'email.required' => 'メールアドレスが未入力です',
            'email.email' => 'メールアドレスを正しく形式で入力してください',
        ];
    }
}
