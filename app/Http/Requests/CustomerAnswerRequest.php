<?php

namespace App\Http\Requests;

use App\Models\Question;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CustomerAnswerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function all($keys = NULL)
    {
        $input = parent::all();
        foreach ($input['type'] as $k => $row) {
            if ($row == Question::TYPE_ESSAY) $input['question'][$k] = str_replace('　', ' ', $input['question'][$k]);
        }

        return $input;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param Request $request
     * @return array
     */
    public function rules(Request $request)
    {
        $data = $request->all();
        $rules = [];
        if (!empty($data['obligatory']) && is_array($data['obligatory'])) {
            foreach ($data['obligatory'] as $key => $row) {
                if (!$row) continue;
                $rules = array_merge($rules, ['question.' . $key => 'required']);
            }
        }

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'question.*.required' => '回答を記入してください',
        ];
    }
}
