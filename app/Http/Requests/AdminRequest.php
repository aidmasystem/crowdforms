<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function all($keys = NULL)
    {
        $input = parent::all();
        $input['first_name'] = str_replace('　', '', $input['first_name']);
        $input['last_name'] = str_replace('　', '', $input['last_name']);
        $input['first_name_furi'] = str_replace('　', '', $input['first_name_furi']);
        $input['last_name_furi'] = str_replace('　', '', $input['last_name_furi']);
        $input['email'] = str_replace('　', '', $input['email']);

        return $input;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'code' => 'required|unique:users,code',
            'password_admin' => 'required|min:8|max:255',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'first_name_furi' => 'required|max:255|katakana',
            'last_name_furi' => 'required|max:255|katakana',
            'email' => 'required|email|max:255|unique:users,email,NULL,id,deleted_at,NULL,type,' . User::TYPE_ACCOUNT_AA,
            'role' => 'required'
        ];
        if ($this->id != null) {
            $rules = array_merge($rules, [
                'code' => 'required|unique:users,code,' . $this->id,
                'email' => 'required|email|max:255|unique:users,email,' . $this->id . ',id,deleted_at,NULL,type,' . User::TYPE_ACCOUNT_AA,
                'password_admin' => 'nullable',
            ]);
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'first_name' => '氏名',
            'last_name' => '氏名',
            'first_name_furi' => 'フリガナ',
            'last_name_furi' => 'フリガナ',
            'email' => 'メールアドレス',
            'password_admin' => 'パスワード'
        ];
    }

    public function messages()
    {
        return [
            'code.unique' => 'このアカウントIDが既存です。新しいIDが生成されましたのでもう一回登録ボタントクリックしてください。',
            'password_admin.required' => 'パスワードを入力してください',
            'password_admin.min' => '※8文字以上、半角英数字',
            'first_name.required' => '氏名を入力してください',
            'last_name.required' => '氏名を入力してください',
            'first_name_furi.required' => 'フリガナを入力してください',
            'last_name_furi.required' => 'フリガナを入力してください',
            'katakana' => 'カタカナの正しい形式で入力してください',
            'email.required' => 'メールアドレスを入力してください',
            'email.unique' => 'このメールアドレスは既に登録されています。別のメールアドレスを登録してください。',
            'email.email' => 'メールアドレスを正しく形式で入力してください',
            'role.required' => '権限を選択してください',
            'max' => ':attributeは:max文字以下にしてください',
        ];
    }
}
