<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function all($keys = NULL)
    {
        $input = parent::all();
        $input['first_name'] = str_replace('　', '', $input['first_name']);
        $input['last_name'] = str_replace('　', '', $input['last_name']);
        $input['first_name_furi'] = str_replace('　', '', $input['first_name_furi']);
        $input['last_name_furi'] = str_replace('　', '', $input['last_name_furi']);
        $input['email'] = str_replace('　', '', $input['email']);

        return $input;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'code' => 'required|unique:users,code',
            'password_employee' => 'required|min:8|max:255',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'first_name_furi' => 'required|max:255|katakana',
            'last_name_furi' => 'required|max:255|katakana',
            'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL,type,' . User::TYPE_ACCOUNT_CE,
        ];
        if ($this->id != null) {
            $rules = array_merge($rules, [
                'code' => 'required|unique:users,code,' . $this->id,
                'email' => 'required|email|unique:users,email,' . $this->id . ',id,deleted_at,NULL,type,' . User::TYPE_ACCOUNT_CE,
                'password_employee' => 'nullable',
            ]);
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'first_name' => '氏名',
            'last_name' => '氏名',
            'first_name_furi' => 'フリガナ',
            'last_name_furi' => 'フリガナ',
            'email' => 'メールアドレス',
            'password_employee' => 'パスワード'
        ];
    }

    public function messages()
    {
        return [
            'code.unique' => 'このアカウントIDが既存です。新しいIDが生成されましたのでもう一回登録ボタントクリックしてください。',
            'required' => ':attributeを入力してください',
            'password_employee.min' => '※8文字以上、半角英数字',
            'email.unique' => 'このメールアドレスは既に登録されています。別のメールアドレスを登録してください。',
            'email.email' => 'メールアドレスを正しく形式で入力してください',
            'max' => ':attributeは:max文字以下にしてください',
            'katakana' => 'カタカナの正しい形式で入力してください',
        ];
    }
}
