<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists:users',
            'password' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'メールアドレスが未入力です',
            'email.email' => 'メールアドレスを正しく形式で入力してください',
            'email.exists' => 'ご入力頂いたメールアドレスでの登録がありません',
            'password.required' => 'パスワードが未入力です',
        ];
    }
}
