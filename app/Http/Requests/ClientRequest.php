<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function all($keys = NULL)
    {
        $input = parent::all();
        $input['name'] = str_replace('　', '', $input['name']);
        $input['name_furi'] = str_replace('　', '', $input['name_furi']);
        $input['first_name'] = str_replace('　', '', $input['first_name']);
        $input['last_name'] = str_replace('　', '', $input['last_name']);
        $input['first_name_furi'] = str_replace('　', '', $input['first_name_furi']);
        $input['last_name_furi'] = str_replace('　', '', $input['last_name_furi']);
        $input['email'] = str_replace('　', '', $input['email']);
        $input['post_code_1'] = str_replace('　', '', $input['post_code_1']);
        $input['post_code_2'] = str_replace('　', '', $input['post_code_2']);
        $input['address'] = str_replace('　', '', $input['address']);
        $input['hp'] = str_replace('　', '', $input['hp']);
        $input['note'] = str_replace('　', '', $input['note']);
        $input['tel_1'] = str_replace('　', '', $input['tel_1']);
        $input['tel_2'] = str_replace('　', '', $input['tel_2']);
        $input['tel_3'] = str_replace('　', '', $input['tel_3']);
        $input['fax_1'] = str_replace('　', '', $input['fax_1']);
        $input['fax_2'] = str_replace('　', '', $input['fax_2']);
        $input['fax_3'] = str_replace('　', '', $input['fax_3']);
        return $input;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'code' => 'required|unique:clients',
            'name' => 'required|max:255',
            'name_furi' => 'required|max:255|katakana',
            'post_code_1' => 'required|digits:3',
            'post_code_2' => 'required|digits:4',
            'address' => 'required',
            'tel_1' => 'nullable|digits:2|regex:/(0)[0-9]{1}/',
            'tel_2' => 'nullable|digits:4',
            'tel_3' => 'nullable|digits_between:4,5',
            'fax_1' => 'nullable|digits:2|regex:/(0)[0-9]{1}/',
            'fax_2' => 'nullable|digits:4',
            'fax_3' => 'nullable|digits:4',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'first_name_furi' => 'required|max:255|katakana',
            'last_name_furi' => 'required|max:255|katakana',
            'email' => 'required|email|max:255|unique:clients,email,NULL,id,deleted_at,NULL',
            'hp' => 'nullable|url',
        ];
        if ($this->id != '') {
            $rules = array_merge($rules, [
                'code' => 'required|unique:clients,code,' . $this->id,
                'email' => 'required|email|max:255|unique:clients,email,' . $this->id .',id,deleted_at,NULL',
            ]);
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'name' => '組織名称',
            'name_furi' => 'フリガナ',
            'first_name' => '代表者氏名',
            'last_name' => '代表者氏名',
            'first_name_furi' => 'フリガナ',
            'last_name_furi' => 'フリガナ',
            'email' => 'メールアドレス',
            'address' => '住所',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'code.unique' => 'このアカウントIDが既存です。新しいIDが生成されましたのでもう一回登録ボタントクリックしてください。',
            'required' => ':attributeを入力してください',
            'email.unique' => 'このメールアドレスは既に登録されています。別のメールアドレスを登録してください。',
            'email.email' => 'メールアドレスを正しく形式で入力してください',
            'max' => ':attributeは:max文字以下にしてください',
            'hp.url' => 'HPを正しく形式で入力してください',
            'katakana' => 'カタカナの正しい形式で入力してください',
        ];
    }
}
