<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'exists:users',
            'password' => 'required|min:8',
            'confirm-password' => 'required|same:password|min:8'
        ];
    }

    public function messages()
    {
        return [
            'email.exists' => 'ご入力頂いたメールアドレスでの登録がありません',
            'password.required' => '変更パスワードが未入力です',
            'confirm-password.required' => '確認パスワードが未入力です',
            'password.min' => '8文字以上入力ください',
            'confirm-password.same' => '変更パスワードに入力した内容と異なります',
            'confirm-password.min' => '8文字以上入力ください',
        ];
    }
}
