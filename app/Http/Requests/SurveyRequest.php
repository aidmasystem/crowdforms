<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SurveyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'descriptions' => 'max:512',
            'question.*.content' => 'required|max:512',
            'question.*.answer.*' => 'required|max:255',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => ':attributeを入力してください',
            'max' => ':attributeは、:max文字以下にしてください。',
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'アンケートタイトル',
            'descriptions' => 'アンケート詳細',
            'question.*.content' => '質問内容',
            'question.*.answer.*' => '選択肢',
        ];
    }
}
