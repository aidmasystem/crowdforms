<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerSurveyRequest extends FormRequest
{
    public function all($keys = NULL)
    {
        $input = parent::all();
        $input['old_message']= $input['message'];
        $input['message'] = strip_tags($input['message']);
        return $input;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'send_type' => 'required',
            'full_name' => 'required|max:255',
            'customer_name' => 'required',
            'title' => 'required|max:255',
            'question_package_id' => 'required',
            'message' => 'required|max:512',
            'ce_account_id' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => ':attributeを入力してください',
            'send_type.required' => '送信方法を選択してください',
            'question_package_id.required' => 'アンケートを選択してください',
            'max' => ':attributeは、:max文字以下にしてください。',
        ];
    }

    public function attributes()
    {
        return [
            'full_name' => '送信者名',
            'customer_name' => '送信先',
            'title' => '件名',
            'message' => 'メッセージ',
        ];
    }
}
