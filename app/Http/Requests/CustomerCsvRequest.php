<?php

namespace App\Http\Requests;

use App\Models\Customer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CustomerCsvRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param Request $request
     * @return array
     */
    public function rules(Request $request)
    {
        $data = $request->all();
        $arrSelect = [1, 2, 6, 7, 8, 9];
        $rules = [
            'file_upload' => 'required',
            'duplicateEmail' => 'required',
        ];

        if (!empty($data['title'])) {
            foreach ($data['title'] as $key => $row) {
                if (!empty($data['selectTitle']) && !empty($data['selectTitle'][$key])) {
                    $diff = array_diff($arrSelect, $data['selectTitle'][$key]);
                    if (!empty($diff)) {
                        if (in_array(Customer::COLUMN_FULL_NAME, $diff)) $rules = array_merge($rules, ['name.' . $key => 'required']);
                        if (in_array(Customer::COLUMN_FULL_NAME_FURI, $diff)) $rules = array_merge($rules, ['name_furi.' . $key => 'required']);
                        if (in_array(Customer::COLUMN_TEL, $diff)) $rules = array_merge($rules, ['tel.' . $key => 'required']);
                        if (in_array(Customer::COLUMN_EMAIL, $diff)) $rules = array_merge($rules, ['email.' . $key => 'required']);
                        if (in_array(Customer::COLUMN_POST_CODE, $diff)) $rules = array_merge($rules, ['post_code.' . $key => 'required']);
                        if (in_array(Customer::COLUMN_ADDRESS, $diff)) $rules = array_merge($rules, ['address.' . $key => 'required']);
                    }
                } else {
                    $rules = array_merge($rules, [
                        'name.' . $key => 'required',
                        'name_furi.' . $key => 'required',
                        'tel.' . $key => 'required',
                        'email.' . $key => 'required',
                        'post_code.' . $key => 'required',
                        'address.' . $key => 'required',
                    ]);
                }
            }
        }

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'file_upload.required' => 'アップロードしたいファイルを選択してください',
            'duplicateEmail.required' => 'メールを登録方法を一つ選択してください',
            'name.*.required' => '氏名を選択してください',
            'name_furi.*.required' => 'フリガナを選択してください',
            'tel.*.required' => '電話番号を選択してください ',
            'email.*.required' => 'メールアドレスを選択してください ',
            'post_code.*.required' => '郵便番号を選択してください',
            'address.*.required' => '住所を選択してください',
        ];
    }
}
