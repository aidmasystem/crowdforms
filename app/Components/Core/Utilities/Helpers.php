<?php

namespace App\Components\Core\Utilities;

use Illuminate\Support\Facades\Log;

class Helpers
{
    /**
     *  helper to check if a given variable has value
     *
     * @param $var
     * @param string|null $default
     * @return string
     */
    public static function hasValue(&$var, $default = null)
    {
        try {
            if (!isset($var)) return $default;

            if (is_bool($var)) return $var;

            if (empty($var) || is_null($var)) return $default;

            return $var;
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return $default;
        }
    }

    /**
     * multi-dimensional array sorting by custom keys
     *
     * @param $array
     * @param string $key
     * @param bool $highestFirst
     */
    public static function sort(&$array, $key = 'sort', $highestFirst = true)
    {
        usort($array, function ($a, $b) use ($key, $highestFirst) {
            return ($highestFirst) ? $b[$key] - $a[$key] : $a[$key] - $b[$key];
        });
    }
}
