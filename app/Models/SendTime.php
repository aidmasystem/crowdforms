<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SendTime extends Model
{
    protected $table = 'send_times';
    protected $primaryKey = 'id';
    protected $fillable = [
        'customer_survey_id',
        'total_email',
        'start_time',
        'end_time',
    ];
}
