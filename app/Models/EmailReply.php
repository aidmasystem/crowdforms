<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailReply extends Model
{
    protected $table = 'email_reply';
    protected $primaryKey = 'id';
    protected $fillable = [
        'question_package_id',
        'email',
        'created_at',
        'updated_at',
    ];
}
