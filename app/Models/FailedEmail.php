<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FailedEmail extends Model
{
    protected $table = 'failed_email';
    protected $primaryKey = 'id';
    protected $fillable = ['email', 'type', 'time'];
}
