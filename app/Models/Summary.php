<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Summary extends Model
{
    protected $table = 'summaries';
    protected $primaryKey = 'id';
    protected $fillable = [
        'customer_survey_id',
        'question_package_id',
        'send_amount',
        'success_amount',
        'fail_amount',
        'reply_amount',
        'created_at',
        'updated_at',
        'deleted_at',
        'created_by',
        'updated_by',
    ];

    const TEXT_SEND_DONE = '送信完了';
    const TEXT_SEND_ERROR = '送信エラー';
    const SEND_MAIL_DONE = 0; // Mail fail = 0

    public function survey()
    {
        return $this->hasOne(Survey::class, 'id', 'question_package_id');
    }

    public function customer_survey()
    {
        return $this->hasOne(CustomerSurvey::class, 'id');
    }

}
