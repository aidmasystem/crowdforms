<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Survey extends Model
{
    use SoftDeletes;

    protected $table = 'question_packages';
    protected $primaryKey = 'id';
    protected $fillable = [
        'client_id',
        'title',
        'descriptions',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];

    public function getCheckAttribute()
    {
        $check = false;
        $employee = CustomerSurvey::where(['question_package_id' => $this->id])->first();
        if ($employee != '') $check = true;

        return $check;
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }

    public function userCreate()
    {
        return $this->belongsTo(User::class, 'created_by', 'id')->withTrashed();
    }

    public function userUpdate()
    {
        return $this->belongsTo(User::class, 'updated_by', 'id')->withTrashed();
    }

    public function question()
    {
        return $this->hasMany(Question::class, 'question_package_id', 'id')
            ->select('id', 'question_package_id', 'content', 'type', 'is_obligatory');
    }

    protected $appends = ['check'];
}
