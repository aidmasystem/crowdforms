<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Question extends Model
{
    use SoftDeletes;

    protected $table = 'questions';
    protected $primaryKey = 'id';
    protected $fillable = [
        'question_package_id',
        'content',
        'type',
        'is_obligatory',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];

    const TYPE_ESSAY = 1;
    const TYPE_RADIO = 2;
    const TYPE_CHECKBOX = 3;
    const TYPE_SELECT_BOX = 4;
    const IS_OBLIGATORY = 1;

    public static $typeQuestion = [
        self::TYPE_ESSAY => '記述式',
        self::TYPE_RADIO => 'ラジオボタン',
        self::TYPE_CHECKBOX => 'チェックボタン',
        self::TYPE_SELECT_BOX => 'プルダウン',
    ];

    public function answer()
    {
        return $this->hasMany(Answer::class, 'question_id', 'id')
            ->select('id', 'question_id', 'content', 'is_other');
    }
}
