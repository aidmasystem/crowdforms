<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerSurvey extends Model
{
    protected $table = 'customer_survey';
    protected $primaryKey = 'id';
    protected $fillable = [
        'customer_id',
        'question_package_id',
        'ce_account_id',
        'send_type',
        'full_name',
        'email',
        'title',
        'message',
        'email_fail',
        'created_at',
        'updated_at',
        'deleted_at',
        'created_by',
        'updated_by',
    ];

    const SEND_TYPE_EMAIL = 1;

    public static $sendType = [
        self::SEND_TYPE_EMAIL => 'メール',
    ];
}
