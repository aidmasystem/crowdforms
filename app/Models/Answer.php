<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Answer extends Model
{
    use SoftDeletes;

    protected $table = 'answers';
    protected $primaryKey = 'id';
    protected $fillable = [
        'question_id',
        'content',
        'is_other',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];

    const ANSWER_OTHER = 1;

    public function getResultAnswer($question_id)
    {
        $result = CustomerAnswer::select('customer_answers.question_id', 'customer_answers.result', 'questions.type')
            ->join('questions', 'customer_answers.question_id', 'questions.id')
            ->where('customer_answers.question_id', $question_id)
            ->get();
        $arr = [];
        foreach ($result as $item){
            if ($item->type == Question::TYPE_RADIO || $item->type == Question::TYPE_SELECT_BOX){
                array_push($arr, ($item->result));

            }
            if ($item->type == Question::TYPE_CHECKBOX){
                $arr = array_merge($arr, json_decode($item->result));
            }
        }
        return array_count_values($arr);
    }
}
