<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerAnswer extends Model
{
    protected $table = 'customer_answers';
    protected $primaryKey = 'id';
    protected $fillable = [
        'question_id',
        'question_package_id',
        'customer_id',
        'result',
        'free_result',
        'created_at',
        'updated_at',
        'deleted_at',
        'created_by',
        'updated_by',
    ];
}
