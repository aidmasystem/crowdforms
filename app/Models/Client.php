<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    protected $table = 'clients';
    protected $primaryKey = 'id';
    protected $fillable = [
        'code',
        'name',
        'name_furi',
        'first_name',
        'last_name',
        'first_name_furi',
        'last_name_furi',
        'email',
        'tel',
        'fax',
        'post_code',
        'address',
        'hp',
        'note',
        'created_at',
        'updated_at',
        'deleted_at',
        'created_by',
        'updated_by',
    ];

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->last_name . ' ' . $this->first_name;
    }

    public function getPhoneAttribute()
    {
        return formatPhone($this->tel);
    }

    public function getCheckAttribute()
    {
        $check = false;
        $employee = User::where(['client_id' => $this->id, 'type' => User::TYPE_ACCOUNT_CE])->first();
        if ($employee != '') $check = true;

        return $check;
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function customers()
    {
        return $this->hasMany(Customer::class);
    }

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['fullname', 'phone', 'check'];
}
