<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'first_name',
        'last_name',
        'first_name_furi',
        'last_name_furi',
        'email',
        'password',
        'client_id',
        'role',
        'type',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->last_name . ' ' . $this->first_name;
    }

    /**
     * @return string
     */
    public function getFuriNameAttribute()
    {
        return $this->last_name_furi . ' ' . $this->first_name_furi;
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    // notice that here the attribute name is in snake_case
    protected $appends = ['fullname','furiname'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    const TYPE_ACCOUNT_AA = 1;
    const TYPE_ACCOUNT_CE = 2;

    const ROLE_AA_MANAGER = 1;
    const ROLE_AA_STAFF = 2;
    const ROLE_AA_PART = 3;
    public static $roleAdmin = [
        self::ROLE_AA_MANAGER => '管理者',
        self::ROLE_AA_STAFF => '一般社員',
        self::ROLE_AA_PART => 'バイト',
    ];
}
