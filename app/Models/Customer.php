<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;

    protected $table = 'customers';
    protected $primaryKey = 'id';
    protected $fillable = [
        'client_id',
        'full_name',
        'full_name_furi',
        'email',
        'can_duplicate',
        'company_name',
        'department_name',
        'position',
        'tel',
        'post_code',
        'address',
        'note',
        'created_at',
        'updated_at',
        'deleted_at',
        'created_by',
        'updated_by',
    ];

    const CAN_NOT_DUPLICATE = 0;
    const CAN_DUPLICATE = 1;

    const COLUMN_FULL_NAME = 1;
    const COLUMN_FULL_NAME_FURI = 2;
    const COLUMN_COMPANY_NAME = 3;
    const COLUMN_DEPARTMENT_NAME = 4;
    const COLUMN_POSITION = 5;
    const COLUMN_TEL = 6;
    const COLUMN_EMAIL = 7;
    const COLUMN_POST_CODE = 8;
    const COLUMN_ADDRESS = 9;
    const COLUMN_NOTE = 10;

    public function getPhoneAttribute()
    {
        return formatPhone($this->tel);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['phone'];
}
