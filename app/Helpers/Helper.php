<?php

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Models\Client;

if (!function_exists('formatPhone')) {
    function formatPhone($data)
    {
        $result = '';
        if ($data != '')
            $result = sprintf("%s-%s-%s",
                substr($data, 0, 2),
                substr($data, 2, 4),
                substr($data, 6));

        return $result;
    }
}

if (!function_exists('formatPostCode')) {
    function formatPostCode($data)
    {
        $result = '';
        if ($data != '') $result = sprintf("%s-%s", substr($data, 0, 3), substr($data, 3));

        return $result;
    }
}

if (!function_exists('convertNumberCsv')) {
    function convertNumberCsv($data)
    {
        $result = '';
        if ($data != '') $result = str_replace('-', '', $data);

        return $result;
    }
}

if (!function_exists('checkValidDomainMail')) {
    function checkValidDomainMail($email = '')
    {
        if (@$email) {
            try {
                $domain = @explode('@', $email)[1] ?? '______';
                if (!checkdnsrr($domain, 'MX')) {
                    return false; //If domain do not exists then return false
                } else {
                    return true;
                }
            } catch (\Exception $e) {
                Log::info($e->getMessage());
            }
        }
        return false;
    }
}

if (!function_exists('getCompanyName')) {
    function getCompanyName()
    {
        $companyName = '';
        $clientId = Auth::user()->client_id;
        if ($clientId != '') {
            $client = Client::find($clientId);
            if ($client != '') $companyName = $client->name;
        }

        return $companyName;
    }
}

if (!function_exists('getFullName')) {
    function getFullName()
    {
        return Auth::user()->fullname;
    }
}
