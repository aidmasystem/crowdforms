<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailService extends Mailable
{
    use Queueable, SerializesModels;

    public $content;
    public $subject;
    public $senderAddress;
    public $senderName;

    /**
     * Create a new message instance.
     *
     * @param $content
     * @param null $subject
     * @param null $senderAddress
     * @param null $senderName
     */
    public function __construct($content, $subject = null, $senderAddress = null, $senderName = null)
    {
        $this->content = $content;
        $this->subject = $subject != null ? $subject : config('app.name');
        $this->senderAddress = $senderAddress != null ? $senderAddress : config('mail.from.address');
        $this->senderName = $senderName != null ? $senderName : config('mail.from.name');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->senderAddress, $this->senderName)
            ->html($this->content)
            ->subject($this->subject);
    }
}
