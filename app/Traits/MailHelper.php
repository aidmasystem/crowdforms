<?php

namespace App\Traits;

use App\Mail\MailService;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

trait MailHelper
{
    public function sendMail($content, $receiveAddress, $surveyId, $route, $subject, $senderAddress = null, $senderName = null)
    {
        try {
            if (!empty($receiveAddress)) {
                foreach ($receiveAddress as $row) {
                    Mail::to($row)->send(new MailService($this->replaceMessage($row, $surveyId, $route, $content), $subject, $senderAddress, $senderName));
                }
            }

            return true;
        } catch (\Exception $exception) {
            Log::error("Send mail has exception: " . $exception->getMessage());
            Log::info($subject);
            Log::info($receiveAddress);
            Log::info($exception->getTraceAsString());
            return false;
        }
    }

    public function sendBCCMail($content, $receiveAddresses, $subject = null, $senderAddress = null, $senderName = null): bool
    {
        try {
            Mail::bcc($receiveAddresses)->send(new MailService($content, $subject, $senderAddress, $senderName));
            return true;
        } catch (\Exception $exception) {
            Log::error("Send mail has exception: " . $exception->getMessage());
            Log::info($subject);
            Log::info(print_r($receiveAddresses, true));
            Log::info($exception->getTraceAsString());
            return false;
        }
    }

    public function replaceMessage($email, $surveyId, $route, $content)
    {
        $pattern = "/(?<=href=(\"|'))[^\"']+(?=(\"|'))/";
        $param = [
            'survey_id' => $surveyId,
            'email' => $email,
        ];
        $newUrl = $route . '?token=' . Crypt::encrypt($param);
        preg_match_all($pattern, $content, $matches);
        if (!empty($matches[0])) {
            foreach ($matches[0] as $k => $v) {
                $content = str_replace('href="' . $v . '"', 'href="' . $newUrl . '"', $content);
            }
        } else {
            $content = preg_replace($pattern, $newUrl, $content);
        }

        return $content;
    }
}
