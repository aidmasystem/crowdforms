<?php

namespace App\Traits;

trait CoreQuery
{
    public function scopeLikeInject($query, $field, $value)
    {
        $inject_defend = str_replace(['%', '_'], ['\%', '\_'], $value);

        return $query->where($field, 'like', "%{$inject_defend}%");
    }
}
