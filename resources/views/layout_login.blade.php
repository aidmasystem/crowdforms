<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Crowd Forms</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/fontawesome/css/fontawesome.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/fontawesome/css/solid.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/fontawesome/css/brands.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/bootstrap/bootstrap-select.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet"/>

</head>
<body>
<!--header-->
<header class="header">
    <div class="header_content">
        <div class="container-custom">
            <div class="header_logo"><img src="{{ asset('images/logo.svg') }}" alt="" class="img-fluid"></div>
        </div>
    </div>
</header>

<!--main-->
<div class="main main-bg main-auth">
    @yield('content')
</div>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('js/common.js') }}"></script>
</body>
</html>
