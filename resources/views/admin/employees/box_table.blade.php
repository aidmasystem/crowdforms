@if($lists->isNotEmpty())
    @foreach($lists as $row)
    <tr>
        <td class="main_table_action">
            <a href="javascript:void(0);" data-id="{{ $row->id }}" data-url="{{ route('admin.employee.destroy', [$row->client_id, $row->id]) }}" class="delete-employee"
               data-toggle="modal" data-target="#deleteEmployee">
                <i class="fas fa-trash-alt main_icon m-0"></i>
            </a>
        </td>
        <td class="main_table_action"><a href="{{ route('admin.employee.edit', [$row->client_id, $row->id]) }}"><i class="fas fa-pen main_icon m-0"></i></a></td>
        <td>{{ $row->code }}</td>
        <td>{{ $row->fullname }}</td>
        <td>{{ $row->furiname }}</td>
        <td class="text-nowrap">{{ $row->email }}</td>
    </tr>
    @endforeach
@else
    <tr>
        <td colspan="6">
            <div class="text-center">データがありません。</div>
        </td>
    </tr>
@endif
