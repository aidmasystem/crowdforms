@extends('admin.layouts.app')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large mb-0"><i class="fas fa-pen main_icon"></i>クライアント編集</h2>
    </div>
    <!--table-->
    <form action="{{ route('admin.client.update', $client->id) }}" method="POST" id="frm" class="form-custom h-adr">
        @csrf
        {{ method_field('PUT') }}
        <div class="card-custom card-table mb-30">
            <table class="table table-bordered mb-0">
                <tbody>
                <tr class="card-table_bg">
                    <th>
                        アカウントID
                        @if($errors->has('code'))
                            <small class="form-text text-error">{{ $errors->first('code') }}</small>
                        @endif
                    </th>
                    <td>{{ $client->code }}<input type="hidden" name="code" value="{{ $client->code }}"></td>
                </tr>
                <tr>
                    <th class="th-header">
                        組織名称 <span class="card-table_label">必須</span>
                        @if($errors->has('name'))
                            <small class="form-text text-error">{{ $errors->first('name') }}</small>
                        @endif
                    </th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-6 col-custom">
                                <input type="text" name="name" value="{{ str_replace('　', '', old('name',$client->name)) }}"
                                       class="form-control @if($errors->has('name')) border-error @endif"
                                       placeholder="組織名称を入力してください" maxlength="255">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">
                        <label class="">フリガナ <span class="card-table_label">必須</span></label>
                        @if($errors->has('name_furi'))
                            <small class="form-text text-error">{{ $errors->first('name_furi') }}</small>
                        @endif
                    </th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-6 col-custom">
                                <input type="text" name="name_furi" value="{{ str_replace('　', '', old('name_furi',$client->name_furi)) }}"
                                       class="form-control @if($errors->has('name_furi')) border-error @endif"
                                       placeholder="組織名称のフリガナを入力してください" maxlength="255">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">
                        <label class="">住所 <span class="card-table_label">必須</span></label>
                        @if($errors->has('address'))
                            <small class="form-text text-error">{{ $errors->first('address') }}</small>
                        @elseif($errors->has('post_code_1') || $errors->has('post_code_2'))
                            <small class="form-text text-error">住所を正しく形式で入力してください (3数字- 4 数字 - 住所)</small>
                        @endif
                    </th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <span class="p-country-name" style="display:none;">Japan</span>
                            <span class="pl-2">〒</span>
                            <div class="col-1 col-custom">
                                <input type="text" name="post_code_1" value="{{ str_replace('　', '', old('post_code_1', @$post_code_1)) }}" class="p-postal-code form-control @if($errors->has('post_code_1')) border-error @endif" maxlength="3">
                            </div>
                            <span>-</span>
                            <div class="col-2 col-custom">
                                <input type="text" name="post_code_2" value="{{ str_replace('　', '', old('post_code_2', @$post_code_2)) }}" class="p-postal-code form-control @if($errors->has('post_code_2')) border-error @endif" maxlength="4">
                            </div>
                            <div class="col-6 col-custom">
                                <input type="text" name="address" value="{{ old('address', $client->address) }}"
                                       class="p-region p-locality p-street-address p-extended-address form-control @if($errors->has('address')) border-error @endif"
                                       placeholder="住所を入力してください" maxlength="255">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">
                        <label class="">電話番号</label>
                        @if($errors->has('tel_1') || $errors->has('tel_2') || $errors->has('tel_3'))
                            <small class="form-text text-error">電話番号を正しく形式で入力してください (0x-xxxx-xxxx|0x-xxxx-xxxxx )</small>
                        @endif
                    </th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-1 col-custom">
                                <input type="text" name="tel_1" value="{{ str_replace('　', '', old('tel_1',@$tel_1)) }}" class="form-control @if($errors->has('tel_1')) border-error @endif" maxlength="2">
                            </div>
                            <span>-</span>
                            <div class="col-2 col-custom">
                                <input type="text" name="tel_2" value="{{ str_replace('　', '', old('tel_2',@$tel_2)) }}" class="form-control @if($errors->has('tel_2')) border-error @endif" maxlength="4">
                            </div>
                            <span>-</span>
                            <div class="col-2 col-custom">
                                <input type="text" name="tel_3" value="{{ str_replace('　', '', old('tel_3',@$tel_3)) }}" class="form-control @if($errors->has('tel_3')) border-error @endif" maxlength="5">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">
                        <label class="">FAX番号</label>
                        @if($errors->has('fax_1') || $errors->has('fax_2') || $errors->has('fax_3'))
                            <small class="form-text text-error">ファクスを正しく形式で入力してください (0x-xxxx-xxxx)</small>
                        @endif
                    </th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-1 col-custom">
                                <input type="text" name="fax_1" value="{{ str_replace('　', '', old('fax_1',@$fax_1))}}" class="form-control @if($errors->has('fax_1')) border-error @endif" maxlength="2">
                            </div>
                            <span>-</span>
                            <div class="col-2 col-custom">
                                <input type="text" name="fax_2" value="{{ str_replace('　', '', old('fax_2',@$fax_2))}}" class="form-control @if($errors->has('fax_2')) border-error @endif" maxlength="4">
                            </div>
                            <span>-</span>
                            <div class="col-2 col-custom">
                                <input type="text" name="fax_3" value="{{ str_replace('　', '', old('fax_3',@$fax_3))}}" class="form-control @if($errors->has('fax_3')) border-error @endif" maxlength="4">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">
                        <label class="">代表者氏名 <span class="card-table_label">必須</span></label>
                        @if($errors->has('first_name'))
                            <small class="form-text text-error">{{ $errors->first('first_name') }}</small>
                        @elseif($errors->has('last_name'))
                            <small class="form-text text-error">{{ $errors->first('last_name') }}</small>
                        @endif
                    </th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-2 col-custom">
                                <input type="text" name="last_name" value="{{ str_replace('　', '', old('last_name',$client->last_name))}}"
                                       class="form-control @if($errors->has('last_name')) border-error @endif"
                                       placeholder="姓" maxlength="255">
                            </div>
                            <div class="col-2 col-custom">
                                <input type="text" name="first_name" value="{{ str_replace('　', '', old('first_name',$client->first_name))}}"
                                       class="form-control @if($errors->has('first_name')) border-error @endif"
                                       placeholder="名" maxlength="255">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">
                        <label class="">フリガナ <span class="card-table_label">必須</span></label>
                        @if($errors->has('first_name_furi'))
                            <small class="form-text text-error">{{ $errors->first('first_name_furi') }}</small>
                        @elseif($errors->has('last_name_furi'))
                            <small class="form-text text-error">{{ $errors->first('last_name_furi') }}</small>
                        @endif
                    </th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-2 col-custom">
                                <input type="text" name="last_name_furi" value="{{ str_replace('　', '', old('last_name_furi',$client->last_name_furi)) }}"
                                       class="form-control @if($errors->has('last_name_furi')) border-error @endif"
                                       placeholder="セイ" maxlength="255">
                            </div>
                            <div class="col-2 col-custom">
                                <input type="text" name="first_name_furi" value="{{ str_replace('　', '', old('first_name_furi',$client->first_name_furi)) }}"
                                       class="form-control @if($errors->has('first_name_furi')) border-error @endif"
                                       placeholder="メイ" maxlength="255">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">
                        <label class="">メールアドレス <span class="card-table_label">必須</span></label>
                        @if($errors->has('email'))
                            <small class="form-text text-error">{{ $errors->first('email') }}</small>
                        @endif
                    </th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-5 col-custom">
                                <input type="text" name="email" value="{{ str_replace('　', '', old('email',$client->email)) }}"
                                       class="form-control @if($errors->has('email')) border-error @endif"
                                       placeholder="メールアドレスを入力してください" maxlength="255">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">
                        <label>HP</label>
                        @if($errors->has('hp'))
                            <small class="form-text text-error">{{ $errors->first('hp') }}</small>
                        @endif
                    </th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-8 col-custom">
                                <input type="text" name="hp" value="{{ old('hp',$client->hp) }}"
                                       class="form-control @if($errors->has('hp')) border-error @endif"
                                       placeholder="HPのアドレスを入力してください" maxlength="255">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>備考</th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-11 col-custom">
                                <textarea class="form-control" placeholder="備考を入力してください"
                                          name="note" id="" cols="30" row row-customs="10" maxlength="512">{{ old('note',$client->note) }}</textarea>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="d-flex justify-content-center">
            <a href="{{ route('admin.client.index') }}" class="btn-custom btn-custom-disabled md mx-4 btn-custom-large">戻る</a>
            <button type="submit" id="btn-submit" class="btn-custom btn-custom-primary md mx-4 btn-custom-large">編集</button>
        </div>
    </form>
@endsection
