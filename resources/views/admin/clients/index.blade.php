@extends('admin.layouts.app')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large"><i class="fas fa-building main_icon"></i>クライアント一覧</h2>
        <a href="{{ route('admin.client.create') }}" class="btn-custom btn-custom-warning md"><i
                class="fas fa-plus main_icon text-white"></i>クライアント新規登録</a>
    </div>
    <div class="card-custom mb-30">
        <form action="" class="form-custom">
            <div class="form-group mb-0">
                <div class="d-flex form-custom_search position-relative">
                    <input type="text" name="keyword" class="form-control input-search" value="{{ request()->keyword }}"
                           placeholder="検索したいワードを入力してください"/>
                    <i class="fas fa-search"></i>
                    <button type="submit" class="btn-custom btn-custom-primary">検索</button>
                </div>
            </div>
        </form>
    </div>

    <!-- alert -->
    @if ($message = Session::get('success'))
        <div class="alert alert-success" role="alert">
            <p class="mb-0">{{ $message }}</p>
        </div>
    @endif

    <!--table-->
    <div class="main_table">
        <div class="d-flex justify-content-between">
            <p class="fs-20">件数 {{ @$count }}件</p>
            <ul class="pagination">
                {!! $lists->appends($_GET)->links() !!}
            </ul>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered text-center mb-0">
                <thead>
                <tr>
                    <th scope="col" class="main_table_action">削除</th>
                    <th scope="col" class="main_table_action">編集</th>
                    <th scope="col">担当者</th>
                    <th scope="col">クライアントID<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                                    class="img-fluid mb-1 sort-heading"
                                                                    id="code" data-sort="asc"></span></th>
                    <th scope="col">組織名<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                               class="img-fluid mb-1 sort-heading" id="name" data-sort="asc"></span>
                    </th>
                    <th scope="col">代表者名<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                                class="img-fluid mb-1 sort-heading"
                                                                id="fullname" data-sort="asc"></span></th>
                    <th scope="col">電話番号<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                                class="img-fluid mb-1 sort-heading" id="tel" data-sort="asc"></span>
                    </th>
                    <th scope="col">住所<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                              class="img-fluid mb-1 sort-heading"
                                                              id="address" data-sort="asc"></span></th>
                    <th scope="col">HP<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                              class="img-fluid mb-1 sort-heading" id="hp" data-sort="asc"></span>
                    </th>
                    <th scope="col">備考<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                              class="img-fluid mb-1 sort-heading" id="note" data-sort="asc"></span>
                    </th>
                </tr>
                </thead>
                <tbody>
                @if($lists->isNotEmpty())
                    @foreach($lists as $row)
                        <tr>
                            <td class="main_table_action">
                                <a href="javascript:void(0);" data-id="{{ $row->id }}" data-url="{{ route('admin.client.destroy', $row->id) }}"
                                   class="delete-client {{ $row->check ? 'disable' : '' }}"
                                   data-toggle="modal" data-target="#deleteClient">
                                    <i class="fas fa-trash-alt main_icon m-0"></i>
                                </a>
                            </td>
                            <td class="main_table_action">
                                <a href="{{ route('admin.client.edit', $row->id) }}"><i
                                        class="fas fa-pen main_icon m-0"></i></a>
                            </td>
                            <td><a href="{{ route('admin.employee.index', $row->id) }}" class="main_link">担当者確認</a></td>
                            <td>{{ $row->code }}</td>
                            <td class="large text-overflow">{{ $row->name }}</td>
                            <td>{{ $row->fullname }}</td>
                            <td>{{ $row->phone }}</td>
                            <td class="large">{{ $row->address }}</td>
                            <td class="text-nowrap">{{ $row->hp }}</td>
                            <td class="large text-overflow">{{ $row->note }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10">
                            <div class="text-center">データがありません。</div>
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
        <div class="mt-2 d-flex justify-content-between mt-2">
            <p class="fs-20">件数 {{ @$count }}件</p>
            <ul class="pagination">
                {!! $lists->appends($_GET)->links() !!}
            </ul>
        </div>

        <input type="hidden" name="hidden_page" id="hidden_page" value="1"/>
        <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="" />
        <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="" />
    </div>

    <div class="modal fade modal-custom" id="deleteClient" role="dialog" aria-labelledby="deleteClient" aria-hidden="true">
        <div class="delete modal-dialog modal-confirm">
            <div class="modal-content">
                <form action="#" method="POST" id="form_delete_client">
                    @csrf
                    @method('DELETE')
                    <div class="modal-header mb-3">
                        <h3 class="title title-large mb-0"><i class="mr-2 fas fa-exclamation-triangle fs-20 color-red"></i>削除</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body p-0 mb-3 text-center fs-16">
                        <p>削除しますがよろしいですか</p>
                    </div>
                    <div class="modal-footer justify-content-center p-0 mb-1">
                        <button type="button" class="btn-custom btn-custom-disabled w-50" data-dismiss="modal">キャンセル</button>
                        <button type="submit" class="btn-custom btn-custom-danger w-50">削除</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function () {
            $(document).on('click', '.delete-client', function() {
                let url = $(this).data('url');
                $('#form_delete_client').attr('action', url);
            } );

            $(".sort-heading").on('click', function (e) {
                e.preventDefault();
                let sort_name = $(this).attr('id');
                let sort_by = $(this).data('sort');
                clearIcon(sort_name);

                let keyword = '{{ request()->keyword }}';
                let page = 1;
                $('#hidden_column_name').val(sort_name);
                $('#hidden_sort_type').val(sort_by);

                fetchData(page, sort_name, sort_by, keyword);
            });

            $(document).on('click', '.pagination a', function(e){
                e.preventDefault();
                let page = $(this).attr('href').split('page=')[1];
                $('#hidden_page').val(page);
                var column_name = $('#hidden_column_name').val();
                var sort_by = $('#hidden_sort_type').val();
                let keyword = '{{ request()->keyword }}';

                fetchData(page, column_name, sort_by, keyword);
            });

            function fetchData(page, column_name, sort_by, keyword) {
                let getId = column_name;
                let sortBy = sort_by;
                $.ajax({
                    url: '{{ route('admin.client.index') }}',
                    type: 'GET',
                    data: {
                        page: page,
                        column: column_name,
                        sortOrder: sort_by,
                        keyword: keyword,
                    },
                    beforeSend: showLoad(),
                    success:function(data) {
                        hideLoad();
                        if (sortBy == 'asc') {
                            $('#' + getId).data('sort', 'desc');
                            $('#' + getId).attr('src', '{{ asset('images/icons/sort-asc.svg') }}');
                        } else if (sortBy == 'desc') {
                            $('#' + getId).data('sort', 'all');
                            $('#' + getId).attr('src', '{{ asset('images/icons/sort-desc.svg') }}');
                        } else if (sortBy == 'all') {
                            $('#' + getId).data('sort', 'asc');
                            $('#' + getId).attr('src', '{{ asset('images/icons/sort.svg') }}');
                        }

                        $('ul.pagination').html(data.paginate);
                        $(".table tr:not(:first)").remove();
                        $(".table").append(data.data);
                        // window.history.pushState("", "", this.url);
                    }
                });
            }
        });
    </script>
@endpush
