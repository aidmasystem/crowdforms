@if($lists->isNotEmpty())
    @foreach($lists as $row)
        <tr>
            <td class="main_table_action">
                <a href="javascript:void(0);" data-id="{{ $row->id }}" data-url="{{ route('admin.client.destroy', $row->id) }}"
                   class="delete-client {{ $row->check ? 'disable' : '' }}"
                   data-toggle="modal" data-target="#deleteClient">
                    <i class="fas fa-trash-alt main_icon m-0"></i>
                </a>
            </td>
            <td class="main_table_action">
                <a href="{{ route('admin.client.edit', $row->id) }}"><i
                        class="fas fa-pen main_icon m-0"></i></a>
            </td>
            <td><a href="{{ route('admin.employee.index', $row->id) }}" class="main_link">担当者確認</a></td>
            <td>{{ $row->code }}</td>
            <td class="large">{{ $row->name }}</td>
            <td>{{ $row->fullname }}</td>
            <td>{{ $row->phone }}</td>
            <td class="large">{{ $row->address }}</td>
            <td class="text-nowrap">{{ $row->hp }}</td>
            <td class="large text-overflow">{{ $row->note }}</td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="10">
            <div class="text-center">データがありません。</div>
        </td>
    </tr>
@endif
