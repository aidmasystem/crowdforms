<header class="header header-admin">
    <div class="header_content">
        <div class="container-custom">
            <div class="header_logo"><img src="{{ asset('images/logo.svg') }}" alt="" class="img-fluid"></div>
        </div>
    </div>
    <!--navbar-->
    <nav class="nav">
        <div class="container-custom px-0 d-flex justify-content-between align-items-center">
            <ul class="nav_list d-flex">
                <li class="nav_item">
                    <a class="nav_link {{ (request()->segment(2) == '' || request()->segment(2) == 'create'|| request()->segment(2) == 'edit') ? 'active' : '' }}" href="{{ route('admin.index') }}"><i class="fas fa-user main_icon"></i>AAアカウント管理</a>
                </li>
                <li class="nav_item">
                    <a class="nav_link {{ (request()->segment(2) == 'client') ? 'active' : '' }}" href="{{ route('admin.client.index') }}"><i class="fas fa-building main_icon"></i>クライアント管理</a>
                </li>
            </ul>
            <div class="dropdown">
                <div class="dropdown-toggle" id="dropdownAddress" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="line-clamp">
                        {{ getFullName() }}
                    </div>
                    <span class="dropdown_icon"></span>
                </div>
                <div class="dropdown-menu" aria-labelledby="dropdownAddress">
                    <a class="dropdown-item" href="{{ route('admin.logout') }}"><i class="fas fa-sign-out-alt main_icon"></i>ログアウト</a>
                </div>
            </div>
        </div>
    </nav>
</header>
