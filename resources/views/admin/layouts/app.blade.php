<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->

    <title>Crowd Forms</title>

    <!-- Fonts -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/fontmaterial.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fontawesome/css/fontawesome.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/fontawesome/css/solid.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/fontawesome/css/brands.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap-select.min.css') }}">

</head>
<body>
<div class="loadingspinnerbackdop">
    <div class="loader d-flex">
        <div class="dot dot1"></div>
        <div class="dot dot2"></div>
        <div class="dot dot3"></div>
    </div>
</div>

<!--header-->
@include('admin.layouts.header')
<!--/header-->

<!--main-->
<div class="main main-bg">
    <div class="container-custom main_content">
        @yield('content')
    </div>
</div>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('js/yubinbango.js') }}"></script>
<script src="{{ asset('js/common.js') }}"></script>
@stack('script')
</body>
</html>
