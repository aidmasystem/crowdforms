@extends('admin.layouts.app')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large mb-0"><i class="fas fa-pen main_icon"></i>AAアカウント編集</h2>
    </div>
    <!--table-->
    <form action="{{ route('admin.update', $admin->id) }}" method="POST" id="frm" class="form-custom">
        @csrf
        {{ method_field('PUT') }}
        <div class="card-custom card-table mb-30">
            <table class="table table-bordered mb-0">
                <tbody>
                <tr class="card-table_bg">
                    <th>
                        アカウントID
                        @if($errors->has('code'))
                            <small class="form-text text-error">{{ $errors->first('code') }}</small>
                        @endif
                    </th>
                    <td>{{ $admin->code }}<input type="hidden" name="code" value="{{ $admin->code }}"></td>
                </tr>
                <tr>
                    <th class="th-header">パスワード <span class="card-table_label">必須</span>
                    </th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-7 col-custom">
                                <input type="password" value="********" class="form-control" disabled>
                            </div>
                            <span>※8文字以上、半角英数字</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">氏名 <span class="card-table_label">必須</span>
                        @if($errors->has('first_name'))
                            <small class="form-text text-error">{{ $errors->first('first_name') }}</small>
                        @elseif($errors->has('last_name'))
                            <small class="form-text text-error">{{ $errors->first('last_name') }}</small>
                        @endif
                    </th>
                    <td>
                        <div class="row align-items-center">
                            <div class="col-2 col-custom">
                                <input type="text" name="last_name" value="{{ str_replace("　", '', old('last_name', $admin->last_name)) }}"
                                       class="form-control @if($errors->has('last_name')) border-error @endif"
                                       placeholder="田中" maxlength="255">
                            </div>
                            <div class="col-2 col-custom">
                                <input type="text" name="first_name" value="{{ str_replace("　", '', old('first_name', $admin->first_name)) }}"
                                       class="form-control @if($errors->has('first_name')) border-error @endif"
                                       placeholder="太郎" maxlength="255">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">フリガナ <span class="card-table_label">必須</span>
                        @if($errors->has('first_name_furi'))
                            <small class="form-text text-error">{{ $errors->first('first_name_furi') }}</small>
                        @elseif($errors->has('last_name_furi'))
                            <small class="form-text text-error">{{ $errors->first('last_name_furi') }}</small>
                        @endif
                    </th>
                    <td>
                        <div class="row align-items-center">
                            <div class="col-2 col-custom">
                                <input type="text" name="last_name_furi" value="{{ str_replace("　", '', old('last_name_furi', $admin->last_name_furi)) }}"
                                       class="form-control @if($errors->has('last_name_furi')) border-error @endif"
                                       placeholder="タナカ" maxlength="255">
                            </div>
                            <div class="col-2 col-custom">
                                <input type="text" name="first_name_furi" value="{{ str_replace("　", '', old('first_name_furi', $admin->first_name_furi)) }}"
                                       class="form-control @if($errors->has('first_name_furi')) border-error @endif"
                                       placeholder="タロウ" maxlength="255">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">メールアドレス <span class="card-table_label">必須</span>
                        @if($errors->has('email'))
                            <small class="form-text text-error">{{ $errors->first('email') }}</small>
                        @endif
                    </th>
                    <td>
                        <div class="row align-items-center">
                            <div class="col-9 col-custom">
                                <input type="text" name="email" value="{{ str_replace("　", '', old('email', $admin->email)) }}"
                                       class="form-control @if($errors->has('email')) border-error @endif"
                                       placeholder="メールアドレスを入力してください" maxlength="255">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">権限 <span class="card-table_label">必須</span>
                        @if($errors->has('role'))
                            <small class="form-text text-error">{{ $errors->first('role') }}</small>
                        @endif
                    </th>
                    <td>
                        @foreach($roles as $key => $role)
                            <div class="form-check form-check-inline">
                                <input class="form-check-input @if($errors->has('role'))border-error @endif"
                                       type="radio" name="role" value="{{ $key }}" {{ (old('role', $admin->role) == $key) ? 'checked' : ''}}>
                                <label class="form-check-label pl-2">
                                    {{ $role }}
                                </label>
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="d-flex justify-content-center">
            <a href="{{ route('admin.index') }}" class="btn-custom btn-custom-disabled md mx-4 btn-custom-large">戻る</a>
            <button type="submit" id="btn-submit" class="btn-custom btn-custom-primary md mx-4 btn-custom-large">編集</button>
        </div>
    </form>
@endsection
