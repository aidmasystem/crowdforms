@extends('layout_login')

@section('content')
    <div class="card-custom card-auth">
        <h2 class="card-auth_title text-center mb-20">パスワード再設定</h2>
        @if(session()->has('message'))
            <div class="alert alert-success">
                {!! session()->get('message') !!}
            </div>
        @endif
        <form action="{{route('admin.recover_pass')}}" method="POST" id="frm" class="form-custom">
            @csrf
            <div class="form-group mb-2">
                @if(session()->has('error'))
                    <small class="form-text text-error">
                        {!! session()->get('error') !!}
                    </small>
                @endif
                @if($errors->has('email'))
                    <small class="form-text text-error">{{ $errors->first('email') }}</small>
                @endif
                <input type="text" name="email" value="{{ session()->has('email') ? session()->get('email') : old('email') }}"
                       class="form-control @if($errors->has('email') || session()->has('email')) border-error @endif" placeholder="メールアドレスを入力してください">
            </div>
            <p class="mb-30 fs-12">
                ご登録されているメールアドレスに再設定用のメールを送信します。 メールアドレスをお忘れの場合は、お手数ですが
                <a href="{{ route('admin.contact') }}" class="card-auth_link m-0">サポートデスク</a>に お問い合わせください。
            </p>
            <button type="submit" id="btn-submit" class="w-100 btn-custom btn-custom-primary mb-25 btn-custom-medium">メール送信</button>
            <a href="{{ route('admin.login') }}" class="w-100 btn-custom btn-custom-disabled btn-custom-large">ログイン画面に戻る</a>
        </form>
    </div>
@endsection
