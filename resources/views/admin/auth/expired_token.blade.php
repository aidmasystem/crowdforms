@extends('layout_login')

@section('content')
    <div class="card-custom card-auth large">
        <h2 class="card-auth_title text-center mb-3">{{ @$title ? @$title : 'URLが存在しません.' }}</h2>
        <p class="text-center mb-30 fs-12">
            再度URLをチェックしてください。あるいはパスワード再設定画面に戻り、再度操作してください。
        </p>
        <div class="px-5"><a href="{{ route('admin.forgot_pass') }}"
             class="w-100 btn-custom btn-custom-primary btn-custom-medium">パスワード再設定画面に戻る</a></div>
    </div>
@endsection
