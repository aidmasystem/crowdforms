@extends('layout_login')

@section('content')

<div class="card-custom card-auth card-contact">
    <form action="">
        <p class="mb-3">再発行は管理者権限の方が行うか、サポートデスクまで お電話ください。</p>
        <a href="tel:0120-979-577" class="card-contact_block"><i class="fas fa-phone-alt main_icon"></i>0120-979-577</a>
        <p class="card-contact_time text-center">土日祝日を除く平日 10:00~18:00</p>
        <div class="px-4"><a href="{{ route('admin.login') }}" class="w-100 btn-custom btn-custom-disabled btn-custom-large">ログイン画面に戻る</a></div>
    </form>
</div>

@endsection
