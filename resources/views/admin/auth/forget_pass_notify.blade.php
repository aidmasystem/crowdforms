<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>パスワードのリセットを確認してください</title>
</head>
<body>
<p><strong>−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−</strong></p>
<p><strong>アカウントのパスワードリマインダー処理を行います。</strong></p>
<p><strong>登録されたメールアドレスにURLお送りします。</strong></p>
<p><strong>−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−</strong></p>
<p><strong>▼アカウント登録情報▼</strong><br></p>
<p><strong>認証用URL :</strong></p>
<a href="{{ $data['link'] }}">{{ $data['link'] }}</a><br>
<p><strong>URLは1日だけ有効です。</strong></p>
</body>
</html>
