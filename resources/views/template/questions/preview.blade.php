@extends('layout')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large"><i class="fas fa-edit main_icon"></i>アンケート詳細</h2>
    </div>
    <div class="card-custom mb-30">
        <p class="font-family-w6">アンケートタイトル</p>
        <h3 class="title-large fs-24 px-3 mb-3">無題のアンケート</h3>
        <p class="font-family-w6">アンケート詳細</p>
        <h4 class="fs-16 mb-0 px-3">アンケート詳細を入力してください</h4>
    </div>
    <!--list question-->
    <div class="mb-30">
        <div class="card-custom mb-3 form-custom">
            <h4 class="fs-14 font-family-w6 mb-3">性別を教えてください<span class="card-table_label font-family-w3 ml-1">必須</span></h4>
            <div class="form-custom_select col-3 p-0">
                <select class="selectType" title="選択してください">
                    <option>記述式</option>
                    <option>ラジオボタン</option>
                    <option>チェックボックス</option>
                    <option>プルダウン</option>
                </select>
            </div>
        </div>
        <div class="card-custom mb-3 form-custom">
            <h4 class="fs-14 font-family-w6 mb-3">今回のセミナーはいかがでしたか？<span class="card-table_label font-family-w3 ml-1">必須</span></h4>
            <div class="form-check mb-2"> 
                <input class="form-check-input large" type="radio" name="exampleRadios"  value="option1">
                <label class="form-check-label" for="">満足</label>
            </div>
            <div class="form-check mb-2"> 
                <input class="form-check-input large" type="radio" name="exampleRadios"  value="option1">
                <label class="form-check-label" for="">普通</label>
            </div>
            <div class="form-check mb-2"> 
                <input class="form-check-input large" type="radio" name="exampleRadios"  value="option1">
                <label class="form-check-label" for="">不満</label>
            </div>
        </div>
        <div class="card-custom mb-3 form-custom">
            <h4 class="fs-14 font-family-w6 mb-3">不満を選んだ方はどの点が不満だったかを入力してください</h4>
            <div class="form-custom_select">
            <textarea class="form-control" placeholder="入力してください。" name="" id="" cols="30" row row-customs="10"></textarea>
            </div>
        </div>
        <div class="card-custom mb-3 form-custom">
            <h4 class="fs-14 font-family-w6 mb-3">セミナーで今後受けたい内容はありますか？<span class="card-table_label font-family-w3 ml-1">必須</span></h4>
            <div class="form-check mb-2"> 
                <input class="form-check-input large" type="checkbox" name="exampleRadios"  value="option1">
                <label class="form-check-label" for="">満足</label>
            </div>
            <div class="form-check mb-2"> 
                <input class="form-check-input large" type="checkbox" name="exampleRadios"  value="option1">
                <label class="form-check-label" for="">どちらでもない</label>
            </div>
            <div class="form-check mb-2"> 
                <input class="form-check-input large" type="checkbox" name="exampleRadios"  value="option1">
                <label class="form-check-label" for="">不満</label>
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-center">
        <a href="/" class="btn-custom btn-custom-disabled md btn-custom-large">アンケート作成画面に戻る</a>
    </div>
@endsection