@extends('layout')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large"><i class="fas fa-plus main_icon"></i>アンケート新規登録</h2>
    </div>

    <form action="" class="form-custom">
        <div class="card-custom mb-30 card-custom-w">
            <div class="row flex-wrap">
                <div class="form-group col-8">
                    <label class="font-family-w6 fs-14" for="">アンケートタイトル<span class="card-table_label font-family-w3 ml-1">必須</span></label>
                    <!-- <small class="font-family-w3 color-red">アンケートタイトルを入力してください</small> -->
                    <input type='text' class="form-control large" placeholder="無題のアンケート" />
                </div>
                <div class="form-group col-8 mb-0">
                    <label class="font-family-w6 fs-14" for="">アンケート詳細</label>
                    <textarea class="form-control" placeholder="アンケート詳細を入力してください" name="" id="" cols="30" row row-customs="10"></textarea>
                </div>
            </div>
        </div>
        <div class="position-relative d-flex mb-2">
            <div class="card-custom_action">
                <button class="btn-custom"><i class="fas fa-plus-circle"></i></button>
                <a href="/question-list" class="btn-custom"><i class="fas fa-eye"></i></a>
            </div>
            <div class="card-custom-w" id="sortable">
                <div class="card-custom p-0 mb-3 main_card position-relative">
                    <span class="icon-drag"></span>
                    <button class="px-4 main_card_btn first" disabled>
                        <div class="mb-2">
                            <label class="font-family-w6 fs-14" for="">質問内容</label>
                            <!-- <small class="font-family-w3 color-red">質問内容を入力してください</small> -->
                            <div class="row row-custom">
                                <div class="col-5 col-custom">
                                    <input type='text' class="form-control" placeholder="質問内容を入力してください" />
                                </div>
                                <div class="col-3 col-custom">
                                    <div class="form-custom_select">
                                        <select class="selectType">
                                            <option value="normal">記述式</option>
                                            <option value="radio">ラジオボタン</option>
                                            <option value="checkbox">チェックボックス</option>
                                            <option value="index">プルダウン</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-custom_item mb-3 row" id="normal">
                            <div class="col-8">
                                <textarea class="form-control" name="" id="" cols="30" row row-customs="10"></textarea>
                            </div>
                        </div>
                    </button>
                    <div class="card-custom_item d-none" id="radio">
                        <div id="sortable_answer">
                            <div class="d-flex align-items-center mb-2 px-4">
                                <div class="form-check"> 
                                    <input class="form-check-input large" type="radio" name="exampleRadios"  value="option1">
                                </div>
                                <div class="row row-custom w-100">
                                    <div class="col-5 col-custom">
                                        <input type='text' class="form-control" placeholder="選択肢1" />
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex align-items-center mb-2 px-4">
                                <div class="form-check"> 
                                    <input class="form-check-input large" type="radio" name="exampleRadios"  value="option1">
                                </div>
                                <div class="row row-custom w-100">
                                    <div class="col-5 col-custom">
                                        <input type='text' class="form-control" placeholder="選択肢1" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="d-flex mb-3 main_card_btn px-4" disabled>
                            <div class="btn-custom btn-custom-outlined">選択肢を追加</div>
                            <div class="btn-custom btn-custom-outlined ml-2">「その他」を追加</div>
                        </button>
                    </div>
                    <button class="px-4 pb-4 main_card_btn last" disabled>
                        <div class="card-custom_item d-none" id="checkbox">
                            <div class="align-items-center d-flex mb-2">
                                <div class="form-check"> 
                                    <input class="form-check-input large" type="checkbox" name="exampleRadios"  value="option1">
                                </div>
                                <div class="row row-custom w-100">
                                    <div class="col-5 col-custom">
                                        <input type='text' class="form-control" placeholder="選択肢1" />
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex mb-3">
                                <div class="btn-custom btn-custom-outlined">選択肢を追加</div>
                                <div class="btn-custom btn-custom-outlined ml-2">「その他」を追加</div>
                            </div>
                        </div>
                        <div class="card-custom_item d-none" id="index">
                            <ol class="card-custom_list mb-2">
                                <li class="mb-2">
                                    <div class="row row-custom w-100">
                                        <div class="col-5 col-custom">
                                            <input type='text' class="form-control" placeholder="選択肢1" />
                                        </div>
                                    </div>
                                </li>
                            </ol>
                            <div class="d-flex mb-3">
                                <div class="btn-custom btn-custom-outlined">選択肢を追加</div>
                            </div>
                        </div>
                        <div class="card-custom_footer d-flex align-items-center justify-content-end">
                            <div class="btn-custom btn-custom-outlined mr-2"><i class="fas fa-copy main_icon large m-0"></i></div>
                            <div class="btn-custom btn-custom-outlined"><i class="fas fa-trash-alt main_icon large m-0"></i></div>
                            <label class="ml-3 mr-1 mb-0">必須</label>
                            <input type="checkbox" class="main_switch" />
                        </div>
                    </button>
                </div>
                <div class="card-custom p-0 mb-3 main_card position-relative">
                    <span class="icon-drag"></span>
                    <button class="px-4 main_card_btn first" disabled>
                        <div class="mb-2">
                        <label class="font-family-w6 fs-14" for="">質問内容<span class="card-table_label font-family-w3 ml-1">必須</span></label>
                            <!-- <small class="font-family-w3 color-red">質問内容を入力してください</small> -->
                            <div class="row row-custom">
                                <div class="col-5 col-custom">
                                    <input type='text' class="form-control" placeholder="質問内容を入力してください" />
                                </div>
                                <div class="col-3 col-custom">
                                    <div class="form-custom_select">
                                        <select class="selectType">
                                            <option value="normal">記述式</option>
                                            <option value="radio">ラジオボタン</option>
                                            <option value="checkbox">チェックボックス</option>
                                            <option value="index">プルダウン</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-custom_item mb-3 row" id="normal">
                            <div class="col-8">
                                <textarea class="form-control" name="" id="" cols="30" row row-customs="10"></textarea>
                            </div>
                        </div>
                    </button>
                    <div class="card-custom_item d-none" id="radio">
                        <div id="sortable_answer">
                            <div class="d-flex align-items-center mb-2 px-4">
                                <div class="form-check"> 
                                    <input class="form-check-input large" type="radio" name="exampleRadios"  value="option1">
                                </div>
                                <div class="row row-custom w-100">
                                    <div class="col-5 col-custom">
                                        <input type='text' class="form-control" placeholder="選択肢1" />
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex align-items-center mb-2 px-4">
                                <div class="form-check"> 
                                    <input class="form-check-input large" type="radio" name="exampleRadios"  value="option1">
                                </div>
                                <div class="row row-custom w-100">
                                    <div class="col-5 col-custom">
                                        <input type='text' class="form-control" placeholder="選択肢1" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="d-flex mb-3 main_card_btn px-4" disabled>
                            <div class="btn-custom btn-custom-outlined">選択肢を追加</div>
                            <div class="btn-custom btn-custom-outlined ml-2">「その他」を追加</div>
                        </button>
                    </div>
                    <button class="px-4 pb-4 main_card_btn last" disabled>
                        <div class="card-custom_item d-none" id="checkbox">
                            <div class="align-items-center d-flex mb-2">
                                <div class="form-check"> 
                                    <input class="form-check-input large" type="checkbox" name="exampleRadios"  value="option1">
                                </div>
                                <div class="row row-custom w-100">
                                    <div class="col-5 col-custom">
                                        <input type='text' class="form-control" placeholder="選択肢1" />
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex mb-3">
                                <div class="btn-custom btn-custom-outlined">選択肢を追加</div>
                                <div class="btn-custom btn-custom-outlined ml-2">「その他」を追加</div>
                            </div>
                        </div>
                        <div class="card-custom_item d-none" id="index">
                            <ol class="card-custom_list mb-2">
                                <li class="mb-2">
                                    <div class="row row-custom w-100">
                                        <div class="col-5 col-custom">
                                            <input type='text' class="form-control" placeholder="選択肢1" />
                                        </div>
                                    </div>
                                </li>
                            </ol>
                            <div class="d-flex mb-3">
                                <div class="btn-custom btn-custom-outlined">選択肢を追加</div>
                            </div>
                        </div>
                        <div class="card-custom_footer d-flex align-items-center justify-content-end">
                            <div class="btn-custom btn-custom-outlined mr-2"><i class="fas fa-copy main_icon large m-0"></i></div>
                            <div class="btn-custom btn-custom-outlined"><i class="fas fa-trash-alt main_icon large m-0"></i></div>
                            <label class="ml-3 mr-1 mb-0">必須</label>
                            <input type="checkbox" class="main_switch" />
                        </div>
                    </button>
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-center">
            <a href="/" class="btn-custom btn-custom-disabled md mx-4 btn-custom-large">キャンセル</a>
            <button type="submit" class="btn-custom btn-custom-primary md mx-4 btn-custom-large">登録</button>
        </div>
    </form>
@endsection