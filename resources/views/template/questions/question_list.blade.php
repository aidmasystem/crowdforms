@extends('layout')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large"><i class="fas fa-plus main_icon"></i>アンケート新規登録</h2>
    </div>
    <div class="card-custom mb-30">
        <h3 class="title-large">無題のアンケート</h3>
        <p class="title-medium mb-0">アンケート詳細を入力してください</p>
    </div>
    <!--list question-->
    <div class="mb-30">
        <div class="card-custom mb-3 form-custom">
            <h4 class="fs-14 font-family-w6 mb-3">本日のセミナーはどうでしたか？（ラジオボタン例）<span class="card-table_label font-family-w3 ml-1">必須</span></h4>
            <div class="form-check mb-2"> 
                <input class="form-check-input large" type="radio" name="exampleRadios"  value="option1">
                <label class="form-check-label" for="">満足</label>
            </div>
            <div class="form-check mb-2"> 
                <input class="form-check-input large" type="radio" name="exampleRadios"  value="option1">
                <label class="form-check-label" for="">どちらでもない</label>
            </div>
            <div class="form-check mb-2"> 
                <input class="form-check-input large" type="radio" name="exampleRadios"  value="option1">
                <label class="form-check-label" for="">不満</label>
            </div>
        </div>
        <div class="card-custom mb-3 form-custom">
            <h4 class="fs-14 font-family-w6 mb-3">本日のセミナーはどうでしたか？（チェックボタン例）<span class="card-table_label font-family-w3 ml-1">必須</span></h4>
            <div class="form-check mb-2"> 
                <input class="form-check-input large" type="checkbox" name="exampleRadios"  value="option1">
                <label class="form-check-label" for="">満足</label>
            </div>
            <div class="form-check mb-2"> 
                <input class="form-check-input large" type="checkbox" name="exampleRadios"  value="option1">
                <label class="form-check-label" for="">どちらでもない</label>
            </div>
            <div class="form-check mb-2"> 
                <input class="form-check-input large" type="checkbox" name="exampleRadios"  value="option1">
                <label class="form-check-label" for="">不満</label>
            </div>
        </div>
        <div class="card-custom mb-3 form-custom">
            <h4 class="fs-14 font-family-w6 mb-3">本日のセミナーはどうでしたか？（プルダウン例）<span class="card-table_label font-family-w3 ml-1">必須</span></h4>
            <div class="form-custom_select col-3 p-0">
                <select class="selectType" title="選択してください">
                    <option>記述式</option>
                    <option>ラジオボタン</option>
                    <option>チェックボックス</option>
                    <option>プルダウン</option>
                </select>
            </div>
        </div>
        <div class="card-custom mb-3 form-custom">
            <h4 class="fs-14 font-family-w6 mb-3">不満を選んだ方はどの点が不満だったかを入力してください</h4>
            <div class="form-custom_select">
                <input type='text' class="form-control" placeholder="回答を入力" />
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-center">
        <a href="/create-question" class="btn-custom btn-custom-disabled md mx-4 btn-custom-large">キャンセル</a>
        <button type="submit" class="btn-custom btn-custom-primary md mx-4 btn-custom-large">登録</button>
    </div>
@endsection