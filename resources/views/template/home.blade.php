@extends('layout')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large"><i class="fas fa-edit main_icon"></i>アンケート一覧</h2>
        <a href="/create-question" class="btn-custom btn-custom-warning md"><i class="fas fa-plus main_icon text-white"></i>アンケート新規登録</a>
    </div>
    <div class="card-custom mb-30">
        <form action="" class="form-custom">
            <div class="form-group">
                <div class="d-flex form-custom_search">
                    <input type="text" class="form-control" placeholder="検索したいワードを入力してください">
                    <i class="fas fa-search"></i>
                    <button type="submit" class="btn-custom btn-custom-primary">検索</button>
                </div>
                <small class="form-text color-red"></small>
            </div>
            <div class="d-flex">
                <div class="form-group mb-0 mr-5">
                    <label class="font-weight-bold fs-14" for="">作成日時</label>
                    <div class="d-flex align-items-center">
                        <div class='input-group form-custom_date' id='datepicker'>
                            <input type='text' class="form-control" />
                            <span class="input-group-addon"></span>
                        </div>
                        <span class="icon">~</span>
                        <div class='input-group form-custom_date' id='datepicker1'>
                            <input type='text' class="form-control" />
                            <span class="input-group-addon"></span>
                        </div>
                    </div>
                    <small class="form-text color-red"></small>
                </div>
                <div class="form-group mb-0 w-25">
                    <label class="font-weight-bold fs-14" for="">作成者</label>
                    <div class="form-custom_select multi">
                        <select class="selectpicker" 
                            data-live-search="true" 
                            data-actions-box="true" 
                            data-live-search-placeholder="検索" 
                            data-select-all-text="全て"
                            data-deselect-all-text="全て"
                            data-selected-text-format="count > 3"
                            title="選択してください"
                            multiple>
                            <optgroup label="選択中">
                                <option>田中太郎</option>
                                <option>田中太郎</option>
                                <option>田中太郎</option>
                            </optgroup>
                            <optgroup label="候補">
                                <option>山田花子</option>
                                <option>山田花子</option>
                                <option>山田花子</option>
                            </optgroup>
                        </select>
                    </div>
                    <small class="form-text color-red"></small>
                </div>
            </div>
        </form>
    </div>
    <!--table-->
    <div class="main_table">
        <div class="d-flex justify-content-between">
            <p class="fs-20">件数 1,000件</p>
            <ul class="pagination">
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item active">
                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                </li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#">Next</a>
                </li>
            </ul>
        </div>
        <div class="table-responsive mb-3">
            <table class="table table-bordered table-custom text-center mb-0">
                <thead>
                    <tr>
                        <th scope="col" class="main_table_action">詳細</th>
                        <th scope="col" class="main_table_action">削除</th>
                        <th scope="col" class="main_table_action">編集</th>
                        <th scope="col">アンケート名<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">アンケート詳細</th>
                        <th scope="col">作成日時<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">作成者<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">更新日<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">更新者<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="main_table_action large"><a href="/preview" class="btn-custom btn-custom-outlined">詳細</a></td>
                        <td class="main_table_action"><i class="fas fa-trash-alt main_icon m-0"></i></td>
                        <td class="main_table_action"><i class="fas fa-pen main_icon m-0"></i></td>
                        <td>〇〇〇アンケート</td>
                        <td class="large">セミナー後受講者満足度アンケート</td>
                        <td class="text-nowrap">2015-10-18 23:21:29</td>
                        <td>田中太郎</td>
                        <td class="text-nowrap">2015-10-18 23:21:29</td>
                        <td>田中太郎</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="d-flex justify-content-between">
            <p class="fs-20">件数 1,000件</p>
            <ul class="pagination">
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item active">
                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                </li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#">Next</a>
                </li>
            </ul>
        </div>
    </div>

@endsection