@extends('layout_admin')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large"><i class="fas fa-building main_icon"></i>クライアント一覧</h2>
        <a href="/admin/create-client" class="btn-custom btn-custom-warning md"><i class="fas fa-plus main_icon text-white"></i>クライアント新規登録</a>
    </div>
    <div class="card-custom mb-30">
        <form action="" class="form-custom">
            <div class="form-group mb-0">
                <div class="d-flex form-custom_search position-relative">
                    <input type="text" class="form-control" placeholder="検索したいワードを入力してください">
                    <i class="fas fa-search"></i>
                    <button type="submit" class="btn-custom btn-custom-primary">検索</button>
                </div>
            </div>
        </form>
    </div>
    <!--table-->
    <div class="main_table">
        <div class="d-flex justify-content-between">
            <p class="fs-20">件数 1,000件</p>
            <ul class="pagination">
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item active">
                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                </li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#">Next</a>
                </li>
            </ul>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered text-center mb-0">
                <thead>
                    <tr>
                        <th scope="col" class="main_table_action">削除</th>
                        <th scope="col" class="main_table_action">編集</th>
                        <th scope="col">担当者</th>
                        <th scope="col">クライアントID<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">組織名<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">代表者名<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">電話番号<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">住所<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">HP<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">備考<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="main_table_action"><i class="fas fa-trash-alt main_icon m-0"></i></td>
                        <td class="main_table_action"><i class="fas fa-pen main_icon m-0"></i></td>
                        <td><a href="" class="main_link">担当者確認</a></td>
                        <td>CA000000</td>
                        <td class="large">株式会社アイドマ・ホールディングス</td>
                        <td>田中 太郎</td>
                        <td>03-0000-0000</td>
                        <td class="large">東京都豊島区南池袋1-2-3 〇〇ビル2F</td>
                        <td class="text-nowrap">https://www.aidma-hd.jp/</td>
                        <td class="large text-overflow">備考が長い場合備考が長い場合備考が長い場合備考が</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection