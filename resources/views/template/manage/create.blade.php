@extends('layout_admin')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large mb-0"><i class="fas fa-plus main_icon"></i>クライアント新規登録</h2>
    </div>
    <!--table-->
    <form action="" class="form-custom">
        <div class="card-custom card-table mb-30">
            <table class="table table-bordered mb-0">
                <tbody>
                    <tr class="card-table_bg">
                        <th>アカウントID</th>
                        <td>AA00001</td>
                    </tr>
                    <tr>
                        <th>組織名称 <span class="card-table_label">必須</span></th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <div class="col-6 col-custom"><input type="text" class="form-control" placeholder="組織名称を入力してください"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>フリガナ <span class="card-table_label">必須</span></th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <div class="col-6 col-custom"><input type="text" class="form-control" placeholder="組織名称のフリガナを入力してください"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>住所 <span class="card-table_label">必須</span></th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <span class="pl-2">〒</span>
                                <div class="col-1 col-custom"><input type="text" class="form-control"></div>
                                <span>-</span>
                                <div class="col-2 col-custom"><input type="text" class="form-control"></div>
                                <div class="col-6 col-custom"><input type="text" class="form-control" placeholder="住所を入力してください"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>電話番号</th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <div class="col-1 col-custom"><input type="text" class="form-control"></div>
                                <span>-</span>
                                <div class="col-2 col-custom"><input type="text" class="form-control"></div>
                                <span>-</span>
                                <div class="col-2 col-custom"><input type="text" class="form-control"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>FAX番号</th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <div class="col-1 col-custom"><input type="text" class="form-control"></div>
                                <span>-</span>
                                <div class="col-2 col-custom"><input type="text" class="form-control"></div>
                                <span>-</span>
                                <div class="col-2 col-custom"><input type="text" class="form-control"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>代表者氏名 <span class="card-table_label">必須</span></th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <div class="col-2 col-custom"><input type="text" class="form-control" placeholder="姓"></div>
                                <div class="col-2 col-custom"><input type="text" class="form-control" placeholder="名"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>フリガナ <span class="card-table_label">必須</span></th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <div class="col-2 col-custom"><input type="text" class="form-control" placeholder="セイ"></div>
                                <div class="col-2 col-custom"><input type="text" class="form-control" placeholder="メイ"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>代表者メールアドレス <span class="card-table_label">必須</span></th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <div class="col-5 col-custom"><input type="text" class="form-control" placeholder="代表者メールアドレスを入力してください"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>HP</th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <div class="col-8 col-custom"><input type="text" class="form-control" placeholder="HPのアドレスを入力してください"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>備考</th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <div class="col-11 col-custom">
                                    <textarea class="form-control" placeholder="備考を入力してください" name="" id="" cols="30" row row-customs="10"></textarea>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="text-center">
            <button type="submit" class="btn-custom btn-custom-primary md mx-auto">編集</button>
        </div>
    </form>
@endsection