@extends('layout_admin')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large mb-0"><i class="fas fa-plus main_icon"></i>AAアカウント編集</h2>
    </div>
    <!--table-->
    <form action="" class="form-custom">
        <div class="card-custom card-table mb-30">
            <table class="table table-bordered mb-0">
                <tbody>
                    <tr>
                        <th>アカウントID</th>
                        <td>AA00001</td>
                    </tr>
                    <tr>
                        <th>パスワード <span class="card-table_label">必須</span></th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <div class="col-7 col-custom"><input type="text" class="form-control" placeholder="パスワードを入力してください"></div>
                                <span>※8文字以上、半角英数字</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>氏名 <span class="card-table_label">必須</span></th>
                        <td>
                            <div class="row align-items-center">
                                <div class="col-2 col-custom"><input type="text" class="form-control" placeholder="田中"></div>
                                <div class="col-2 col-custom"><input type="text" class="form-control" placeholder="太郎"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>フリガナ <span class="card-table_label">必須</span></th>
                        <td>
                            <div class="row align-items-center">
                                <div class="col-2 col-custom"><input type="text" class="form-control" placeholder="タナカ"></div>
                                <div class="col-2 col-custom"><input type="text" class="form-control" placeholder="タロウ"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>メールアドレス <span class="card-table_label">必須</span></th>
                        <td>
                            <div class="row align-items-center">
                                <div class="col-9 col-custom"><input type="text" class="form-control" placeholder="メールアドレスを入力してください"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>権限 <span class="card-table_label">必須</span></th>
                        <td>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="administrator" value="option1">
                                <label class="form-check-label pl-2" for="administrator">
                                    管理者
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="employee" value="option1" checked>
                                <label class="form-check-label pl-2" for="employee">
                                    一般社員
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="partime" value="option1">
                                <label class="form-check-label pl-2" for="partime">
                                    バイト
                                </label>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="text-center">
            <button type="submit" class="btn-custom btn-custom-primary md mx-auto">登録</button>
        </div>
    </form>
@endsection