@extends('layout_admin')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large"><i class="fas fa-user main_icon"></i>AAアカウント一覧</h2>
        <a href="/admin/create-user" class="btn-custom btn-custom-warning md"><i class="fas fa-plus main_icon text-white"></i>AAアカウント新規登録</a>
    </div>
    <div class="card-custom mb-30">
        <form action="" class="form-custom">
            <div class="form-group mb-0">
                <div class="d-flex form-custom_search position-relative">
                    <input type="text" class="form-control" placeholder="検索したいワードを入力してください">
                    <i class="fas fa-search"></i>
                    <button type="submit" class="btn-custom btn-custom-primary">検索</button>
                </div>
            </div>
        </form>
    </div>
    <!--table-->
    <div class="main_table">
        <div class="d-flex justify-content-between">
            <p class="fs-20">件数 1,000件</p>
            <ul class="pagination">
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item active">
                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                </li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#">Next</a>
                </li>
            </ul>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered text-center mb-0">
                <thead>
                    <tr>
                        <th scope="col" class="main_table_action">削除</th>
                        <th scope="col" class="main_table_action">編集</th>
                        <th scope="col">アカウントID<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">氏名<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">フリガナ<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">メールアドレス<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">権限<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="main_table_action"><i class="fas fa-trash-alt main_icon m-0"></i></td>
                        <td class="main_table_action"><i class="fas fa-pen main_icon m-0"></i></td>
                        <td>AA000000</td>
                        <td>@田中 太郎</td>
                        <td>タナカ タロウ</td>
                        <td class="text-nowrap">tanaka@aidma-hd.jp</td>
                        <td>管理者</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection