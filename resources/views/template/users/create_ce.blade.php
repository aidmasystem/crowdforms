@extends('layout_admin')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large mb-0"><i class="fas fa-plus main_icon"></i>CEアカウント編集</h2>
    </div>
    <!--table-->
    <form action="" class="form-custom">
        <div class="card-custom card-table mb-30">
            <table class="table table-bordered mb-0">
                <tbody>
                    <tr class="card-table_bg">
                        <th>アカウントID</th>
                        <td>AA00001</td>
                    </tr>
                    <tr class="card-table_bg">
                        <th>クライアント担当ID</th>
                        <td>CE00001</td>
                    </tr>
                    <tr>
                        <th>
                            パスワード <span class="card-table_label">必須</span><br>
                            <small class="font-family-w3 color-red">パスワードが未入力です</small>
                        </th>
                        <td>
                            <div class="row row-custom row row-custom-custom align-items-center">
                                <div class="col-7 col-custom"><input type="text" class="form-control" placeholder="パスワードを入力してください"></div>
                                <span>※8文字以上、半角英数字</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            氏名 <span class="card-table_label">必須</span><br>
                            <small class="font-family-w3 color-red">氏名が未入力です</small>
                        </th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <div class="col-2 col-custom"><input type="text" class="form-control" placeholder="姓"></div>
                                <div class="col-2 col-custom"><input type="text" class="form-control" placeholder="名"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            フリガナ <span class="card-table_label">必須</span><br>
                            <small class="font-family-w3 color-red">フリガナが未入力です</small>
                        </th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <div class="col-2 col-custom"><input type="text" class="form-control" placeholder="セイ"></div>
                                <div class="col-2 col-custom"><input type="text" class="form-control" placeholder="メイ"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            代表者メールアドレス <span class="card-table_label">必須</span><br>
                            <small class="font-family-w3 color-red">代表者メールアドレスが未入力です</small>
                        </th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <div class="col-5 col-custom"><input type="text" class="form-control" placeholder="代表者メールアドレスを入力してください"></div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="text-center">
            <button type="submit" class="btn-custom btn-custom-primary md mx-auto">編集</button>
        </div>
    </form>
@endsection