@extends('layout')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large mb-0"><i class="fas fa-plus main_icon"></i>送信先新規登録</h2>
    </div>
    <!--table-->
    <form action="" class="form-custom">
        <div class="card-custom card-table mb-30">
            <table class="table table-bordered mb-0">
                <tbody>
                    <tr>
                        <th>氏名 <span class="card-table_label">必須</span> <br>
                            <small class="font-family-w3 color-red">氏名が未入力です</small>
                        </th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <div class="col-6 col-custom"><input type="text" class="form-control" placeholder="氏名を入力してください"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>フリガナ <span class="card-table_label">必須</span><br>
                            <small class="font-family-w3 color-red">フリガナが未入力です</small>
                        </th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <div class="col-6 col-custom"><input type="text" class="form-control" placeholder="フリガナを入力してください"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>企業名</th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <div class="col-5 col-custom"><input type="text" class="form-control" placeholder="企業名を入力してください"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>部署名</th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <div class="col-4 col-custom"><input type="text" class="form-control" placeholder="部署名を入力してください"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>役職名</th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <div class="col-4 col-custom"><input type="text" class="form-control" placeholder="役職名を入力してください"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>電話番号 <span class="card-table_label">必須</span><br>
                            <small class="font-family-w3 color-red">電話番号が未入力です</small>
                        </th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <div class="col-4 col-custom"><input type="text" class="form-control" placeholder="電話番号を入力してください"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>メールアドレス <span class="card-table_label">必須</span><br>
                            <small class="font-family-w3 color-red">メールアドレスが未入力です</small>
                        </th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <div class="col-7 col-custom"><input type="text" class="form-control" placeholder="メールアドレスを入力してください"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>住所 <span class="card-table_label">必須</span><br>
                            <small class="font-family-w3 color-red">住所が未入力です</small>
                        </th>
                        <td>
                            <div class="row row-custom align-items-center flex-wrap">
                                <span class="pl-2">〒</span>
                                <div class="col-2 col-custom"><input type="text" class="form-control"></div>
                                <span>-</span>
                                <div class="col-2 col-custom"><input type="text" class="form-control"></div>
                                <div class="col-8 col-custom mt-2"><input type="text" class="form-control" placeholder="住所を入力してください"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>備考</th>
                        <td>
                            <div class="row row-custom align-items-center">
                                <div class="col-10 col-custom">
                                    <textarea class="form-control" placeholder="備考を入力してください" name="" id="" cols="30" row row-customs="10"></textarea>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-check mt-3">
                <input class="form-check-input large" type="checkbox" name="exampleRadios" id="registerEmail" value="option1">
                <label class="form-check-label pl-3" for="registerEmail">
                    メールアドレスの重複をそのまま登録する
                </label>
            </div>
        </div>
        <div class="text-center">
            <a href="/confirm-address" class="btn-custom btn-custom-primary md mx-auto d-inline-block btn-custom-large">確認</a>
        </div>
    </form>
@endsection