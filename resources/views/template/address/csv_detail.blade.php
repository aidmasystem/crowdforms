@extends('layout')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-3">
        <h2 class="title title-large mb-0"><i class="fas fa-file-csv main_icon"></i>CSV登録</h2>
    </div>
    <p class="mb-30 fs-18">
        登録する先頭5行を表示しています。<br>
        内容に問題がなければ「登録する」ボタンをクリックしてください。
    </p>
    <!--message error-->
    <!-- <p class="mb-30 fs-18 color-red">
        <i class="fas fa-exclamation-triangle fs-18"></i>
        エラーを検知しました。修正してから登録してください。
    </p> -->
    <form action="" class="form-custom">
        <div class="card-custom mb-20 main_table">
            <p class="mb-20 fs-16">
                company_20201001.csv
                <!-- <span class="d-block">
                    該当件数　1件
                </span> -->
            </p>
            <div class="table-responsive">
                <table class="table table-bordered table-custom-3 text-center mb-0 form-custom">
                    <thead>
                        <tr>
                            <th scope="col" class="main_table_action">削除</th>
                            <th scope="col">氏名</th>
                            <th scope="col">
                                組織名称
                            </th>
                            <th scope="col">
                                フリガナ
                            </th>
                            <th scope="col">
                                電話番号
                            </th>
                            <th scope="col">
                                住所
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <td class="main_table_action"><i class="fas fa-trash-alt main_icon m-0"></i></td>
                            <td>
                                田中 太郎
                            </td>
                            <td>sample株式会社</td>
                            <td>さんぷるかぶしきがいしゃ</td>
                            <td>
                                03-0000-0000
                                <!-- <small class="text-error">電話番号として不正です。</small>  
                                <input type="text" placeholder="20字以内" class="form-control border-error pl-small">   -->
                            </td>
                            <td>東京都豊島区南池</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="d-flex justify-content-center mt-4">
            <a href="/create-csv" class="btn-custom btn-custom-disabled btn-custom-large md mx-4">戻る</a>
            <button type="submit" class="btn-custom btn-custom-primary btn-custom-large md mx-4">登録</button>
        </div>
    </form>
@endsection