@extends('layout')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large mb-0"><i class="fas fa-plus main_icon"></i>送信先新規登録</h2>
    </div>
    <!--table-->
    <form action="" class="form-custom">
        <div class="card-custom card-table mb-30">
            <table class="table table-bordered mb-0">
                <tbody>
                    <tr>
                        <th>氏名 <span class="card-table_label">必須</span>
                        </th>
                        <td>
                            田中太郎
                        </td>
                    </tr>
                    <tr>
                        <th>フリガナ <span class="card-table_label">必須</span>
                        </th>
                        <td>
                            タナカタロウ
                        </td>
                    </tr>
                    <tr>
                        <th>企業名</th>
                        <td>
                            株式会社アイドマ・ホールディングス
                        </td>
                    </tr>
                    <tr>
                        <th>部署名</th>
                        <td>
                            営業
                        </td>
                    </tr>
                    <tr>
                        <th>役職名</th>
                        <td>
                            本部長
                        </td>
                    </tr>
                    <tr>
                        <th>電話番号 <span class="card-table_label">必須</span>
                        </th>
                        <td>
                            03-0000-0001
                        </td>
                    </tr>
                    <tr>
                        <th>メールアドレス <span class="card-table_label">必須</span>
                        </th>
                        <td>
                            t_tanaka@aidma-hd.jp
                        </td>
                    </tr>
                    <tr>
                        <th>住所 <span class="card-table_label">必須</span>
                        </th>
                        <td>
                            〒170-0022 <br>
                            東京都豊島区南池袋２丁目２５−５ 藤久ビル東三号館 5号館 4F
                        </td>
                    </tr>
                    <tr>
                        <th>備考</th>
                        <td>
                            
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-check mt-3">
                <input class="form-check-input large" type="checkbox" name="exampleRadios" id="registerEmail" value="option1">
                <label class="form-check-label pl-3" for="registerEmail">
                    メールアドレスの重複をそのまま登録する
                </label>
            </div>
        </div>
        <div class="d-flex justify-content-center">
            <a href="/create-address" class="btn-custom btn-custom-disabled btn-custom-large md mx-4">戻る</a>
            <button type="submit" class="btn-custom btn-custom-primary btn-custom-large md mx-4">登録</button>
        </div>
    </form>
@endsection