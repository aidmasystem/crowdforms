@extends('layout')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large mb-0"><i class="fas fa-file-csv main_icon"></i>CSV登録</h2>
    </div>
    <!--message upload success-->
    <!-- <p class="mb-30 fs-18">
        <i class="fas fa-check-circle color-green fs-24"></i>
        送信先の登録に成功しました。　[新規登録：〇〇件]
    </p> -->
    
    <form action="" class="form-custom_upload">
        <div class="card-custom mb-30">
            <div class="mb-3"><button class="btn-custom btn-custom-outlined btn-custom-large ml-auto"><i class="fas fa-download main_icon"></i>CSVフォーマットダウンロード</button></div>
            <div class="form-custom_upload_content mb-3">
                <div class="form-custom_upload_position">
                    <p class="form-custom_upload_txt">
                        タグを付与する企業データをドラッグ&ドロップしてください <br>
                        （最大容量は10Mまで）
                    </p>
                    <input type="file" id="upload" hidden/>
                    <label for="upload" class="form-custom_upload_label">ファイルを選択</label>
                </div>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="exampleRadios" id="registerEmail" value="option1">
                <label class="form-check-label" for="registerEmail">
                    メールアドレスの重複をそのまま登録する
                </label>
            </div><br>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="exampleRadios" id="emailAddress" value="option1">
                <label class="form-check-label" for="emailAddress">
                    メールアドレスで重複を一意にする
                </label>
            </div>
            <p class="mb-0">メールアドレスで重複を一意にする場合は、入力したメールアドレスを元にデータベースのデータを纏めます。</p>
        </div>
        <div class="form-custom_upload_list mb-30">
        </div>
        <div class="card-custom mb-2 main_table collapse" id="upload-content">
            <p class="mb-20">CSVファイルの先頭5行を表示しています</p>
            <div class="table-responsive">
                <table class="table table-bordered table-custom-3 text-center mb-0 form-custom">
                    <thead>
                        <tr>
                            <th scope="col" class="main_table_action large">データ<br>開始位置</th>
                            <th scope="col">
                                <div class="form-custom_select">
                                    <select class="selectType" title="プルダウン">
                                        <option>記述式</option>
                                        <option>ラジオボタン</option>
                                        <option>チェックボックス</option>
                                        <option>プルダウン</option>
                                    </select>
                                </div>
                            </th>
                            <th scope="col">
                                <div class="form-custom_select">
                                    <select class="selectType" title="プルダウン">
                                        <option>記述式</option>
                                        <option>ラジオボタン</option>
                                        <option>チェックボックス</option>
                                        <option>プルダウン</option>
                                    </select>
                                </div>
                            </th>
                            <th scope="col">
                                <div class="form-custom_select">
                                    <select class="selectType" title="プルダウン">
                                        <option>記述式</option>
                                        <option>ラジオボタン</option>
                                        <option>チェックボックス</option>
                                        <option>プルダウン</option>
                                    </select>
                                </div>
                            </th>
                            <th scope="col">
                                <div class="form-custom_select">
                                    <select class="selectType" title="プルダウン">
                                        <option>記述式</option>
                                        <option>ラジオボタン</option>
                                        <option>チェックボックス</option>
                                        <option>プルダウン</option>
                                    </select>
                                </div>
                            </th>
                            <th scope="col">
                                <div class="form-custom_select">
                                    <select class="selectType" title="プルダウン">
                                        <option>記述式</option>
                                        <option>ラジオボタン</option>
                                        <option>チェックボックス</option>
                                        <option>プルダウン</option>
                                    </select>
                                </div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="main_table_action large">
                                <div class="form-check pl-2">
                                    <input class="form-check-input large" type="radio" name="exampleRadios" id="registerEmail" value="option1">
                                </div>
                            </td>
                            <td>氏名</td>
                            <td>sample株式会社</td>
                            <td>さんぷるかぶしきがいしゃ</td>
                            <td>03-0000-0000</td>
                            <td>東京都豊島区南池</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="text-center mt-4">
            <a href="/csv-detail" class="btn-custom btn-custom-primary btn-custom-large md mx-auto d-inline-block">確認</a>
        </div>
    </form>
@endsection