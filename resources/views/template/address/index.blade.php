@extends('layout')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large"><i class="fas fa-paper-plane main_icon"></i>送信先一覧</h2>
        <div class="d-flex">
            <a href="/create-address" class="btn-custom btn-custom-warning md"><i class="fas fa-plus main_icon text-white"></i>送信先新規登録</a>
            <a href="/create-question" class="btn-custom btn-custom-warning px-4 ml-2"><i class="fas fa-file-csv main_icon text-white"></i>CSV登録</a>
        </div>
    </div>
    <div class="card-custom mb-30">
        <form action="" class="form-custom">
            <div class="form-group">
                <div class="d-flex form-custom_search">
                    <input type="text" class="form-control" placeholder="検索したいワードを入力してください">
                    <i class="fas fa-search"></i>
                    <button type="submit" class="btn-custom btn-custom-primary">検索</button>
                </div>
                <small class="form-text color-red"></small>
            </div>
            <div class="d-flex">
                <div class="form-group mb-0 mr-5">
                    <label class="font-weight-bold fs-14" for="">作成日時</label>
                    <div class="d-flex align-items-center">
                        <div class='input-group form-custom_date' id='datepicker'>
                            <input type='text' class="form-control" />
                            <span class="input-group-addon"></span>
                        </div>
                        <span class="icon">~</span>
                        <div class='input-group form-custom_date' id='datepicker1'>
                            <input type='text' class="form-control" />
                            <span class="input-group-addon"></span>
                        </div>
                    </div>
                    <small class="form-text color-red"></small>
                </div>
            </div>
        </form>
    </div>
    <!--table-->
    <div class="main_table">
        <div class="d-flex justify-content-between align-items-center mb-3">
            <p class="fs-20 mb-0">件数 1,000件</p>
            <div class="d-flex align-items-center">
                <button class="btn-custom btn-custom-primary btn-custom-large px-4" data-toggle="modal" data-target="#sendAddress">送信</button>
                <button class="btn-custom btn-custom-outlined ml-2 btn-custom-large bg-transparent px-4" data-toggle="modal" data-target="#checkStatus">送信状況の確認</button>
                <ul class="pagination ml-3 mb-0">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1">Previous</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item active">
                        <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#">Next</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="table-responsive mb-3">
            <table class="table table-bordered table-custom-3 text-center mb-0 form-custom">
                <thead>
                    <tr>
                        <th scope="col" class="main_table_action py-1">
                            <div class="form-check pl-2"> 
                                <input class="form-check-input large" type="checkbox" name="" id="checkAll">
                            </div>
                            <small>全選択</small>
                        </th>
                        <th scope="col" class="main_table_action">削除</th>
                        <th scope="col" class="main_table_action">編集</th>
                        <th scope="col">氏名<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">会社名<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">部署<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">役職<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">登録日時<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="main_table_action">
                            <div class="form-check pl-2"> 
                                <input class="form-check-input large" type="checkbox" name="chkSearch">
                            </div>
                        </td>
                        <td class="main_table_action"><i class="fas fa-trash-alt main_icon m-0"></i></td>
                        <td class="main_table_action"><i class="fas fa-pen main_icon m-0"></i></td>
                        <td>田中太郎</td>
                        <td class="large">株式会社〇〇〇〇〇〇</td>
                        <td>営業部</td>
                        <td>部長</td>
                        <td class="text-nowrap">2015-10-18 23:21:29</td> 
                    </tr>
                    <tr>
                        <td class="main_table_action">
                            <div class="form-check pl-2"> 
                                <input class="form-check-input large" type="checkbox" name="chkSearch">
                            </div>
                        </td>
                        <td class="main_table_action"><i class="fas fa-trash-alt main_icon m-0"></i></td>
                        <td class="main_table_action"><i class="fas fa-pen main_icon m-0"></i></td>
                        <td>田中太郎</td>
                        <td class="large">株式会社〇〇〇〇〇〇</td>
                        <td>営業部</td>
                        <td>部長</td>
                        <td class="text-nowrap">2015-10-18 23:21:29</td> 
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="d-flex justify-content-between align-items-center mb-3">
            <p class="fs-20 mb-0">件数 1,000件</p>
            <div class="d-flex align-items-center">
                <button class="btn-custom btn-custom-primary btn-custom-large px-4" data-toggle="modal" data-target="#sendAddress">送信</button>
                <button class="btn-custom btn-custom-outlined ml-2 btn-custom-large bg-transparent px-4" data-toggle="modal" data-target="#checkStatus">送信状況の確認</button>
                <ul class="pagination ml-3 mb-0">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1">Previous</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item active">
                        <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#">Next</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!--modal list-->
    <div class="modal fade modal-custom" id="checkStatus" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header mb-4">
                    <h2 class="title title-large mb-0"><i class="fas fa-paper-plane main_icon"></i>送信状況の確認</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-0 form-custom">
                    <h3 class="modal-body_title fs-14 font-family-w6 d-inline-block"><i class="fs-16 fas fa-envelope mr-1"></i>メール</h3>
                    <div class="modal-body_content main_table">
                        <div class="d-flex form-custom_search mb-3">
                            <input type="text" class="form-control">
                            <button type="submit" class="btn-custom btn-custom-primary">検索</button>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-custom-3 text-center mb-0 form-custom">
                                <thead>
                                    <tr>
                                        <th scope="col">送信アンケート<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                                        <th scope="col">送信完了<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                                        <th scope="col">状況<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>〇〇〇〇〇〇アンケート</td>
                                        <td>30/30人</td>
                                        <td>送信完了</td>
                                        <td class="px-3"><button class="btn-custom btn-custom-primary w-100 fs-14 py-1">再送信</button></td> 
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--modal send-->
    <div class="modal fade modal-custom" id="sendAddress" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered md" role="document">
            <div class="modal-content border-0 rounded">
                <div class="modal-header mb-3">
                    <h2 class="title title-large mb-0"><i class="fs-20 fas fa-paper-plane main_icon"></i>アンケートを送信</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-0">
                    <form action="" class="form-custom form-custom-modal">
                        <div class="form-group">
                            <label class="font-family-w6 fs-14" for="">
                                送信方法
                                <small class="font-family-w3 color-red">送信方法を選択してください</small>
                            </label>
                            <div class="row">
                                <div class="form-custom_select col-5">
                                    <select class="selectType" title="送信方法を選択してください">
                                        <option value="normal">記述式</option>
                                        <option value="radio">ラジオボタン</option>
                                        <option value="checkbox">チェックボックス</option>
                                        <option value="index">プルダウン</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="font-family-w6 fs-14" for="">
                                送信者名
                                <small class="font-family-w3 color-red">送信者名を入力してください</small>
                            </label>
                            <div class="row">
                                <div class="col-5">
                                    <input type="text" class="form-control w-100" placeholder="送信者名を入力してください">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="font-family-w6 fs-14" for="">
                                送信者アドレス
                                <small class="font-family-w3 color-red">送信者アドレスを入力してください</small>
                            </label>
                            <div class="row">
                                <div class="col-7">
                                    <input type="text" class="form-control w-100" placeholder="送信者アドレスを入力してください">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="font-family-w6 fs-14" for="">
                                送信先
                                <small class="font-family-w3 color-red">送信先を入力してください</small>
                            </label>
                            <div class="row">
                                <div class="col-5">
                                    <input type="text" class="form-control w-100" placeholder="送信先を選択してください">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="font-family-w6 fs-14" for="">
                                件名
                                <small class="font-family-w3 color-red">件名を入力してください</small>
                            </label>
                            <div class="row">
                                <div class="col-12">
                                    <input type="text" class="form-control w-100" placeholder="件名を入力してください">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="font-family-w6 fs-14" for="">
                                アンケート
                                <small class="font-family-w3 color-red">アンケートを選択してください</small>
                            </label>
                            <div class="row">
                                <div class="form-custom_select col-7">
                                    <select class="selectType" title="挿入するアンケートを選択してください">
                                        <option value="normal">記述式</option>
                                        <option value="radio">ラジオボタン</option>
                                        <option value="checkbox">チェックボックス</option>
                                        <option value="index">プルダウン</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="font-family-w6 fs-14" for="">
                                メッセージ
                                <small class="font-family-w3 color-red">メッセージを入力してください</small>
                            </label>
                            <div class="row">
                                <div class="col-12">
                                    <textarea class="form-control w-100" placeholder="メッセージを入力してください" name="note" id="" cols="30" row row-customs="10"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="mb-30">
                            <button class="mb-30 btn-custom btn-custom-outlined ml-auto"><i class="fas fa-edit main_icon"></i>アンケートの挿入</button>
                            <button class="btn-custom btn-custom-primary md m-auto btn-custom-large" data-dismiss="modal" data-toggle="modal" data-target="#confirm">確認</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--modal confirm--->
    <div class="modal fade modal-custom" id="confirm" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered md" role="document">
            <div class="modal-content border-0 rounded">
                <div class="modal-header mb-1">
                    <h2 class="title title-large mb-0"><i class="fs-24 fas fa-envelope main_icon"></i>送信メール内容確認</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-0">
                    <div class="modal-custom_item">
                        <span class="font-family-w6 d-inline-block modal-custom_item_title">送信方法</span>
                        メール
                    </div>
                    <div class="modal-custom_item">
                        <span class="font-family-w6 d-inline-block modal-custom_item_title">送信先</span>
                        田中太郎, 他10件の宛先
                    </div>
                    <div class="modal-custom_item">
                        <span class="font-family-w6 d-inline-block modal-custom_item_title">件名</span>
                        セミナー受講後アンケート
                    </div>
                    <div class="modal-custom_item">
                        <span class="font-family-w6 d-inline-block modal-custom_item_title">アンケート</span>
                        無題のアンケート
                    </div>
                    <div class="modal-custom_item border-0">
                        <span class="mb-1 font-family-w6 d-inline-block modal-custom_item_title">本文</span>
                        <div class="modal-custom_item_inner">
                            無題のアンケート
                        </div>
                    </div>
                    <div class="my-3">
                        <button class="mx-auto btn-custom btn-custom-primary btn-custom-large md" data-dismiss="modal" data-toggle="modal" data-target="#sendError">送信</button>
                    </div>
                </div>
            </div>
        </div>
    </div>  

    <!--modal send error-->
    <div class="modal fade modal-custom" id="sendError" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered sm" role="document">
            <div class="modal-content border-0 rounded">
                <div class="modal-header mb-3">
                    <h2 class="title title-large mb-0"><i class="fs-20 fas fa-paper-plane main_icon"></i>アンケートを送信</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-custom_border px-4 py-3">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-exclamation-triangle modal-custom_icon color-red"></i>
                        <div class="ml-4">
                            <h3 class="fs-18 mb-1 font-family-w6">メールの送信エラー</h3>
                            <p class="m-0">
                                送信先にアンケートを送信することができませんでした。<br>
                                メールアドレスを確認して、もう一度送信してみてください。
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--modal send success-->
    <div class="modal fade modal-custom" id="sendSuccess" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered sm" role="document">
            <div class="modal-content border-0 rounded">
                <div class="modal-header mb-3">
                    <h2 class="title title-large mb-0"><i class="fs-20 fas fa-paper-plane main_icon"></i>アンケートを送信</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-custom_border p-4">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-envelope main_icon modal-custom_icon m-0"></i>
                        <div class="ml-4">
                            <h3 class="fs-18 mb-1 font-family-w6">送信完了</h3>
                            <p class="m-0">
                                指定した送信先にアンケートを送信しました。
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection