@extends('layout_login')

@section('content')

<!--reset password-->
<div class="card-custom card-auth large">
    <h2 class="card-auth_title text-center mb-3">パスワード再設定</h2>
    <p class="text-center mb-20 fs-12">
        新しいパスワードを入力してください。 <br>
        登録したメールアドレス宛にパスワードを送信します。
    </p>
    <form action="" class="form-custom">
        <div class="row align-items-center mb-20">
            <div class="col-3">
                <label for="" class="mb-0 fs-12">メールアドレス</label>
            </div>
            <div class="form-group col-9 mb-0">
                <input type="email" class="form-control">
            </div>
        </div>
        <div class="row align-items-center mb-20">
            <div class="col-3">
                <label for="" class="mb-0 fs-12">変更パスワード</label>
            </div>
            <div class="form-group col-9 mb-0">
                <small class="form-text color-red pl-2"></small>
                <input type="password" class="form-control" placeholder="パスワードを入力してください">
            </div>
        </div>
        <div class="row align-items-center mb-30">
            <div class="col-3">
                <label for="" class="mb-0 fs-12">確認パスワード</label>
            </div>
            <div class="form-group col-9 mb-0">
                <small class="form-text color-red pl-2"></small>
                <input type="password" class="form-control" placeholder="パスワードを入力してください">
            </div>
        </div>
        <div class="px-5"><button class="w-100 btn-custom btn-custom-primary">変更する</button></div>
    </form>
</div>

<!--complete-->
<div class="card-custom card-auth large d-none">
    <h2 class="card-auth_title text-center mb-3">パスワード再設定完了</h2>
    <p class="text-center mb-30 fs-12">
        新しいパスワードに変更しました。<br>
        メールアドレスと先程設定した新しいパスワードでログインしてください。
    </p>
    <div class="px-5"><a href="/login" class="w-100 btn-custom btn-custom-primary">ログイン画面へ</a></div>
</div>

@endsection