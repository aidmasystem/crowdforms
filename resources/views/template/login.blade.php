@extends('layout_login')

@section('content')

<div class="card-custom card-auth">
    <h2 class="card-auth_title text-center">ログイン</h2>
    <form action="" class="form-custom">
        <div class="form-group">
            <small class="form-text color-red"></small>
            <input type="text" class="form-control" placeholder="メールアドレスを入力してください">
        </div>
        <div class="form-group">
            <small class="form-text color-red"></small>
            <input type="text" class="form-control" placeholder="パスワードを入力してください">
        </div>
        <p class="text-center mb-0">
            <a href="/forgot-password" class="card-auth_link">パスワードを忘れた場合</a>
        </p>
        <button class="w-100 btn-custom btn-custom-primary">ログイン</button>
    </form>
</div>

@endsection
