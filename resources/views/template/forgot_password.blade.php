@extends('layout_login')

@section('content')

<div class="card-custom card-auth">
    <h2 class="card-auth_title text-center mb-20">パスワード再設定</h2>
    <form action="" class="form-custom">
        <div class="form-group mb-2">
            <small class="form-text color-red"></small>
            <input type="email" class="form-control" placeholder="メールアドレスを入力してください">
        </div>
        <p class="mb-30 fs-12">
            ご登録されているメールアドレスに再設定用のメールを送信します。 メールアドレスをお忘れの場合は、お手数ですが
            <a href="/contact" class="card-auth_link m-0">サポートデスク</a>に お問い合わせください。
        </p>
        <button class="w-100 btn-custom btn-custom-primary mb-25">メール送信</button>
        <a href="/login" class="w-100 btn-custom btn-custom-disabled">ログイン画面に戻る</a>
    </form>
</div>

@endsection