@extends('layout')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large mb-0"><i class="fs-20 fas fa-clipboard-check main_icon"></i>アンケート結果集計</h2>
    </div>
    <div class="card-custom mb-30">
        <p class="font-family-w6">アンケートタイトル</p>
        <h3 class="title-large fs-24 px-3 mb-3">無題のアンケート</h3>
        <p class="font-family-w6">アンケート詳細</p>
        <h4 class="fs-16 mb-0 px-3">セミナー後受講者満足度アンケート</h4>
    </div>
    
    <div class="card-custom form-custom main_table">
        <p class="fs-20">回答数 5件</p>
        <div class="mb-20">
            <h4 class="fs-14 font-family-w6">【ラジオボタン・プルダウン例】セミナーはいかがでしたか？</h4>
            <div class="table-responsive">
                <table class="table table-bordered text-center mb-0">
                    <thead>
                        <tr>
                            <th scope="col">項目名</th>
                            <th scope="col">結果<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>満足</td>
                            <td>4</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="mb-2">
            <h4 class="fs-14 font-family-w6">【記述式例】感想をご自由に記載してください</h4>
            <div class="table-responsive">
                <table class="table table-bordered text-center mb-0">
                    <thead>
                        <tr>
                            <th scope="col">回答</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="mb-3">
            <button class="btn-custom btn-custom-outlined ml-auto"><i class="fas fa-edit main_icon"></i>回答をもっと表示</button>
        </div>
        <div>
            <h4 class="fs-14 font-family-w6">【チェックボタン例】セミナーで今後受けたい内容はありますか？（複数回答可能）</h4>
            <div class="table-responsive">
                <table class="table table-bordered text-center mb-0">
                    <thead>
                        <tr>
                            <th scope="col">項目名</th>
                            <th scope="col">結果<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ITの資格など（ITパスポートなど）</td>
                            <td>4</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection