@extends('layout')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large mb-0"><i class="fs-20 fas fa-clipboard-check main_icon"></i>アンケート結果集計</h2>
    </div>

    <!--table-->
    <div class="main_table card-custom">
        <div class="d-flex justify-content-between mb-2">
            <div class="d-flex form-custom align-items-center col-8 px-0">
                <div class="form-custom_select mr-3 col-6 px-0 multi sm">
                    <select class="selectpicker"
                        data-live-search="true"
                        data-actions-box="true"
                        data-live-search-placeholder="検索"
                        data-select-all-text="全て"
                        data-deselect-all-text="全て"
                        data-selected-text-format="count > 3"
                        title="選択してください"
                        multiple>
                        <optgroup label="選択中">
                            <option>〇〇〇〇〇アンケート</option>
                            <option>〇〇〇〇〇アンケート</option>
                            <option>〇〇〇〇〇アンケート</option>
                        </optgroup>
                        <optgroup label="候補">
                            <option>〇〇〇〇〇アンケート</option>
                            <option>〇〇〇〇〇アンケート</option>
                            <option>〇〇〇〇〇アンケート</option>
                        </optgroup>
                    </select>
                </div>
                <p class="fs-20 mb-0">件数 1,000件</p>
            </div>
            <ul class="pagination">
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item active">
                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                </li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#">Next</a>
                </li>
            </ul>
        </div>
        <div class="table-responsive mb-2">
            <table class="table table-bordered text-center mb-0">
                <thead>
                    <tr>
                        <th scope="col" class="main_table_action large">詳細</th>
                        <th scope="col">アンケート名</th>
                        <th scope="col">アンケート詳細</th>
                        <th scope="col">送信数<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">送信完了件数<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">送信エラー数<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">回答状況<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">各項目件数<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                        <th scope="col">作成日時<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1"></span></th>
                    </tr>
                    <tr>
                        <th scope="col" colspan="2"></th>
                        <th scope="col" class="font-family-w3 font-weight-normal">合計</th>
                        <th scope="col" class="font-family-w3 font-weight-normal">1000</th>
                        <th scope="col" class="font-family-w3 font-weight-normal">900</th>
                        <th scope="col" class="font-family-w3 font-weight-normal">100</th>
                        <th scope="col" class="font-family-w3 font-weight-normal">450</th>
                        <th scope="col" colspan="2"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="main_table_action large px-3"><a href="/statistic-detail" class="btn-custom btn-custom-outlined">詳細</a></td>
                        <td class="large">〇〇〇〇〇アンケート</td>
                        <td class="large">セミナー後受講者満足度アンケート</td>
                        <td>80</td>
                        <td>70</td>
                        <td>10</td>
                        <td>60</td>
                        <td>4</td>
                        <td class="text-nowrap">2015-10-18 23:21:29</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="d-flex justify-content-between mb-2">
            <div class="d-flex form-custom align-items-center col-8 px-0">
                <div class="form-custom_select mr-3 col-6 px-0 multi sm">
                    <select class="selectpicker"
                        data-live-search="true"
                        data-actions-box="true"
                        data-live-search-placeholder="検索"
                        data-select-all-text="全て"
                        data-deselect-all-text="全て"
                        data-selected-text-format="count > 3"
                        title="選択してください"
                        multiple>
                        <optgroup label="選択中">
                            <option>〇〇〇〇〇アンケート</option>
                            <option>〇〇〇〇〇アンケート</option>
                            <option>〇〇〇〇〇アンケート</option>
                        </optgroup>
                        <optgroup label="候補">
                            <option>〇〇〇〇〇アンケート</option>
                            <option>〇〇〇〇〇アンケート</option>
                            <option>〇〇〇〇〇アンケート</option>
                        </optgroup>
                    </select>
                </div>
                <p class="fs-20 mb-0">件数 1,000件</p>
            </div>
            <ul class="pagination">
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item active">
                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                </li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#">Next</a>
                </li>
            </ul>
        </div>
    </div>

@endsection
