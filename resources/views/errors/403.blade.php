<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->

    <title>Crowd Forms</title>

    <!-- Fonts -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/fontawesome/css/fontawesome.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/fontawesome/css/solid.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/fontawesome/css/brands.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet"/>

</head>
<body>
<header class="header">
    <div class="header_content">
        <div class="container-custom">
            <div class="header_logo"><img src="{{ asset('images/logo.svg') }}" alt="" class="img-fluid"></div>
        </div>
    </div>
</header>
<div class="main main-bg">
    <div class="container-custom main_content">
        <div class="page-wrap d-flex flex-row align-items-center">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-12 text-center">
                        <i class="fas fa-exclamation-triangle error-icon mb-4 mt-5"></i>
                        <span class="display-4 mb-4 d-block">システムエラーが発生しました。</span>
                        <div class="mb-4 lead">申し訳ありませんがもう一度操作をやり直してください</div>
                        <a href="javascript:void(0);" onclick="window.history.back()" class="btn btn-link">前の画面へ戻る</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
