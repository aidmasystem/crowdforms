@extends('client.layouts.master')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large mb-0"><i class="fas fa-pen main_icon"></i>アカウント編集</h2>
    </div>
    <!--table-->
    <form action="{{ route('client.employee.update', $employee->id) }}" method="POST" id="frm" class="form-custom">
        @csrf
        {{ method_field('PUT') }}
        <div class="card-custom card-table mb-30">
            <table class="table table-bordered mb-0">
                <tbody>
                <tr class="card-table_bg">
                    <th class="th-header">
                        クライアントID
                        @if($errors->has('code_ca'))
                            <small class="form-text text-error">{{ $errors->first('code_ca') }}</small>
                        @endif
                    </th>
                    <td>{{ $code_ca }}<input type="hidden" name="code_ca" value="{{ $code_ca }}"></td>
                </tr>
                <input type="hidden" name="client_id" value="{{ $employee->client_id }}">
                <tr class="card-table_bg">
                    <th class="th-header">
                        クライアント担当ID
                        @if($errors->has('code'))
                            <small class="form-text text-error">{{ $errors->first('code') }}</small>
                        @endif
                    </th>
                    <td>{{ $employee->code }}<input type="hidden" name="code" value="{{ $employee->code }}"></td>
                </tr>
                <tr>
                    <th class="th-header">パスワード <span class="card-table_label">必須</span>
                    </th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-7 col-custom">
                                <input type="password" value="********" class="form-control" disabled>
                            </div>
                            <span>※8文字以上、半角英数字</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">氏名 <span class="card-table_label">必須</span>
                        @if($errors->has('first_name'))
                            <small class="form-text text-error">{{ $errors->first('first_name') }}</small>
                        @elseif($errors->has('last_name'))
                            <small class="form-text text-error">{{ $errors->first('last_name') }}</small>
                        @endif
                    </th>
                    <td>
                        <div class="row align-items-center">
                            <div class="col-2 col-custom">
                                <input type="text" name="last_name" value="{{ str_replace('　', '', old('last_name',$employee->last_name)) }}"
                                       class="form-control @if($errors->has('last_name')) border-error @endif"
                                       placeholder="田中" maxlength="255">
                            </div>
                            <div class="col-2 col-custom">
                                <input type="text" name="first_name" value="{{ str_replace('　', '', old('first_name',$employee->first_name)) }}"
                                       class="form-control @if($errors->has('first_name')) border-error @endif"
                                       placeholder="太郎" maxlength="255">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">フリガナ <span class="card-table_label">必須</span>
                        @if($errors->has('first_name_furi'))
                            <small class="form-text text-error">{{ $errors->first('first_name_furi') }}</small>
                        @elseif($errors->has('last_name_furi'))
                            <small class="form-text text-error">{{ $errors->first('last_name_furi') }}</small>
                        @endif
                    </th>
                    <td>
                        <div class="row align-items-center">
                            <div class="col-2 col-custom">
                                <input type="text" name="last_name_furi" value="{{ str_replace('　', '', old('last_name_furi',$employee->last_name_furi)) }}"
                                       class="form-control @if($errors->has('last_name_furi')) border-error @endif"
                                       placeholder="タナカ" maxlength="255">
                            </div>
                            <div class="col-2 col-custom">
                                <input type="text" name="first_name_furi" value="{{ str_replace('　', '', old('first_name_furi',$employee->first_name_furi)) }}"
                                       class="form-control @if($errors->has('first_name_furi')) border-error @endif"
                                       placeholder="タロウ" maxlength="255">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">代表者メールアドレス <span class="card-table_label">必須</span>
                        @if($errors->has('email'))
                            <small class="form-text text-error">{{ $errors->first('email') }}</small>
                        @endif
                    </th>
                    <td>
                        <div class="row align-items-center">
                            <div class="col-9 col-custom">
                                <input type="text" name="email" value="{{ str_replace('　', '', old('email',$employee->email)) }}"
                                       class="form-control @if($errors->has('email')) border-error @endif"
                                       placeholder="代表者メールアドレスを入力してください" maxlength="255">
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="d-flex justify-content-center">
            <a href="{{ route('client.employee.index') }}" class="btn-custom btn-custom-disabled md mx-4 btn-custom-large">戻る</a>
            <button type="submit" id="btn-submit" class="btn-custom btn-custom-primary md mx-4 btn-custom-large">編集</button>
        </div>
    </form>
@endsection
