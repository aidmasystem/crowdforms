<div class="modal-header mb-3">
    <h2 class="title title-large mb-0"><i class="fs-20 fas fa-paper-plane main_icon"></i>アンケートを送信</h2>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-custom_border p-4">
    <div class="d-flex align-items-center">
        <i class="fas fa-envelope main_icon modal-custom_icon m-0"></i>
        <div class="ml-4">
            <h3 class="fs-18 mb-1 font-family-w6">送信完了</h3>
            <p class="m-0">
                指定した送信先にアンケートを送信しました。
            </p>
        </div>
    </div>
</div>
