<div class="modal-header mb-3">
    <h2 class="title title-large mb-0"><i class="fs-20 fas fa-paper-plane main_icon"></i>アンケートを送信
    </h2>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body p-0">
    <form action="" method="POST" class="form-custom form-custom-modal" id="form-confirm-send">
        @csrf
        <div class="form-group">
            <label class="font-family-w6 fs-14" for="">
                送信方法
            </label>
            <small class="font-family-w3 color-red error-mgs-type"></small>
            <div class="row">
                <div class="form-custom_select col-5">
                    <select class="selectType input-mgs-type" name="send_type" title="送信方法を選択してください">
                        @foreach($sendType as $key => $row)
                            <option value="{{ $key }}" {{ $key == \App\Models\CustomerSurvey::SEND_TYPE_EMAIL ? 'selected' : '' }}>{{ $row }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="font-family-w6 fs-14" for="">
                送信者名
            </label>
            <small class="font-family-w3 color-red error-mgs-name"></small>
            <div class="row">
                <div class="col-5">
                    <input type="text" name="full_name" class="form-control input-mgs-name w-100"
                           maxlength="255" value="" placeholder="送信者名を入力してください">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="font-family-w6 fs-14" for="">
                送信先
            </label>
            <small class="font-family-w3 color-red error-mgs-customer"></small>
            <div class="row">
                <div class="col-5">
                    <input type="text" name="customer_name" class="form-control input-mgs-customer w-100"
                           value="{{ @$customerName }}" readonly
                           placeholder="送信先を選択してください">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="font-family-w6 fs-14" for="">
                件名
            </label>
            <small class="font-family-w3 color-red error-mgs-title"></small>
            <div class="row">
                <div class="col-12">
                    <input type="text" name="title" class="form-control input-mgs-title w-100" value=""
                           maxlength="255" placeholder="件名を入力してください">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="font-family-w6 fs-14" for="">
                アンケート
            </label>
            <small class="font-family-w3 color-red error-mgs-survey"></small>
            <div class="row">
                <div class="form-custom_select select-survey_send col-7">
                    <select name="question_package_id" id="choose-survey" class="selectType input-mgs-survey" title="挿入するアンケートを選択してください">
                        @foreach($survey as $key => $row)
                            <option value="{{ $key }}">{{ $row }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="font-family-w6 fs-14" for="">
                メッセージ
            </label>
            <small class="font-family-w3 color-red error-mgs-message"></small>
            <div class="row">
                <div class="col-12">
                <textarea id="editorMess" class="form-control input-mgs-message w-100" placeholder="メッセージを入力してください" name="message"
                          cols="30" row row-customs="10"></textarea>
                </div>
            </div>
        </div>
        <input type="hidden" name="ce_account_id" value="{{ $id }}">
        <input type="hidden" name="customer_id" value="{{ @$customerId }}">
        <div class="mb-30">
            <button type="button" id="add-link_survey" class="mb-30 btn-custom btn-custom-outlined ml-auto"><i class="fas fa-edit main_icon"></i>アンケートの挿入
            </button>
            <button type="submit" class="btn-custom btn-custom-primary md m-auto btn-custom-large"
                    id="send-mail_confirm">確認
            </button>
        </div>
    </form>
</div>
