<div class="modal-header mb-1">
    <h2 class="title title-large mb-0"><i class="fs-24 fas fa-envelope main_icon"></i>送信メール内容確認</h2>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body p-0">
    <form action="" method="POST" id="form-submit-send">
        @csrf
        <div class="modal-custom_item">
            <span class="font-family-w6 d-inline-block modal-custom_item_title">送信方法</span>
            {{ \App\Models\CustomerSurvey::$sendType[$send_type] }}
        </div>
        <div class="modal-custom_item">
            <span class="font-family-w6 d-inline-block modal-custom_item_title">送信先</span>
            {{ @$customer_name }}
        </div>
        <div class="modal-custom_item">
            <span class="font-family-w6 d-inline-block modal-custom_item_title">件名</span>
            {{ $title }}
        </div>
        <div class="modal-custom_item">
            <span class="font-family-w6 d-inline-block modal-custom_item_title">アンケート</span>
            {{ @$survey_name }}
        </div>
        <div class="modal-custom_item border-0">
            <span class="mb-1 font-family-w6 d-inline-block modal-custom_item_title">本文</span>
            <div class="modal-custom_item_inner style-scroll">
                {!! $message !!}
            </div>
        </div>
        <input type="hidden" name="send_type" value="{{ @$send_type }}">
        <input type="hidden" name="ce_account_id" value="{{ $ce_account_id }}">
        <input type="hidden" name="question_package_id" value="{{ $question_package_id }}">
        <input type="hidden" name="customer_id" value="{{ @$customer_id }}">
        <input type="hidden" name="full_name" value="{{ @$full_name }}">
        <input type="hidden" name="title" value="{{ @$title }}">
        <input type="hidden" name="message" value="{{ @$message }}">
        <div class="my-3">
            <button type="submit" class="mx-auto btn-custom btn-custom-primary btn-custom-large md"
                    id="send-mail_submit">送信
            </button>
        </div>
    </form>
</div>
