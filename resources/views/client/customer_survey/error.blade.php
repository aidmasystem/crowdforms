<div class="modal-header mb-3">
    <h2 class="title title-large mb-0"><i class="fs-20 fas fa-paper-plane main_icon"></i>アンケートを送信</h2>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-custom_border px-4 py-3">
    <div class="d-flex align-items-center">
        <i class="fas fa-exclamation-triangle modal-custom_icon color-red"></i>
        <div class="ml-4">
            <h3 class="fs-18 mb-1 font-family-w6">メールの送信エラー</h3>
            <p class="m-0">
                送信先にアンケートを送信することができませんでした。<br>
                @if(!empty($failEmail) && is_array($failEmail))
                    "{{ implode(', ', $failEmail) }}"<br>
                @endif
                メールアドレスを確認して、もう一度送信してみてください。
            </p>
        </div>
    </div>
</div>
