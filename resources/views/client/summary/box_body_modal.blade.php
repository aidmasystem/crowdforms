@if($summary->isNotEmpty())
    @foreach($summary as $row)
        <tr>
            <td>{{ @$row->title }}</td>
            <td>{{ $row->total_success . '/' . $row->total_send }}人</td>
            <td>{{ $row->total_fail > 0 ? '送信エラー' : '送信完了' }}</td>
            <td class="px-3">
                <a href="javascript:void(0);" data-id="{{ $row->survey_id }}"
                   class="btn-custom btn-custom-primary w-100 fs-14 py-1 {{ $row->total_fail == 0 ? 'disable' : '' }} resend-email_survey">再送信</a>
            </td>
        </tr>
    @endforeach
@endif
