<div class="modal-header mb-4">
    <h2 class="title title-large mb-0"><i class="fas fa-paper-plane main_icon"></i>送信状況の確認</h2>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body p-0 form-custom">
    <h3 class="modal-body_title fs-14 font-family-w6 d-inline-block"><i
            class="fs-16 fas fa-envelope mr-1"></i>メール</h3>
    <div class="modal-body_content main_table">
        <div class="d-flex form-custom_search mb-3">
            <input type="text" class="form-control input-search" id="key-modal_summary" value="">
            <a href="javascript:void(0);" id="modal-search_summary" class="btn-custom btn-custom-primary">検索</a>
        </div>
        <div class="table-custom-responsive style-scroll">
            <table class="table table-bordered table-custom-3 text-center mb-0 form-custom">
                <thead>
                <tr>
                    <th scope="col">送信アンケート<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                                   class="img-fluid mb-1 sort-modal_summary" id="title_asc"></span></th>
                    <th scope="col">送信完了<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                                class="img-fluid mb-1 sort-modal_summary" id="total_asc"></span></th>
                    <th scope="col">状況<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                              class="img-fluid mb-1 sort-modal_summary" id="send_asc"></span></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody id="body-modal_summary">
                @if($summary->isNotEmpty())
                    @foreach($summary as $row)
                        <tr>
                            <td>{{ $row->title }}</td>
                            <td>{{ $row->total_success . '/' . $row->total_send }}人</td>
                            <td>{{ $row->total_fail > 0 ? '送信エラー' : '送信完了' }}</td>
                            <td class="px-3">
                                <a href="javascript:void(0);" data-id="{{ $row->survey_id }}"
                                   class="btn-custom btn-custom-primary w-100 fs-14 py-1
                                   {{ $row->total_fail == 0 ? 'disable' : '' }} resend-email_survey">再送信</a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
