@extends('layout_login')

@section('content')
    <div class="container-custom main_content">
        <!-- content-->
        <div class="d-flex align-items-center justify-content-between mb-30">
            <h2 class="title title-large"><i class="fas fa-edit main_icon"></i>アンケートを回答する</h2>
        </div>
        @if(@$success || $success = Session::get('success'))
            <div class="survey-success card-custom mb-30">
                <div class="alert-success" role="alert">
                    <p class="mb-0">{{ $success }}</p>
                </div>
            </div>
        @endif
        @if(@$error)
            <div class="survey-error card-custom mb-30">
                <div class="alert-danger" role="alert">
                    <p class="mb-0">{{ $error }}</p>
                </div>
            </div>
        @endif

        @if(!empty($survey))
            <div class="card-custom mb-30">
                <p class="font-family-w6">アンケートタイトル</p>
                <h3 class="title-large fs-24 px-3 mb-3">{{ @$survey['title'] }}</h3>
                <p class="font-family-w6">アンケート詳細</p>
                <h4 class="fs-16 mb-0 px-3">{!! nl2br(@$survey['descriptions']) !!}</h4>
            </div>
            <!--list question-->
            <form action="{{ route('survey.customerSubmit') }}" method="POST" id="frm" class="form-custom">
                @csrf
                <div class="mb-30">
                    @if($errors->has('question'))
                        <small class="form-text text-error">{{ $errors->first('question') }}</small>
                    @endif
                    @if(!empty($survey['question']))
                        @foreach($survey['question'] as $key => $row)
                            <div class="card-custom mb-3 form-custom">
                                <h4 class="fs-14 font-family-w6 mb-3 th-header">{{ $row['content'] }}@if($row['is_obligatory'])
                                        <span
                                            class="card-table_label font-family-w3 ml-1">必須</span>@endif
                                    @if($errors->has('question.' . $row['id']))
                                        <span class="form-text text-error ml-1">{{ $row['type'] == \App\Models\Question::TYPE_ESSAY ? '回答を入力してください' : '回答を選択してください' }}</span>
                                    @endif
                                </h4>
                                <input type="hidden" name="obligatory[{{ $row['id'] }}]" value="{{ $row['is_obligatory'] }}">
                                <input type="hidden" name="type[{{ $row['id'] }}]" value="{{ $row['type'] }}">
                                @if($row['type'] == \App\Models\Question::TYPE_ESSAY)
                                    <div class="form-custom_select">
                                        <textarea class="form-control col-8" placeholder="入力してください。"
                                                  name="question[{{ $row['id'] }}]" id="" cols="30" row
                                                  maxlength="512" row-customs="10">{{ str_replace('　', '', old('question.'.$row['id'])) }}</textarea>
                                    </div>
                                @elseif($row['type'] == \App\Models\Question::TYPE_RADIO)
                                    @if(!empty($row['answer']))
                                        @foreach($row['answer'] as $item)
                                            <div class="form-check box-input-check d-flex mb-2">
                                                <div class="input-check">
                                                    <input class="form-check-input large" type="radio"
                                                           name="question[{{ $row['id'] }}]"
                                                           value="{{ $item['id'] }}" {{ old('question.'.$row['id']) == $item['id'] ? 'checked' : '' }}>
                                                </div>
                                                <label class="form-check-label" for="">{{ $item['content'] }}</label>
                                            </div>
                                        @endforeach
                                    @endif
                                @elseif($row['type'] == \App\Models\Question::TYPE_CHECKBOX)
                                    @if(!empty($row['answer']))
                                        @foreach($row['answer'] as $item)
                                            <div class="form-check box-input-check d-flex mb-2">
                                                <div class="input-check">
                                                    <input class="form-check-input large" type="checkbox"
                                                           name="question[{{ $row['id'] }}][]"
                                                           value="{{ $item['id'] }}" {{ @old('question.'.$row['id']) && in_array($item['id'], old('question.'.$row['id'])) ? 'checked' : '' }}>
                                                </div>
                                                <label class="form-check-label" for="">{{ $item['content'] }}</label>
                                            </div>
                                        @endforeach
                                    @endif
                                @else
                                    <div class="form-custom_select select-pulldown col-3 p-0">
                                        <select class="selectType" name="question[{{ $row['id'] }}]"title="選択してください">
                                            @if(!empty($row['answer']))
                                                @foreach($row['answer'] as $item)
                                                    <option value="{{ $item['id'] }}" {{ old('question.'.$row['id']) == $item['id'] ? 'selected' : '' }}>{{ $item['content'] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                @endif
                            </div>
                        @endforeach
                    @endif
                </div>
                <input type="hidden" name="question_package_id" value="{{ @$survey['id'] }}">
                <input type="hidden" name="email" value="{{ @$email }}">
                @if(!@$preview)
                    <div class="d-flex justify-content-center">
                        <button type="submit" id="btn-submit" class="btn-custom btn-custom-primary md btn-custom-large">送信</button>
                    </div>
                @endif
            </form>
        @endif
    </div>
@endsection
