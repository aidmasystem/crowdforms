<header class="header">
    <div class="header_content">
        <div class="container-custom">
            <div class="header_logo"><img src="{{ asset('images/logo.svg') }}" alt="" class="img-fluid"></div>
        </div>
    </div>
    <!--navbar-->
    <nav class="nav">
        <div class="container-custom px-0 d-flex justify-content-between align-items-center">
            <ul class="nav_list d-flex">
                <li class="nav_item">
                    <a class="nav_link {{ request()->segment(1) == 'survey' ? 'active' : '' }}" href="{{ route('client.survey.index') }}"><i class="fas fa-edit main_icon"></i>アンケート一覧</a>
                </li>
                <li class="nav_item">
                    <a class="nav_link {{ request()->segment(1) == 'customer' ? 'active' : '' }}" href="{{ route('client.customer.index') }}"><i class="fas fa-paper-plane main_icon"></i>送信先一覧</a>
                </li>
                <li class="nav_item">
                    <a class="nav_link {{ request()->segment(1) == 'statistic' ? 'active' : '' }}" href="{{route('client.statistic.index')}}"><i class="fas fa-clipboard-check main_icon"></i>アンケート結果集計</a>
                </li>
            </ul>
            <div class="dropdown">
                <div class="dropdown-toggle" id="dropdownAddress" data-toggle="dropdown" aria-haspopup="true"
                     aria-expanded="false">
                    <div class="line-clamp">
                        <p class="company-name_header">{{ getCompanyName() }}</p>
                        {{ getFullName() }}
                    </div>
                    <span class="dropdown_icon"></span>
                </div>
                <div class="dropdown-menu" aria-labelledby="dropdownAddress">
                    <a class="dropdown-item" href="{{route('client.employee.index')}}"><i class="fas fa-user main_icon"></i>アカウント管理</a>
                    <a class="dropdown-item" href="{{ route('logout') }}"><i class="fas fa-sign-out-alt main_icon"></i>ログアウト</a>
                </div>
            </div>
        </div>
    </nav>
</header>
