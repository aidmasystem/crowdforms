@extends('layout_login')

@section('content')
    <div class="card-custom card-auth">
        <h2 class="card-auth_title text-center">ログイン</h2>
        <form action="{{ route('checkLogin') }}" method="POST" class="form-custom" id="frm">
            @csrf
            <div class="form-group">
                @if($errors->has('email'))
                    <small class="form-text text-error">{{ $errors->first('email') }}</small>
                @endif
                <input type="text" name="email" class="form-control @if($errors->has('email')) border-error @endif"
                       value="{{ old('email') }}" placeholder="メールアドレスを入力してください">
            </div>
            <div class="form-group">
                @if($errors->has('password'))
                    <small class="form-text text-error">{{ $errors->first('password') }}</small>
                @endif
                <input type="password" name="password"
                       class="form-control @if($errors->has('password')) border-error @endif"
                       placeholder="パスワードを入力してください">
            </div>
            <p class="text-center mb-0">
                <a href="{{ route('forgot_pass') }}" class="card-auth_link">パスワードを忘れた場合</a>
            </p>
            <button type="submit" id="btn-submit" class="w-100 btn-custom btn-custom-primary btn-custom-medium">ログイン</button>
        </form>
    </div>
@endsection

