<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>パスワードのリセットを確認してください</title>
</head>
<body>
<p>−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−</p>
<p>アカウントのパスワードリマインダー処理を行います。</p>
<p>登録されたメールアドレスにURLお送りします。</p>
<p>−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−</p>
<p>▼アカウント登録情報▼</p><br>
<p>認証用URL :</p>
<p>{{ $data['link']}} </p><br>
URLは1日だけ有効です。
</body>
</html>
