@extends('layout_login')

@section('content')
    <div class="card-custom card-auth large">
        <h2 class="card-auth_title text-center mb-3">パスワード再設定完了</h2>
        <p class="text-center mb-30 fs-12">
            新しいパスワードに変更しました。<br>
            メールアドレスと先程設定した新しいパスワードでログインしてください。
        </p>
        <div class="px-5"><a href="{{ route('login') }}" class="w-100 btn-custom btn-custom-primary btn-custom-medium">ログイン画面へ</a></div>
    </div>
@endsection
