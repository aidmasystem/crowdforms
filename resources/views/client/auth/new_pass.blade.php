@extends('layout_login')

@section('content')
    <!--reset password-->
    <div class="card-custom card-auth large">
        <h2 class="card-auth_title text-center mb-3">パスワード再設定</h2>
        <p class="text-center mb-20 fs-12">
            新しいパスワードを入力してください。 <br>
            登録したメールアドレス宛にパスワードを送信します。
        </p>
        <form action="{{ route('reset_pass') }}" method="POST" id="frm" class="form-custom">
            @csrf
            <div class="row align-items-center mb-20">
                <div class="col-3">
                    <label for="" class="mb-0 fs-12">メールアドレス</label>
                </div>
                <div class="form-group col-9 mb-0">
                    @if($errors->has('email'))
                        <small class="form-text text-error pl-2">{{ $errors->first('email') }}</small>
                    @endif
                    <input type="text" name="email" readonly class="form-control @if($errors->has('email')) border-error @endif" value="{{ @$email }}">
                </div>
            </div>
            <div class="row align-items-center mb-20">
                <div class="col-3">
                    <label for="" class="mb-0 fs-12">変更パスワード</label>
                </div>
                <div class="form-group col-9 mb-0">
                    @if($errors->has('password'))
                        <small class="form-text text-error pl-2">{{ $errors->first('password') }}</small>
                    @endif
                    <input type="password" name="password" class="form-control @if($errors->has('password')) border-error @endif" placeholder="パスワードを入力してください">
                </div>
            </div>
            <div class="row align-items-center mb-30">
                <div class="col-3">
                    <label for="" class="mb-0 fs-12">確認パスワード</label>
                </div>
                <div class="form-group col-9 mb-0">
                    @if($errors->has('confirm-password'))
                        <small class="form-text text-error pl-2">{{ $errors->first('confirm-password') }}</small>
                    @endif
                    <input type="password" name="confirm-password" class="form-control @if($errors->has('confirm-password')) border-error @endif" placeholder="パスワードを入力してください">
                </div>
            </div>
            <div class="px-5"><button type="submit" id="btn-submit" class="w-100 btn-custom btn-custom-primary btn-custom-medium">変更する</button></div>
        </form>
    </div>
    <!--complete-->
@endsection
