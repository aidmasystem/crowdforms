@extends('client.layouts.master')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large"><i class="fas fa-plus main_icon"></i>アンケート新規登録</h2>
    </div>

    <form action="{{ route('client.survey.store') }}" method="POST" class="form-custom" id="form-create-survey">
        @csrf
        <div id="box-form-create">
            <div class="card-custom mb-30 card-custom-w">
                <div class="row flex-wrap">
                    <div class="form-group col-8">
                        <label class="font-family-w6 fs-14" for="">アンケートタイトル<span
                                class="card-table_label font-family-w3 ml-1">必須</span></label>
                        <small class="font-family-w3 color-red error-mgs-title"></small>
                        <input type="text" name="title" maxlength="255"
                               class="form-control large input-title-survey"
                               value="{{ old('title') }}" placeholder="無題のアンケート"/>
                    </div>
                    <div class="form-group col-8 mb-0">
                        <label class="font-family-w6 fs-14" for="">アンケート詳細</label>
                        <small class="font-family-w3 color-red error-mgs-des"></small>
                        <textarea class="form-control description-survey" placeholder="アンケート詳細を入力してください" name="descriptions" id=""
                                  maxlength="512" cols="30" row row-customs="10">{{ '  ' ? '' : old('descriptions') }}</textarea>
                    </div>
                </div>
            </div>
            <div class="position-relative mb-2">
                <div class="card-custom_action" style="float: right">
                    <a href="javascript:void(0);" class="btn-custom" id="add-form-question"><i
                            class="fas fa-plus-circle"></i></a>
                    <a href="javascript:void(0);" class="btn-custom" id="preview-form-question"><i
                            class="fas fa-eye"></i></a>
                </div>
                <div id="sortable" class="all-form-question mb-4">
                    <div class="card-custom-w">
                        <div class="card-custom mb-3 p-0 position-relative main_card parent-question" data-key="1">
                            <span class="icon-drag"></span>
                            <button class="pb-2 px-4 main_card_btn first" disabled>
                                <label class="font-family-w6 fs-14" for="">質問内容</label>
                                <small
                                    class="font-family-w3 color-red error-mgs-content"></small>
                                <div class="row row-custom">
                                    <div class="col-5 col-custom">
                                        <input type="text" name="question[1][content]"
                                            class="form-control input-question" maxlength="512"
                                            placeholder="質問内容を入力してください"/>
                                    </div>
                                    <div class="col-3 col-custom">
                                        <div class="form-custom_select">
                                            <select name="question[1][type]" class="change_value_type selectType">
                                                @foreach($type as $key => $row)
                                                    <option
                                                        value="{{ $key }}" {{ $key == \App\Models\Question::TYPE_RADIO ? 'selected' : '' }}>{{ $row }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div class="form-answers">
                                <div class="card-custom_item box-question-radio">
                                    <div class="box-body-radio sortable_answer">
                                        <div class="box-items-answer">
                                            <span class="icon-drag move-drag-answer drag-first"></span>
                                            <button class="d-flex align-items-center px-4 pb-2 main_card_btn" disabled>
                                                <div class="form-check">
                                                    <input class="form-check-input large" type="radio" name="exampleRadios"
                                                        value="option1">
                                                </div>
                                                <div class="row row-custom w-100">
                                                    <div class="col-5 col-custom">
                                                        <input type="text" name="question[1][answer][]"
                                                            class="form-control input-answer"
                                                            maxlength="255" placeholder="選択肢1"/>
                                                    </div>
                                                    <div class="col-custom text-answer-error">
                                                        <small class="font-family-w3 color-red error-mgs-answer"></small>
                                                    </div>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                    <button class="d-flex pb-3 main_card_btn px-4" disabled>
                                        <a href="javascript:void(0);" data-id="0"
                                        class="btn-custom btn-custom-outlined add-content-radio">選択肢を追加</a>
                                        <a href="javascript:void(0);" data-id="0"
                                        class="btn-custom btn-custom-outlined ml-2 add-other-radio other-answer">「その他」を追加</a>
                                    </button>
                                </div>
                            </div>
                            <button class="px-4 pb-4 main_card_btn last" disabled>
                                <div class="card-custom_footer d-flex align-items-center justify-content-end">
                                    <a href="javascript:void(0);"
                                    class="btn-custom btn-custom-outlined mr-2 copy-form-question"><i
                                            class="fas fa-copy main_icon large m-0"></i></a>
                                    <a href="javascript:void(0);"
                                    class="btn-custom btn-custom-outlined delete-form-question"><i
                                            class="fas fa-trash-alt main_icon large m-0"></i></a>
                                    <label class="ml-3 mr-1 mb-0">必須</label>
                                    <input type="checkbox" name="question[1][is_obligatory]" value="1"
                                           class="main_switch switch-question_required"/>
                                </div>
                            </button>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="length-key" value="1">
            </div>
        </div>
        <div id="box-form-preview"></div>

        <div class="d-flex justify-content-center">
            <a href="{{ route('client.survey.index') }}" id="back-index"
               class="btn-custom btn-custom-disabled md mx-4 btn-custom-large">キャンセル</a>
            <a href="javascript:void(0);" class="btn-custom btn-custom-disabled md mx-4 btn-custom-large"
               id="back-create">戻る</a>
            <button type="submit" id="btn-submit_survey" class="btn-custom btn-custom-primary md mx-4 btn-custom-large">登録</button>
        </div>
    </form>
@endsection
