@if($lists->isNotEmpty())
    @foreach($lists as $row)
        <tr>
            <td class="main_table_action large">
                <a href="{{ route('client.survey.show', $row->id) }}" class="btn-custom btn-custom-outlined">詳細</a>
            </td>
            <td class="main_table_action"><a href="javascript:void(0);" data-id="{{ $row->id }}" data-url="{{ route('client.survey.destroy', $row->id) }}"
                                             class="delete-survey {{ $row->check ? 'disable' : '' }}"
                                             data-toggle="modal" data-target="#deleteAdmin">
                    <i class="fas fa-trash-alt main_icon m-0"></i>
                </a>
            </td>
            <td class="main_table_action">
                <a href="{{ route('client.survey.edit', $row->id) }}" @if($row->check) class="disable" @endif><i class="fas fa-pen main_icon m-0"></i></a>
            </td>
            <td class="text-overflow title-survey" data-toggle="tooltip" title="{{ $row->title }}">{{ $row->title }}</td>
            <td class="large text-overflow" data-toggle="tooltip" title="{{ $row->descriptions }}">{{ $row->descriptions }}</td>
            <td class="text-nowrap">{{ $row->created_at }}</td>
            <td class="text-overflow title-survey">{{ @$row->full_name_create }}</td>
            <td class="text-nowrap">{{ $row->updated_at }}</td>
            <td class="text-overflow title-survey">{{ @$row->full_name_update }}</td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="9">
            <div class="text-center">データがありません。</div>
        </td>
    </tr>
@endif
