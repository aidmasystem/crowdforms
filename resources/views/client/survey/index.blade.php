@extends('client.layouts.master')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large"><i class="fas fa-edit main_icon"></i>アンケート一覧</h2>
        <a href="{{ route('client.survey.create') }}" class="btn-custom btn-custom-warning md"><i
                class="fas fa-plus main_icon text-white"></i>アンケート新規登録</a>
    </div>
    <div class="card-custom mb-30">
        <form action="" class="form-custom">
            <div class="form-group">
                <div class="d-flex form-custom_search">
                    <input type="text" name="keyword" value="{{ request()->keyword }}" class="form-control input-search"
                           placeholder="検索したいワードを入力してください">
                    <i class="fas fa-search"></i>
                    <button type="submit" class="btn-custom btn-custom-primary">検索</button>
                </div>
                <small class="form-text color-red"></small>
            </div>
            <div class="d-flex">
                <div class="form-group mb-0 mr-5">
                    <label class="font-weight-bold fs-14" for="">作成日時</label>
                    <div class="d-flex align-items-center">
                        <div class='input-group form-custom_date' id='datepicker'>
                            <input type='text' name="from_date" value="{{ request()->from_date }}"
                                   class="form-control" placeholder="年/月/日"/>
                            <span class="input-group-addon"></span>
                        </div>
                        <span class="icon">~</span>
                        <div class='input-group form-custom_date' id='datepicker1'>
                            <input type='text' name="to_date" value="{{ request()->to_date }}"
                                   class="form-control" placeholder="年/月/日"/>
                            <span class="input-group-addon"></span>
                        </div>
                    </div>
                    <small class="form-text color-red"></small>
                </div>
                <div class="form-group mb-0 w-25">
                    <label class="font-weight-bold fs-14" for="">作成者</label>
                    <div class="form-custom_select multi">
                        <select class="selectpicker select_author"
                                name="group_author[]"
                                data-live-search="true"
                                data-actions-box="true"
                                data-live-search-placeholder="検索"
                                data-select-all-text="全て"
                                data-deselect-all-text="全て"
                                data-selected-text-format="count > 3"
                                data-count-selected-text= "{0}人選択されました。"
                                title="選択してください"
                                multiple>
                            @if($authors->isNotEmpty())
                                @if(!empty(@request()->group_author) && is_array(@request()->group_author))
                                    <optgroup label="選択中">
                                        @foreach($authors as $key => $row)
                                            @if(in_array($row->id, @request()->group_author))
                                                <option class="option-select-survey text-overflow" value="{{ $row->id }}" selected>{{ $row->full_name }}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                @endif
                                @if(@count(@request()->group_author) != count($authors))
                                    <optgroup label="候補">
                                        @foreach($authors as $key => $row)
                                            @if(!in_array($row->id, is_array(@request()->group_author) ? request()->group_author : []))
                                                <option class="option-select-survey text-overflow" value="{{ $row->id }}">{{ $row->full_name }}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                @endif
                            @endif
                        </select>
                    </div>
                    <small class="form-text color-red"></small>
                </div>
            </div>
        </form>
    </div>

    <!-- alert -->
    @if (@$message)
        <div class="alert alert-success" role="alert">
            <p class="mb-0">{{ $message }}</p>
        </div>
    @endif
    @if ($success = Session::get('success'))
        <div class="alert alert-success" role="alert">
            <p class="mb-0">{{ $success }}</p>
        </div>
    @endif
    <!--table-->
    <div class="main_table">
        <div class="d-flex justify-content-between">
            <p class="fs-20">件数 {{ @$count }}件</p>
            <ul class="pagination">
                {!! $lists->appends($_GET)->links() !!}
            </ul>
        </div>
        <div class="table-responsive mb-3">
            <table class="table table-bordered table-custom text-center mb-0">
                <thead>
                <tr>
                    <th scope="col" class="main_table_action large">詳細</th>
                    <th scope="col" class="main_table_action">削除</th>
                    <th scope="col" class="main_table_action">編集</th>
                    <th scope="col">アンケート名<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                                  class="img-fluid mb-1 sort-heading" id="title" data-sort="asc"></span></th>
                    <th scope="col">アンケート詳細</th>
                    <th scope="col">作成日時<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                                class="img-fluid mb-1 sort-heading" id="createdAt" data-sort="asc"></span></th>
                    <th scope="col">作成者<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                               class="img-fluid mb-1 sort-heading" id="nameCreate" data-sort="asc"></span></th>
                    <th scope="col">更新日<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                               class="img-fluid mb-1 sort-heading" id="updatedAt" data-sort="asc"></span></th>
                    <th scope="col">更新者<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                               class="img-fluid mb-1 sort-heading" id="nameUpdate" data-sort="asc"></span></th>
                </tr>
                </thead>
                <tbody>
                @if($lists->isNotEmpty())
                    @foreach($lists as $row)
                        <tr>
                            <td class="main_table_action large">
                                <a href="{{ route('client.survey.show', $row->id) }}" class="btn-custom btn-custom-outlined">詳細</a>
                            </td>
                            <td class="main_table_action"><a href="javascript:void(0);" data-id="{{ $row->id }}" data-url="{{ route('client.survey.destroy', $row->id) }}"
                                                             class="delete-survey {{ $row->check ? 'disable' : '' }}"
                                                             data-toggle="modal" data-target="#deleteAdmin">
                                    <i class="fas fa-trash-alt main_icon m-0"></i>
                                </a>
                            </td>
                            <td class="main_table_action">
                                <a href="{{ route('client.survey.edit', $row->id) }}" @if($row->check) class="disable" @endif><i class="fas fa-pen main_icon m-0"></i></a>
                            </td>
                            <td class="text-overflow title-survey" data-toggle="tooltip" title="{{ $row->title }}">{{ $row->title }}</td>
                            <td class="large text-overflow" data-toggle="tooltip" title="{{ $row->descriptions }}">{{ $row->descriptions }}</td>
                            <td class="text-nowrap">{{ $row->created_at }}</td>
                            <td class="text-overflow title-survey">{{ @$row->full_name_create }}</td>
                            <td class="text-nowrap">{{ $row->updated_at }}</td>
                            <td class="text-overflow title-survey">{{ @$row->full_name_update }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9">
                            <div class="text-center">データがありません。</div>
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
        <div class="d-flex justify-content-between mt-2">
            <p class="fs-20">件数 {{ @$count }}件</p>
            <ul class="pagination">
                {!! $lists->appends($_GET)->links() !!}
            </ul>
        </div>

        <input type="hidden" name="hidden_page" id="hidden_page" value="1"/>
        <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="" />
        <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="" />
    </div>

    <div class="modal fade modal-custom" id="deleteAdmin" role="dialog" aria-labelledby="deleteAdmin" aria-hidden="true">
        <div class="delete modal-dialog modal-confirm">
            <div class="modal-content">
                <form action="#" method="POST" id="form_delete_survey">
                    @csrf
                    @method('DELETE')
                    <div class="modal-header mb-3">
                        <h3 class="title title-large mb-0"><i class="mr-2 fas fa-exclamation-triangle fs-20 color-red"></i>削除</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body p-0 mb-3 text-center fs-16">
                        <p>削除しますがよろしいですか</p>
                    </div>
                    <div class="modal-footer justify-content-center p-0 mb-1">
                        <button type="button" class="btn-custom btn-custom-disabled w-50" data-dismiss="modal">キャンセル</button>
                        <button type="submit" class="btn-custom btn-custom-danger w-50">削除</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function () {
            $('select.select_author').on('change', function () {
                if ($('.select_author optgroup[label="選択中"]').length === 0) $('select.select_author').prepend(`<optgroup label="選択中"></optgroup>`);
                if ($('.select_author optgroup[label="候補"]').length === 0) $('select.select_author').append(`<optgroup label="候補"></optgroup>`);
            })
            var select_all = $('.select_author button.bs-select-all'),
                deselect_all = $('.select_author button.bs-deselect-all')
            $('.selectpicker').on('change', function () {
                check_cookie()
                let $pending = $('.select_author optgroup[label="候補"]')
                let $selected = $('.select_author optgroup[label="選択中"]');

                $pending.find("option:selected").each(function () {
                    let $clone = $(this).clone()
                    $(this).remove()
                    $selected.append($clone.attr('selected', true))
                })
                $selected.find("option:not(:selected)").each(function () {
                    if ($pending.length){
                        let $clone = $(this).clone()
                        $(this).remove()
                        $pending.append($clone.attr('selected', false))
                    }
                })
                if($selected.children('option').length === 0) $selected.remove()
                if($pending.children('option').length === 0){
                    $pending.remove()
                    select_all.addClass('d-none')
                    deselect_all.addClass('d-block')
                } else {
                    select_all.removeClass('d-none')
                    deselect_all.removeClass('d-block')
                }
                $('.select_author').selectpicker('refresh')
            })
            if ($(`.select_author optgroup[label="候補"]`).length){
                select_all.removeClass('d-none')
                deselect_all.removeClass('d-block')
            }else {
                select_all.addClass('d-none')
                deselect_all.addClass('d-block')
            }

            $(document).on('click', '.delete-survey', function() {
                let url = $(this).data('url');
                $('#form_delete_survey').attr('action', url);
            });

            $(".sort-heading").click(function (e) {
                e.preventDefault();
                let sort_name = $(this).attr('id');
                let sort_by = $(this).data('sort');
                clearIcon(sort_name);

                let keyword = '{{ request()->keyword }}';
                let from_date = '{{ request()->from_date }}';
                let to_date = '{{ request()->to_date }}';
                let group_author = '{!! @json_encode(request()->group_author) !!}';

                let page = 1;
                $('#hidden_column_name').val(sort_name);
                $('#hidden_sort_type').val(sort_by);

                fetchData(page, sort_name, sort_by, keyword, from_date, to_date, group_author);
            });

            $(document).on('click', '.pagination a', function(e){
                e.preventDefault();
                let page = $(this).attr('href').split('page=')[1];
                $('#hidden_page').val(page);
                var column_name = $('#hidden_column_name').val();
                var sort_by = $('#hidden_sort_type').val();
                let keyword = '{{ request()->keyword }}';
                let from_date = '{{ request()->from_date }}';
                let to_date = '{{ request()->to_date }}';
                let group_author = '{!! @json_encode(request()->group_author) !!}';

                fetchData(page, column_name, sort_by, keyword, from_date, to_date, group_author);
            });

            function fetchData(page, column_name, sort_by, keyword, from_date, to_date, group_author) {
                let getId = column_name;
                let sortBy = sort_by;
                $.ajax({
                    url: '{{ route('client.survey.index') }}',
                    type: 'GET',
                    data: {
                        page: page,
                        column: column_name,
                        sortOrder: sort_by,
                        keyword: keyword,
                        from_date: from_date,
                        to_date: to_date,
                        group_author: group_author
                    },
                    beforeSend: showLoad(),
                    success:function(data) {
                        hideLoad();
                        if (sortBy == 'asc') {
                            $('#' + getId).data('sort', 'desc');
                            $('#' + getId).attr('src', '{{ asset('images/icons/sort-asc.svg') }}');
                        } else if (sortBy == 'desc') {
                            $('#' + getId).data('sort', 'all');
                            $('#' + getId).attr('src', '{{ asset('images/icons/sort-desc.svg') }}');
                        } else if (sortBy == 'all') {
                            $('#' + getId).data('sort', 'asc');
                            $('#' + getId).attr('src', '{{ asset('images/icons/sort.svg') }}');
                        }

                        $('ul.pagination').html(data.paginate);
                        $(".table tr:not(:first)").remove();
                        $(".table").append(data.data);
                        // window.history.pushState("", "", this.url);
                    }
                });
            }
        });
    </script>
@endpush
