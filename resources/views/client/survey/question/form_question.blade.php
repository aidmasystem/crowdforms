<div class="card-custom-w">
    <div class="card-custom mb-3 p-0 position-relative main_card parent-question" data-key="{{ $number }}">
        <span class="icon-drag"></span>
        <button class="pb-2 px-4 main_card_btn first" disabled>
            <label class="font-family-w6 fs-14" for="">質問内容</label>
            <small class="font-family-w3 color-red error-mgs-content"></small>
            <div class="row row-custom">
                <div class="col-5 col-custom">
                    <input type="text" name="question[{{ $number }}][content]" class="form-control input-question"
                           maxlength="512" placeholder="質問内容を入力してください"/>
                </div>
                <div class="col-3 col-custom">
                    <div class="form-custom_select">
                        <select name="question[{{ $number }}][type]" class="change_value_type selectType">
                            @foreach($type as $key => $row)
                                <option value="{{ $key }}" {{ $key == \App\Models\Question::TYPE_RADIO ? 'selected' : '' }}>{{ $row }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </button>
        <div class="form-answers">
            <div class="card-custom_item box-question-radio">
                <div class="box-body-radio sortable_answer">
                    <div class="box-items-answer">
                        <span class="icon-drag move-drag-answer drag-first"></span>
                        <button class="d-flex align-items-center px-4 pb-2 main_card_btn" disabled>
                            <div class="form-check">
                                <input class="form-check-input large" type="radio" name="exampleRadios" value="option1">
                            </div>
                            <div class="row row-custom w-100">
                                <div class="col-5 col-custom">
                                    <input type="text" name="question[{{ $number }}][answer][]" maxlength="255"
                                           class="form-control input-answer" placeholder="選択肢1"/>
                                </div>
                                <div class="col-custom text-answer-error">
                                    <small class="font-family-w3 color-red error-mgs-answer"></small>
                                </div>
                            </div>
                        </button>
                    </div>
                </div>
                <button class="d-flex pb-3 main_card_btn px-4" disabled>
                    <a href="javascript:void(0);" class="btn-custom btn-custom-outlined add-content-radio">選択肢を追加</a>
                    <a href="javascript:void(0);" class="btn-custom btn-custom-outlined ml-2 add-other-radio other-answer">「その他」を追加</a>
                </button>
            </div>
        </div>
        <button class="px-4 pb-4 main_card_btn last" disabled>
            <div class="card-custom_footer d-flex align-items-center justify-content-end">
                <a href="javascript:void(0);" class="btn-custom btn-custom-outlined mr-2 copy-form-question"><i
                        class="fas fa-copy main_icon large m-0"></i></a>
                <a href="javascript:void(0);" class="btn-custom btn-custom-outlined delete-form-question"><i
                        class="fas fa-trash-alt main_icon large m-0"></i></a>
                <label class="ml-3 mr-1 mb-0">必須</label>
                <input type="checkbox" name="question[{{ $number }}][is_obligatory]" value="1" class="main_switch switch-question_required"/>
            </div>
        </button>
    </div>
</div>
