<div class="card-custom_item box-question-radio">
    <div class="box-body-radio sortable_answer">
        <div class="box-items-answer">
            <span class="icon-drag move-drag-answer drag-first"></span>
            <button class="d-flex align-items-center px-4 pb-2 main_card_btn" disabled>
                <div class="form-check">
                    <input class="form-check-input large" type="radio" name="exampleRadios" value="option1">
                </div>
                <div class="row row-custom w-100">
                    <div class="col-5 col-custom">
                        <input type="text" name="question[{{ $key }}][answer][]" class="form-control input-answer"
                               maxlength="255" placeholder="選択肢1"/>
                    </div>
                    <div class="col-custom text-answer-error">
                        <small class="font-family-w3 color-red error-mgs-answer"></small>
                    </div>
                </div>
            </button>
        </div>
    </div>
    <button class="d-flex pb-3 main_card_btn px-4" disabled>
        <a href="javascript:void(0);" class="btn-custom btn-custom-outlined add-content-radio">選択肢を追加</a>
        <a href="javascript:void(0);" class="btn-custom btn-custom-outlined ml-2 add-other-radio">「その他」を追加</a>
    </button>
</div>
