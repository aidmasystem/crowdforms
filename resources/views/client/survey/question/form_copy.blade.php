<div class="card-custom-w">
    <div class="card-custom mb-3 p-0 position-relative main_card parent-question" data-key="{{ $key }}">
        <span class="icon-drag"></span>
        <button class="pb-2 px-4 main_card_btn first" disabled>
            <label class="font-family-w6 fs-14" for="">質問内容
                {!! @$isObligatory ? '<span class="card-table_label font-family-w3 ml-1">必須</span>' : ''  !!}
            </label>
            <small class="font-family-w3 color-red error-mgs-content"></small>
            <div class="row row-custom">
                <div class="col-5 col-custom">
                    <input type="text" name="question[{{ $key }}][content]" value="{{ @$content }}" maxlength="512"
                           class="form-control input-question" placeholder="質問内容を入力してください"/>
                </div>
                <div class="col-3 col-custom">
                    <div class="form-custom_select">
                        <select name="question[{{ $key }}][type]" class="change_value_type selectType">
                            @foreach($type as $k => $row)
                                <option
                                    value="{{ $k }}" {{ $k == @$typeQuestion ? 'selected' : '' }}>{{ $row }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </button>
        <div class="form-answers">
            @if(!empty($answer))
                @if($typeQuestion == \App\Models\Question::TYPE_RADIO)
                    <div class="card-custom_item box-question-radio">
                        <div class="box-body-radio sortable_answer">
                            @foreach($answer as $k => $row)
                                <div class="box-items-answer">
                                    <span class="icon-drag move-drag-answer {{ $k == 0 ? ' drag-first' : '' }}" @if($k == 0 && count($answer) > 1) style="display: block" @endif></span>
                                    <button class="d-flex align-items-center px-4 pb-2 main_card_btn" disabled>
                                        <div class="form-check">
                                            <input class="form-check-input large" type="radio" name="exampleRadios"
                                                   value="option1">
                                        </div>
                                        <div class="row row-custom w-100">
                                            <div class="col-5 col-custom col-answer">
                                                @if($row === 'is_other_answer')
                                                    その他<input type="hidden" name="question[{{ $key }}][answer][]"
                                                              value="is_other_answer"/>
                                                @else
                                                    <input type="text" name="question[{{ $key }}][answer][]"
                                                           value="{{ $row }}" maxlength="255"
                                                           class="form-control input-answer"
                                                           placeholder="選択肢{{ $k + 1 }}"/>
                                                @endif
                                            </div>
                                            <div class="col-custom text-answer-error">
                                                <small class="font-family-w3 color-red error-mgs-answer"></small>
                                            </div>
                                        </div>
                                        @if($k > 0) <span class="close"></span> @endif
                                    </button>
                                </div>
                            @endforeach
                        </div>
                        <button class="d-flex pb-3 main_card_btn px-4" disabled>
                            <a href="javascript:void(0);" class="btn-custom btn-custom-outlined add-content-radio">選択肢を追加</a>
                            <a href="javascript:void(0);" class="btn-custom btn-custom-outlined ml-2 add-other-radio other-answer @if(@$isOther) disable @endif">「その他」を追加</a>
                        </button>
                    </div>
                @elseif($typeQuestion == \App\Models\Question::TYPE_CHECKBOX)
                    <div class="card-custom_item box-question-checkbox">
                        <div class="box-body-checkbox sortable_answer">
                            @foreach($answer as $k => $row)
                                <div class="box-items-answer">
                                    <span class="icon-drag move-drag-answer {{ $k == 0 ? ' drag-first' : '' }}"  @if($k == 0 && count($answer) > 1) style="display: block" @endif></span>
                                    <button class="d-flex align-items-center px-4 pb-2 main_card_btn" disabled>
                                        <div class="form-check">
                                            <input class="form-check-input large" type="checkbox" name="exampleRadios"
                                                   value="option1">
                                        </div>
                                        <div class="row row-custom w-100">
                                            <div class="col-5 col-custom col-answer">
                                                @if($row === 'is_other_answer')
                                                    その他<input type="hidden" name="question[{{ $key }}][answer][]"
                                                              value="is_other_answer"/>
                                                @else
                                                    <input type="text" name="question[{{ $key }}][answer][]"
                                                           value="{{ $row }}" class="form-control input-answer"
                                                           maxlength="255" placeholder="選択肢{{ $k + 1 }}"/>
                                                @endif
                                            </div>
                                            <div class="col-custom text-answer-error">
                                                <small class="font-family-w3 color-red error-mgs-answer"></small>
                                            </div>
                                        </div>
                                        @if($k > 0) <span class="close"></span> @endif
                                    </button>
                                </div>
                            @endforeach
                        </div>
                        <button class="d-flex pb-3 main_card_btn px-4" disabled>
                            <a href="javascript:void(0);" class="btn-custom btn-custom-outlined add-content-checkbox">選択肢を追加</a>
                            <a href="javascript:void(0);"
                               class="btn-custom btn-custom-outlined ml-2 add-other-checkbox other-answer @if(@$isOther) disable @endif">「その他」を追加</a>
                        </button>
                    </div>
                @else
                    <button class="card-custom_item box-question-select main_card_btn px-4" disabled>
                        <ol class="card-custom_list mb-2">
                            @foreach($answer as $k => $row)
                                <li class="mb-2 box-items-answer">
                                    <div class="row row-custom w-100">
                                        <div class="col-5 col-custom">
                                            <input type="text" name="question[{{ $key }}][answer][]" value="{{ $row }}"
                                                   maxlength="255" class="form-control input-answer" placeholder="選択肢{{ $k + 1 }}"/>
                                        </div>
                                        <div class="col-custom text-answer-error">
                                            <small class="font-family-w3 color-red error-mgs-answer"></small>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ol>
                        <div class="d-flex mb-3">
                            <a href="javascript:void(0);" class="btn-custom btn-custom-outlined add-content-select">選択肢を追加</a>
                        </div>
                    </button>
                @endif
            @else
                @if($typeQuestion == \App\Models\Question::TYPE_ESSAY)
                    <button class="card-custom_item pb-3 main_card_btn px-4" disabled>
                        <div class="col-8 px-0">
                            <textarea class="form-control" name="" id="" cols="30" row row-customs="10"></textarea>
                        </div>
                    </button>
                @endif
            @endif
        </div>
        <button class="px-4 pb-4 main_card_btn last" disabled>
            <div class="card-custom_footer d-flex align-items-center justify-content-end">
                <a href="javascript:void(0);" class="btn-custom btn-custom-outlined mr-2 copy-form-question"><i
                        class="fas fa-copy main_icon large m-0"></i></a>
                <a href="javascript:void(0);" class="btn-custom btn-custom-outlined delete-form-question"><i
                        class="fas fa-trash-alt main_icon large m-0"></i></a>
                <label class="ml-3 mr-1 mb-0">必須</label>
                <input type="checkbox" name="question[{{ $key }}][is_obligatory]"
                       {{ @$isObligatory ? 'checked' : '' }} value="1" class="main_switch switch-question_required"/>
            </div>
        </button>
    </div>
</div>
