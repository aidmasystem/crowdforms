<button class="card-custom_item box-question-select main_card_btn px-4" disabled>
    <ol class="card-custom_list mb-2">
        <li class="mb-2 box-items-answer">
            <div class="row row-custom w-100">
                <div class="col-5 col-custom">
                    <input type="text" name="question[{{ $key }}][answer][]" maxlength="255"
                           class="form-control input-answer" placeholder="選択肢1"/>
                </div>
                <div class="col-custom text-answer-error">
                    <small class="font-family-w3 color-red error-mgs-answer"></small>
                </div>
            </div>
        </li>
    </ol>
    <div class="d-flex mb-3">
        <a href="javascript:void(0);" class="btn-custom btn-custom-outlined add-content-select">選択肢を追加</a>
    </div>
</button>
