<div class="card-custom mb-30">
    <h3 class="title-large">{{ @$title }}</h3>
    <p class="title-medium mb-0">{!! nl2br(@$descriptions) !!}</p>
</div>
<div class="mb-30">
    @if(!empty($question))
        @foreach($question as $key => $row)
            <div class="card-custom mb-3 form-custom">
                <h4 class="fs-14 font-family-w6 mb-3">{{ $row['content'] }}
                    @if(@$row['is_obligatory'])<span
                        class="card-table_label font-family-w3 ml-1">必須</span>@endif
                </h4>
                @if($row['type'] == \App\Models\Question::TYPE_ESSAY)
                    <div class="form-custom_select">
                        <input class="form-control input-answer col-12" placeholder="回答を入力">
                    </div>
                @elseif($row['type'] == \App\Models\Question::TYPE_RADIO)
                    @if(!empty($row['answer']))
                        @foreach($row['answer'] as $item)
                            <div class="form-check box-input-check d-flex mb-2">
                                <div class="input-check">
                                    <input class="form-check-input large" type="radio" value="">
                                </div>
                                <label class="form-check-label"
                                       for="">{{ $item === 'is_other_answer' ? 'その他' :  $item }}</label>
                            </div>
                        @endforeach
                    @endif
                @elseif($row['type'] == \App\Models\Question::TYPE_CHECKBOX)
                    @if(!empty($row['answer']))
                        @foreach($row['answer'] as  $item)
                            <div class="form-check box-input-check d-flex mb-2">
                                <div class="input-check">
                                    <input class="form-check-input large" type="checkbox" value="">
                                </div>
                                <label class="form-check-label"
                                       for="">{{ $item === 'is_other_answer' ? 'その他' :  $item }}</label>
                            </div>
                        @endforeach
                    @endif
                @else
                    <div class="form-custom_select select-pulldown col-3 p-0">
                        @if(!empty($row['answer']))
                            <select class="selectType" title="選択してください">
                                @foreach($row['answer'] as $item)
                                    <option value="">{{ $item }}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                @endif
            </div>
        @endforeach
    @endif
</div>
