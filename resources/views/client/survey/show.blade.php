@extends('client.layouts.master')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large"><i class="fas fa-edit main_icon"></i>アンケート詳細</h2>
    </div>
    <div class="card-custom mb-30">
        <p class="font-family-w6">アンケートタイトル</p>
        <h3 class="title-large fs-24 px-3 mb-3">{{ $survey->title }}</h3>
        <p class="font-family-w6">アンケート詳細</p>
        <h4 class="fs-16 mb-0 px-3">{!! nl2br($survey->descriptions) !!}</h4>
    </div>
    <!--list question-->
    <div class="mb-30">
        @if($survey->question->isNotEmpty())
            @foreach($survey->question as $row)
                <div class="card-custom mb-3 form-custom">
                    <h4 class="fs-14 font-family-w6 mb-3">{{ $row->content }}@if($row->is_obligatory)<span
                            class="card-table_label font-family-w3 ml-1">必須</span>@endif
                    </h4>
                    @if($row->type == \App\Models\Question::TYPE_ESSAY)
                        <div class="form-custom_select">
                            <textarea class="form-control col-8" placeholder="入力してください。" name="" id="" cols="30" row
                                      row-customs="10"></textarea>
                        </div>
                    @elseif($row->type == \App\Models\Question::TYPE_RADIO)
                        @if($row->answer->isNotEmpty())
                            @foreach($row->answer as $item)
                                <div class="form-check box-input-check d-flex mb-2">
                                    <div class="input-check">
                                        <input class="form-check-input large" type="radio" name="exampleRadios" value="">
                                    </div>
                                    <label class="form-check-label" for="">{{ $item->content }}</label>
                                </div>
                            @endforeach
                        @endif
                    @elseif($row->type == \App\Models\Question::TYPE_CHECKBOX)
                        @if($row->answer->isNotEmpty())
                            @foreach($row->answer as $item)
                                <div class="form-check box-input-check d-flex mb-2">
                                    <div class="input-check">
                                        <input class="form-check-input large" type="checkbox" name="exampleRadios"
                                               value="option1">
                                    </div>
                                    <label class="form-check-label" for="">{{ $item->content }}</label>
                                </div>
                            @endforeach
                        @endif
                    @else
                        <div class="form-custom_select select-pulldown col-3 p-0">
                            <select class="selectType" title="選択してください">
                                @if($row->answer->isNotEmpty())
                                    @foreach($row->answer as $item)
                                        <option value="">{{ $item->content }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    @endif
                </div>
            @endforeach
        @endif
    </div>
    <div class="d-flex justify-content-center">
        <a href="{{ route('client.survey.create') }}" class="btn-custom btn-custom-disabled md btn-custom-large">アンケート作成画面に戻る</a>
    </div>
@endsection
