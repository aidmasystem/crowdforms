@extends('client.layouts.master')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large"><i class="fas fa-pen main_icon"></i>アンケート編集</h2>
    </div>

    <form action="{{ route('client.survey.update', $survey['id']) }}" method="POST" class="form-custom"
          id="form-create-survey">
        @csrf
        <div id="box-form-create">
            <input type="hidden" id="check-update-survey" value="{{ $survey['id'] }}">
            <div class="card-custom mb-30 card-custom-w">
                <div class="row flex-wrap">
                    <div class="form-group col-8">
                        <label class="font-family-w6 fs-14" for="">アンケートタイトル<span
                                class="card-table_label font-family-w3 ml-1">必須</span></label>
                        <small class="font-family-w3 color-red error-mgs-title"></small>
                        <input type="text" name="title" max="255"
                               class="form-control large input-title-survey"
                               value="{{ $survey['title'] }}" maxlength="255" placeholder="無題のアンケート"/>
                    </div>
                    <div class="form-group col-8 mb-0">
                        <label class="font-family-w6 fs-14" for="">アンケート詳細</label>
                        <small class="font-family-w3 color-red error-mgs-des"></small>
                        <textarea class="form-control description-survey" placeholder="アンケート詳細を入力してください" name="descriptions" id=""
                                  maxlength="512" cols="30" row
                                  row-customs="10">{{ $survey['descriptions'] }}</textarea>
                    </div>
                </div>
            </div>
            <div class="position-relative mb-2">
                <div class="card-custom_action" style="float: right">
                    <a href="javascript:void(0);" class="btn-custom" id="add-form-question"><i
                            class="fas fa-plus-circle"></i></a>
                    <a href="javascript:void(0);" class="btn-custom" id="preview-form-question"><i
                            class="fas fa-eye"></i></a>
                </div>
                <div id="sortable" class="all-form-question mb-4">
                    @if(!empty($survey['question']))
                        @foreach($survey['question'] as $key => $row)
                            <div class="card-custom-w">
                                <div class="card-custom mb-3 p-0 position-relative main_card parent-question"
                                     data-key="{{ $key + 1 }}">
                                    <span class="icon-drag"></span>
                                    <button class="pb-2 px-4 main_card_btn first" disabled>
                                        <label class="font-family-w6 fs-14" for="">質問内容
                                            {!! $row['is_obligatory'] ? '<span class="card-table_label font-family-w3 ml-1">必須</span>' : ''  !!}
                                        </label>
                                        <small class="font-family-w3 color-red error-mgs-content"></small>
                                        <div class="row row-custom">
                                            <div class="col-5 col-custom">
                                                <input type="text" name="question[{{ $key + 1 }}][content]"
                                                       value="{{ @$row['content'] }}" maxlength="512"
                                                       class="form-control input-question" placeholder="質問内容を入力してください"/>
                                            </div>
                                            <div class="col-3 col-custom">
                                                <div class="form-custom_select">
                                                    <select name="question[{{ $key + 1 }}][type]"
                                                            class="change_value_type selectType">
                                                        @foreach($type as $k => $item)
                                                            <option
                                                                value="{{ $k }}" {{ $k == $row['type'] ? 'selected' : '' }}>{{ $item }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="question[{{ $key + 1 }}][id]"
                                               value="{{ $row['id'] }}">
                                    </button>
                                    <div class="form-answers">
                                        @if(!empty($row['answer']))
                                            @if($row['type'] == \App\Models\Question::TYPE_RADIO)
                                                <div class="card-custom_item box-question-radio">
                                                    <div class="box-body-radio sortable_answer">
                                                        @foreach($row['answer'] as $k => $item)
                                                            <div class="box-items-answer">
                                                                <span class="icon-drag move-drag-answer {{ $k == 0 ? ' drag-first' : '' }}" @if($k == 0 && count($row['answer']) > 1) style="display: block" @endif></span>
                                                                <button class="d-flex align-items-center px-4 pb-2 main_card_btn" disabled>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input large" type="radio"
                                                                               name="exampleRadios" value="option1">
                                                                    </div>
                                                                    <div class="row row-custom w-100">
                                                                        <div class="col-5 col-custom col-answer">
                                                                            @if($item['is_other'])
                                                                                その他<input type="hidden"
                                                                                          name="question[{{ $key + 1 }}][answer][]"
                                                                                          value="is_other_answer"/>
                                                                            @else
                                                                                <input type="text" name="question[{{ $key + 1 }}][answer][]"
                                                                                       value="{{ $item['content'] }}"
                                                                                       class="form-control input-answer"
                                                                                       maxlength="255" placeholder="選択肢{{ $k + 1 }}"/>
                                                                            @endif
                                                                        </div>
                                                                        <div class="col-custom text-answer-error">
                                                                            <small class="font-family-w3 color-red error-mgs-answer"></small>
                                                                        </div>
                                                                    </div>
                                                                    @if($k > 0) <span class="close"></span> @endif
                                                                </button>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <button class="d-flex pb-3 main_card_btn px-4" disabled>
                                                        <a href="javascript:void(0);"
                                                           class="btn-custom btn-custom-outlined add-content-radio">選択肢を追加</a>
                                                        <a href="javascript:void(0);"
                                                           class="btn-custom btn-custom-outlined ml-2 add-other-radio other-answer @if(@$row['is_other']) disable @endif">「その他」を追加</a>
                                                    </button>
                                                </div>
                                            @elseif($row['type'] == \App\Models\Question::TYPE_CHECKBOX)
                                                <div class="card-custom_item box-question-checkbox">
                                                    <div class="box-body-checkbox">
                                                        @foreach($row['answer'] as $k => $item)
                                                            <div class="box-items-answer">
                                                                <span class="icon-drag move-drag-answer {{ $k == 0 ? ' drag-first' : '' }}" @if($k == 0 && count($row['answer']) > 1) style="display: block" @endif></span>
                                                                <button class="d-flex align-items-center px-4 pb-2 main_card_btn" disabled>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input large" type="checkbox" name="exampleRadios"
                                                                               value="option1">
                                                                    </div>
                                                                    <div class="row row-custom w-100">
                                                                        <div class="col-5 col-custom col-answer">
                                                                            @if($item['is_other'])
                                                                                その他<input type="hidden"
                                                                                          name="question[{{ $key + 1 }}][answer][]"
                                                                                          value="is_other_answer"/>
                                                                            @else
                                                                                <input type="text" name="question[{{ $key + 1 }}][answer][]"
                                                                                       value="{{ $item['content'] }}"
                                                                                       class="form-control input-answer"
                                                                                       maxlength="255" placeholder="選択肢{{ $k + 1 }}"/>
                                                                            @endif
                                                                        </div>
                                                                        <div class="col-custom text-answer-error">
                                                                            <small class="font-family-w3 color-red error-mgs-answer"></small>
                                                                        </div>
                                                                    </div>
                                                                    @if($k > 0) <span class="close"></span> @endif
                                                                </button>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <button class="d-flex pb-3 main_card_btn px-4" disabled>
                                                        <a href="javascript:void(0);"
                                                           class="btn-custom btn-custom-outlined add-content-checkbox">選択肢を追加</a>
                                                        <a href="javascript:void(0);"
                                                           class="btn-custom btn-custom-outlined ml-2 add-other-checkbox other-answer @if(@$row['is_other']) disable @endif">「その他」を追加</a>
                                                    </button>
                                                </div>
                                            @else
                                                <button class="card-custom_item box-question-select main_card_btn px-4" disabled>
                                                    <ol class="card-custom_list mb-2">
                                                        @foreach($row['answer'] as $k => $item)
                                                            <li class="mb-2 box-items-answer">
                                                                <div class="row row-custom w-100">
                                                                    <div class="col-5 col-custom">
                                                                        <input type="text" name="question[{{ $key + 1 }}][answer][]"
                                                                               value="{{ $item['content'] }}" class="form-control input-answer"
                                                                               maxlength="255" placeholder="選択肢{{ $k + 1 }}"/>
                                                                    </div>
                                                                    <div class="col-custom text-answer-error">
                                                                        <small class="font-family-w3 color-red error-mgs-answer"></small>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    </ol>
                                                    <div class="d-flex mb-3">
                                                        <a href="javascript:void(0);"
                                                           class="btn-custom btn-custom-outlined add-content-select">選択肢を追加</a>
                                                    </div>
                                                </button>
                                            @endif
                                        @else
                                            @if($row['type'] == \App\Models\Question::TYPE_ESSAY)
                                                <button class="card-custom_item pb-3 main_card_btn px-4" disabled>
                                                    <div class="col-8 px-0">
                                                        <textarea class="form-control" name="" id="" cols="30" row
                                                                  row-customs="10"></textarea>
                                                    </div>
                                                </button>
                                            @endif
                                        @endif
                                    </div>
                                    <button class="px-4 pb-4 main_card_btn last" disabled>
                                        <div class="card-custom_footer d-flex align-items-center justify-content-end">
                                            <a href="javascript:void(0);"
                                               class="btn-custom btn-custom-outlined mr-2 copy-form-question"><i
                                                    class="fas fa-copy main_icon large m-0"></i></a>
                                            <a href="javascript:void(0);"
                                               class="btn-custom btn-custom-outlined delete-form-question"><i
                                                    class="fas fa-trash-alt main_icon large m-0"></i></a>
                                            <label class="ml-3 mr-1 mb-0">必須</label>
                                            <input type="checkbox" name="question[{{ $key + 1 }}][is_obligatory]"
                                                   {{ @$row['is_obligatory'] ? 'checked' : '' }} value="1" class="main_switch switch-question_required"/>
                                        </div>
                                    </button>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
                <input type="hidden" id="length-key" value="{{ $countQuestion }}">
            </div>
        </div>
        <div id="box-form-preview"></div>

        <div class="d-flex justify-content-center">
            <a href="{{ route('client.survey.index') }}" id="back-index"
               class="btn-custom btn-custom-disabled md mx-4 btn-custom-large">キャンセル</a>
            <a href="javascript:void(0);" class="btn-custom btn-custom-disabled md mx-4 btn-custom-large"
               id="back-create">戻る</a>
            <button type="submit" id="btn-submit_survey" class="btn-custom btn-custom-primary md mx-4 btn-custom-large">編集</button>
        </div>
    </form>
@endsection
