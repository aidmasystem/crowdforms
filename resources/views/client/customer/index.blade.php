@extends('client.layouts.master')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large"><i class="fas fa-paper-plane main_icon"></i>送信先一覧</h2>
        <div class="d-flex">
            <a href="{{ route('client.customer.create') }}" class="btn-custom btn-custom-warning md">
                <i class="fas fa-plus main_icon text-white"></i>送信先新規登録</a>
            <a href="{{ route('client.customer.createCsv') }}" class="btn-custom btn-custom-warning px-4 ml-2"><i
                    class="fas fa-file-csv main_icon text-white"></i>CSV登録</a>
        </div>
    </div>
    <div class="card-custom mb-30">
        <form action="" class="form-custom">
            <div class="form-group">
                <div class="d-flex form-custom_search">
                    <input type="text" name="keyword" class="form-control input-search" value="{{ request()->keyword }}"
                           placeholder="検索したいワードを入力してください">
                    <i class="fas fa-search"></i>
                    <button type="submit" class="btn-custom btn-custom-primary">検索</button>
                </div>
                <small class="form-text color-red"></small>
            </div>
            <div class="d-flex">
                <div class="form-group mb-0 mr-5">
                    <label class="font-weight-bold fs-14" for="">作成日時</label>
                    <div class="d-flex align-items-center">
                        <div class='input-group form-custom_date' id='datepicker'>
                            <input type='text' name="from_date" value="{{ request()->from_date }}"
                                   class="form-control" placeholder="年/月/日"/>
                            <span class="input-group-addon"></span>
                        </div>
                        <span class="icon">~</span>
                        <div class='input-group form-custom_date' id='datepicker1'>
                            <input type='text' name="to_date" value="{{ request()->to_date }}"
                                   class="form-control" placeholder="年/月/日"/>
                            <span class="input-group-addon"></span>
                        </div>
                    </div>
                    <small class="form-text color-red"></small>
                </div>
            </div>
        </form>
    </div>

    <!-- alert -->
    @if ($message = Session::get('success'))
        <div class="alert alert-success" role="alert">
            <p class="mb-0">{{ $message }}</p>
        </div>
    @endif
    <!--table-->
    <div class="main_table">
        <div class="d-flex justify-content-between align-items-center mb-3">
            <p class="fs-20 mb-0">件数 {{ @$count }}件</p>
            <div class="d-flex align-items-center">
                <a href="javascript:void(0);" class="btn-custom btn-custom-primary btn-custom-large px-4 add-modal_send disable"
                   data-target="#sendAddress">送信</a>
                <a class="btn-custom btn-custom-outlined ml-2 btn-custom-large bg-transparent px-4 add-modal_status"
                        data-target="#checkStatus">送信状況の確認
                </a>
                <ul class="pagination ml-3 mb-0">
                    {!! $lists->appends($_GET)->links() !!}
                </ul>
            </div>
        </div>
        <div class="table-responsive mb-3">
            <table class="table table-bordered table-custom-3 text-center mb-0 form-custom box-table_customer">
                <thead>
                <tr>
                    <th scope="col" class="main_table_action py-1">
                        <div class="form-check pl-2">
                            <input class="form-check-input large select-all" id="select-all_customer" type="checkbox" name="">
                        </div>
                        <small>全選択</small>
                    </th>
                    <th scope="col" class="main_table_action">削除</th>
                    <th scope="col" class="main_table_action">編集</th>
                    <th scope="col">氏名<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                              class="img-fluid mb-1 sort-heading" id="full_name" data-sort="asc"></span></th>
                    <th scope="col">会社名<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                               class="img-fluid mb-1 sort-heading" id="company_name" data-sort="asc"></span></th>
                    <th scope="col">部署<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                              class="img-fluid mb-1 sort-heading" id="department_name" data-sort="asc"></span></th>
                    <th scope="col">役職<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                              class="img-fluid mb-1 sort-heading" id="position" data-sort="asc"></span></th>
                    <th scope="col">登録日時<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                                class="img-fluid mb-1 sort-heading" id="created_at" data-sort="asc"></span>
                    </th>
                </tr>
                </thead>
                <tbody>
                @if($lists->isNotEmpty())
                    @foreach($lists as $row)
                        <tr>
                            <td class="main_table_action">
                                <div class="form-check pl-2">
                                    <input class="form-check-input large select-customer" value="{{ $row->id }}"
                                           type="checkbox">
                                </div>
                            </td>
                            <td class="main_table_action">
                                <a href="javascript:void(0);" data-id="{{ $row->id }}"
                                   data-url="{{ route('client.customer.destroy', $row->id) }}" class="delete-customer"
                                   data-toggle="modal" data-target="#deleteCustomer">
                                    <i class="fas fa-trash-alt main_icon m-0"></i>
                                </a>
                            </td>
                            <td class="main_table_action">
                                <a href="{{ route('client.customer.edit', $row->id) }}"><i
                                        class="fas fa-pen main_icon m-0"></i></a>
                            </td>
                            <td class="text-overflow title-survey" data-toggle="tooltip" title="{{ $row->full_name }}">{{ $row->full_name }}</td>
                            <td class="large text-overflow" data-toggle="tooltip" title="{{ $row->company_name }}">{{ $row->company_name }}</td>
                            <td class="text-overflow title-survey" data-toggle="tooltip" title="{{ $row->department_name }}">{{ $row->department_name }}</td>
                            <td class="text-overflow title-survey" data-toggle="tooltip" title="{{ $row->position }}">{{ $row->position }}</td>
                            <td class="text-nowrap">{{ $row->created_at }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="8">
                            <div class="text-center">データがありません。</div>
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
        <div class="d-flex justify-content-between align-items-center mb-3">
            <p class="fs-20 mb-0">件数 {{ @$count }}件</p>
            <div class="d-flex align-items-center">
                <a href="javascript:void(0);" class="btn-custom btn-custom-primary btn-custom-large px-4 add-modal_send disable"
                   data-target="#sendAddress">送信
                </a>
                <a class="btn-custom btn-custom-outlined ml-2 btn-custom-large bg-transparent px-4 add-modal_status"
                        data-target="#checkStatus">送信状況の確認
                </a>
                <ul class="pagination ml-3 mb-0">
                    {!! $lists->appends($_GET)->links() !!}
                </ul>
            </div>
        </div>

        <input type="hidden" id="check_all_customer" value="{{ implode(',', $arrCustomer) }}">
        <input type="hidden" id="choose_customer" value="">
        <input type="hidden" name="hidden_page" id="hidden_page" value="1"/>
        <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="" />
        <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="" />
    </div>

    <!--modal delete-->
    <div class="modal fade modal-custom" id="deleteCustomer" role="dialog" aria-labelledby="deleteCustomer"
         aria-hidden="true">
        <div class="delete modal-dialog modal-confirm">
            <div class="modal-content">
                <form action="#" method="POST" id="form_delete_customer">
                    @csrf
                    @method('DELETE')
                    <div class="modal-header mb-3">
                        <h3 class="title title-large mb-0"><i
                                class="mr-2 fas fa-exclamation-triangle fs-20 color-red"></i>削除</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body p-0 mb-3 text-center fs-16">
                        <p>削除しますがよろしいですか</p>
                    </div>
                    <div class="modal-footer justify-content-center p-0 mb-1">
                        <button type="button" class="btn-custom btn-custom-disabled w-50" data-dismiss="modal">キャンセル
                        </button>
                        <button type="submit" class="btn-custom btn-custom-danger w-50">削除</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--modal list-->
    <div class="modal fade modal-custom" id="checkStatus" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header mb-4">
                    <h2 class="title title-large mb-0"><i class="fas fa-paper-plane main_icon"></i>送信状況の確認</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-0 form-custom">

                </div>
            </div>
        </div>
    </div>

    <!--modal send-->
    <div class="modal fade modal-custom" id="sendAddress" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered md" role="document">
            <div class="modal-content border-0 rounded">
                <div class="modal-header mb-3">
                    <h2 class="title title-large mb-0"><i class="fs-20 fas fa-paper-plane main_icon"></i>アンケートを送信
                    </h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-0">

                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function () {
            $(document).on('click', '.delete-customer', function() {
                let url = $(this).data('url');
                $('#form_delete_customer').attr('action', url);
            });

            $(".sort-heading").click(function (e) {
                e.preventDefault();
                let sort_name = $(this).attr('id');
                let sort_by = $(this).data('sort');
                clearIcon(sort_name);

                let keyword = '{{ request()->keyword }}';
                let from_date = '{{ request()->from_date }}';
                let to_date = '{{ request()->to_date }}';
                let page = 1;
                $('#hidden_column_name').val(sort_name);
                $('#hidden_sort_type').val(sort_by);

                fetchData(page, sort_name, sort_by, keyword, from_date, to_date);
            });

            $(document).on('click', '.pagination a', function(e){
                e.preventDefault();
                let page = $(this).attr('href').split('page=')[1];
                $('#hidden_page').val(page);
                var column_name = $('#hidden_column_name').val();
                var sort_by = $('#hidden_sort_type').val();
                let keyword = '{{ request()->keyword }}';
                let from_date = '{{ request()->from_date }}';
                let to_date = '{{ request()->to_date }}';

                fetchData(page, column_name, sort_by, keyword, from_date, to_date);
            });

            function fetchData(page, column_name, sort_by, keyword, from_date, to_date) {
                let getId = column_name;
                let sortBy = sort_by;
                $.ajax({
                    url: '{{ route('client.customer.index') }}',
                    type: 'GET',
                    data: {
                        page: page,
                        column: column_name,
                        sortOrder: sort_by,
                        keyword: keyword,
                        from_date: from_date,
                        to_date: to_date,
                    },
                    beforeSend: showLoad(),
                    success:function(data) {
                        hideLoad();
                        if (sortBy == 'asc') {
                            $('#' + getId).data('sort', 'desc');
                            $('#' + getId).attr('src', '{{ asset('images/icons/sort-asc.svg') }}');
                        } else if (sortBy == 'desc') {
                            $('#' + getId).data('sort', 'all');
                            $('#' + getId).attr('src', '{{ asset('images/icons/sort-desc.svg') }}');
                        } else if (sortBy == 'all') {
                            $('#' + getId).data('sort', 'asc');
                            $('#' + getId).attr('src', '{{ asset('images/icons/sort.svg') }}');
                        }

                        $('ul.pagination').html(data.paginate);
                        $(".table tr:not(:first)").remove();
                        $(".table").append(data.data);
                        chooseCustomer();
                        // window.history.pushState("", "", this.url);
                    }
                });
            }

            function chooseCustomer() {
                let chooseCustomer = $('#choose_customer').val();
                let arrCustomer = chooseCustomer != '' ? chooseCustomer.split(',') : [];

                $('input.select-customer').each(function (k, v) {
                    if (arrCustomer.includes($(v).val())) $(v).prop('checked', true);
                });
            }
        });
    </script>
@endpush
