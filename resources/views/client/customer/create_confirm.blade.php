@extends('client.layouts.master')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large mb-0">送信先新規登録</h2>
    </div>
    <!--table-->
    <form action="{{ route('client.customer.store') }}" method="POST" id="frm" class="form-custom">
        @csrf
        <div class="card-custom card-table mb-30">
            <table class="table table-bordered mb-0">
                <tbody>
                <tr>
                    <th class="th-header">
                        氏名 <span class="card-table_label">必須</span> <br>
                    </th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-12 col-custom break-word">
                                {{ $full_name }}
                            </div>
                            <input type="hidden" name="full_name" value="{{ $full_name }}">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">
                        フリガナ <span class="card-table_label">必須</span><br>
                    </th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-12 col-custom break-word">
                                {{ $full_name_furi }}
                            </div>
                            <input type="hidden" name="full_name_furi" value="{{ $full_name_furi }}">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>企業名</th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-12 col-custom break-word">
                                {{ @$company_name }}
                            </div>
                            <input type="hidden" name="company_name" value="{{ @$company_name }}">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>部署名</th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-12 col-custom break-word">
                                {{ @$department_name }}
                            </div>
                            <input type="hidden" name="department_name" value="{{ @$department_name }}">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>役職名</th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-12 col-custom break-word">
                                {{ @$position }}
                            </div>
                            <input type="hidden" name="position" value="{{ @$position }}">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">
                        電話番号 <span class="card-table_label">必須</span><br>
                    </th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-4 col-custom">
                                {{ $tel }}
                            </div>
                            <input type="hidden" name="tel" value="{{ $tel }}">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">
                        メールアドレス <span class="card-table_label">必須</span><br>
                    </th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-12 col-custom break-word">
                                {{ $email }}
                            </div>
                            <input type="hidden" name="email" value="{{ $email }}">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">
                        住所 <span class="card-table_label">必須</span><br>
                    </th>
                    <td>
                        〒 {{ $post_code_1 . ' - ' . $post_code_2 }} <br>
                        <p class="break-word">{{ $address }}</p>
                    </td>
                    <input type="hidden" name="post_code_1" value="{{ $post_code_1 }}">
                    <input type="hidden" name="post_code_2" value="{{ $post_code_2 }}">
                    <input type="hidden" name="address" value="{{ $address }}">
                </tr>
                <tr>
                    <th>備考</th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-12 col-custom break-word">
                                {{ @$note }}
                            </div>
                            <input type="hidden" name="note" value="{{ @$note }}">
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="form-check form-check-inline mt-3">
                <input class="form-check-input" type="radio" name="can_duplicate" id="registerEmail"
                       {{ @$can_duplicate == \App\Models\Customer::CAN_DUPLICATE ? 'checked' : 'disabled' }} value="{{ \App\Models\Customer::CAN_DUPLICATE }}">
                <label class="form-check-label" for="registerEmail">
                    メールアドレスの重複をそのまま登録する
                </label>
            </div>
            <br>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="can_duplicate" id="registerEmail"
                       {{ @$can_duplicate == \App\Models\Customer::CAN_NOT_DUPLICATE ? 'checked' : 'disabled' }} value="{{ \App\Models\Customer::CAN_NOT_DUPLICATE }}">
                <label class="form-check-label" for="registerEmail">
                    メールアドレスで重複を一意にする
                </label>
            </div>
        </div>
        <div class="d-flex justify-content-center">
            <a href="{{ route('client.customer.create') }}"
               class="btn-custom btn-custom-disabled md mx-4 btn-custom-large">戻る</a>
            <button type="submit" id="btn-submit" class="btn-custom btn-custom-primary md mx-4 btn-custom-large">登録</button>
        </div>
    </form>
@endsection
