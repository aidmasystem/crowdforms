@extends('client.layouts.master')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large mb-0">
            <i class="fas fa-file-csv main_icon"></i>CSV登録
        </h2>
    </div>

    <div class="box-import_csv">
        <!--message upload success-->
        @if (@$message)
            <p class="mb-30 fs-18">
                <i class="fas fa-check-circle color-green fs-24"></i>
                {{ @$message }}
            </p>
        @endif

        <form action="{{ route('client.customer.previewFileUpload') }}" method="POST" class="form-custom_upload"
              id="form-create_csv"
              enctype="multipart/form-data">
            @csrf
            <div class="card-custom mb-30">
                <div class="mb-3">
                    <button type="button" class="btn-custom btn-custom-outlined btn-custom-large ml-auto download-csv">
                        <a href="{{ route('client.customer.templateCsv') }}"><i class="fas fa-download main_icon"></i>CSVフォーマットダウンロード</a>
                    </button>
                </div>
                <small class="font-family-w3 color-red error-msg_file"></small>
                <div class="form-custom_upload_content mb-3" id="dropContainer" ondragover="return false">
                    <div class="form-custom_upload_position">
                        <p class="form-custom_upload_txt">
                            タグを付与する企業データをドラッグ&ドロップしてください <br>
                            （最大容量は10Mまで）
                        </p>
                        <input type="file" name="file_csv[]" id="upload_csv" accept=".csv" multiple hidden/>
                        <label for="upload_csv" class="form-custom_upload_label">ファイルを選択</label>
                    </div>
                </div>
                <div class=""><small class="font-family-w3 color-red error-msg_checkMail"></small></div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="duplicateEmail" id="registerEmail" value="1">
                    <label class="form-check-label" for="registerEmail">
                        メールアドレスの重複をそのまま登録する
                    </label>
                </div>
                <br>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="duplicateEmail" checked id="emailAddress" value="0">
                    <label class="form-check-label" for="emailAddress">
                        メールアドレスで重複を一意にする
                    </label>
                </div>
                <p class="mb-0">メールアドレスで重複を一意にする場合は、入力したメールアドレスを元にデータベースのデータを纏めます。</p>
            </div>
            <div class="form-custom_upload_list mb-30">
            </div>

            <div class="d-flex justify-content-center">
                <a href="{{ route('client.customer.index') }}"
                   class="btn-custom btn-custom-disabled md mx-4 btn-custom-large">戻る</a>
                <button type="submit" id="submit-preview_csv"
                        class="btn-custom btn-custom-primary md mx-4 btn-custom-large">確認
                </button>
            </div>
        </form>
    </div>
    <div class="box-preview_csv">

    </div>
@endsection
