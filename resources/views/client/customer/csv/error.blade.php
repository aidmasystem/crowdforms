<!--message error-->
<p class="mb-30 fs-18 color-red">
    <i class="fas fa-exclamation-triangle fs-18"></i>
    エラーを検知しました。修正してから登録してください。
</p>
<form action="{{ route('client.customer.submitImportCsv') }}" method="POST" id="form-import_csv" class="form-custom">
    @csrf
    @if(!empty($data))
        @foreach($data as $key => $row)
            @if(empty($row['success']) && empty($row['error']))
                <div class="card-custom mb-20 main_table">
                    <p class="mb-20 fs-16">
                        @if(end($row) != '')
                            {{ end($row) }}
                        @endif
                        <span class="d-block color-red">
                            CSV ファイルはデータが空欄です。再確認してください。
                        </span>
                    </p>
                </div>
                @continue
            @endif
            @if(empty($row['error'])) @continue @endif
            <div class="card-custom mb-20 main_table">
                <p class="mb-20 fs-16">
                @if(end($row) != '')
                    {{ end($row) }}
                @endif
                @if(!empty($row['error']))
                    <span class="d-block">
                        該当件数　{{ @count($row['error']) }}件
                    </span>
                @endif
                </p>
                <div class="table-responsive">
                    <table class="table table-bordered table-custom-3 text-center mb-0 form-custom">
                        <thead>
                        <tr>
                            <th scope="col" class="main_table_action">削除</th>
                            <th scope="col">氏名</th>
                            <th scope="col">
                                フリガナ
                            </th>
                            <th scope="col">
                                企業名
                            </th>
                            <th scope="col">
                                部署名
                            </th>
                            <th scope="col">
                                役職名
                            </th>
                            <th scope="col">
                                電話番号
                            </th>
                            <th scope="col">
                                メールアドレス
                            </th>
                            <th scope="col">
                                郵便番号
                            </th>
                            <th scope="col">
                                住所
                            </th>
                            <th scope="col">
                                備考
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($row['error']))
                            @foreach($row['error'] as $k => $item)
                                <tr>
                                    <td class="main_table_action">
                                        <a href="javascript:void(0)" class="delete-error_csv" data-key="{{$key}}" data-item="{{$k}}">
                                            <i class="fas fa-trash-alt main_icon m-0"></i></a>
                                    </td>
                                    <td>
                                        @if(!array_key_exists('full_name', $row['log'][$k]))
                                            {{ $item['full_name'] }}
                                            <input type="hidden" name="customer[{{$key}}][{{$k}}][full_name]" value="{{ $item['full_name'] }}">
                                        @else
                                            <small class="text-error">{{ @$row['log'][$k]['full_name'] }}</small>
                                            <input type="text" name="customer[{{$key}}][{{$k}}][full_name]" value="{{ $item['full_name'] }}"
                                                   maxlength="255" placeholder="氏名を入力してください" class="form-control border-error pl-small">
                                        @endif
                                    </td>
                                    <td>
                                        @if(!array_key_exists('full_name_furi', $row['log'][$k]))
                                            {{ $item['full_name_furi'] }}
                                            <input type="hidden" name="customer[{{$key}}][{{$k}}][full_name_furi]" value="{{ $item['full_name_furi'] }}">
                                        @else
                                            <small class="text-error">{{ @$row['log'][$k]['full_name_furi'] }}</small>
                                            <input type="text" name="customer[{{$key}}][{{$k}}][full_name_furi]" value="{{ $item['full_name_furi'] }}"
                                                   maxlength="255" placeholder="フリガナを入力してください" class="form-control border-error pl-small">
                                        @endif
                                    </td>
                                    <td>
                                        {{ $item['company_name'] }}
                                        <input type="hidden" name="customer[{{$key}}][{{$k}}][company_name]" value="{{ $item['company_name'] }}">
                                    </td>
                                    <td>
                                        {{ $item['department_name'] }}
                                        <input type="hidden" name="customer[{{$key}}][{{$k}}][department_name]" value="{{ $item['department_name'] }}"></td>
                                    <td>
                                        {{ $item['position'] }}
                                        <input type="hidden" name="customer[{{$key}}][{{$k}}][position]" value="{{ $item['position'] }}"></td>
                                    <td>
                                        @if(!array_key_exists('tel', $row['log'][$k]))
                                            {{ formatPhone($item['tel']) }}
                                            <input type="hidden" name="customer[{{$key}}][{{$k}}][tel]" value="{{ $item['tel'] }}">
                                        @else
                                            <small class="text-error">{{ @$row['log'][$k]['tel'] }}</small>
                                            <input type="text" name="customer[{{$key}}][{{$k}}][tel]" value="{{ $item['tel'] }}"
                                                   maxlength="11" placeholder="電話番号を入力してください" class="form-control border-error pl-small">
                                        @endif
                                    </td>
                                    <td>
                                        @if(!array_key_exists('email', $row['log'][$k]))
                                            {{ $item['email'] }}
                                            <input type="hidden" name="customer[{{$key}}][{{$k}}][email]" value="{{ $item['email'] }}">
                                        @else
                                            <small class="text-error">{{ @$row['log'][$k]['email'] }}</small>
                                            <input type="text" name="customer[{{$key}}][{{$k}}][email]" value="{{ $item['email'] }}"
                                                   maxlength="255" placeholder="メールアドレスを入力してください" class="form-control border-error pl-small">
                                        @endif
                                    </td>
                                    <td>
                                        @if(!array_key_exists('post_code', $row['log'][$k]))
                                            {{ formatPostCode($item['post_code']) }}
                                            <input type="hidden" name="customer[{{$key}}][{{$k}}][post_code]" value="{{ $item['post_code'] }}">
                                        @else
                                            <small class="text-error">{{ @$row['log'][$k]['post_code'] }}</small>
                                            <input type="text" name="customer[{{$key}}][{{$k}}][post_code]" value="{{ $item['post_code'] }}"
                                                   maxlength="7" placeholder="郵便番号を入力してください" class="form-control border-error pl-small">
                                        @endif
                                    </td>
                                    <td>
                                        @if(!array_key_exists('address', $row['log'][$k]))
                                            {{ $item['address'] }}
                                            <input type="hidden" name="customer[{{$key}}][{{$k}}][address]" value="{{ $item['address'] }}">
                                        @else
                                            <small class="text-error">{{ @$row['log'][$k]['address'] }}</small>
                                            <input type="text" name="customer[{{$key}}][{{$k}}][address]" value="{{ $item['address'] }}"
                                                   maxlength="255" placeholder="住所を入力してください" class="form-control border-error pl-small">
                                        @endif
                                    </td>
                                    <td>
                                        {{ $item['note'] }}
                                        <input type="hidden" name="customer[{{$key}}][{{$k}}][note]" value="{{ $item['note'] }}">
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        @endforeach
    @endif

    <div class="d-flex justify-content-center mt-4">
        <a href="javascript:void(0)" id="back-import_csv"
           class="btn-custom btn-custom-disabled btn-custom-large md mx-4">戻る</a>
        <button type="submit" class="btn-custom btn-custom-primary btn-custom-large md mx-4">登録</button>
    </div>
</form>
