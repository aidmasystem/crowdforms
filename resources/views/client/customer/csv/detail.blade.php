<p class="mb-30 fs-18">
    登録する先頭5行を表示しています。<br>
    内容に問題がなければ「登録する」ボタンをクリックしてください。
</p>
<form action="{{ route('client.customer.submitImportCsv') }}" method="POST" id="form-import_csv" class="form-custom">
    @csrf
    @if(!empty($data))
        @foreach($data as $key => $row)
            <div class="card-custom mb-20 main_table">
                <p class="mb-20 fs-16">
                @if(end($row) != '')
                    {{ end($row) }}
                @endif
                <!-- <span class="d-block">
                            該当件数　1件
                        </span> -->
                </p>
                <div class="table-responsive">
                    <table class="table table-bordered table-custom-3 text-center mb-0 form-custom">
                        <thead>
                        <tr>
                            <th scope="col">氏名</th>
                            <th scope="col">
                                フリガナ
                            </th>
                            <th scope="col">
                                企業名
                            </th>
                            <th scope="col">
                                部署名
                            </th>
                            <th scope="col">
                                役職名
                            </th>
                            <th scope="col">
                                電話番号
                            </th>
                            <th scope="col">
                                メールアドレス
                            </th>
                            <th scope="col">
                                郵便番号
                            </th>
                            <th scope="col">
                                住所
                            </th>
                            <th scope="col">
                                備考
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($row['data']))
                            @foreach($row['data'] as $k => $item)
                                @if($k > 4) @break @endif
                                <tr>
                                    <td>{{ $item['full_name'] }}</td>
                                    <td>{{ $item['full_name_furi'] }}</td>
                                    <td>{{ $item['company_name'] }}</td>
                                    <td>{{ $item['department_name'] }}</td>
                                    <td>{{ $item['position'] }}</td>
                                    <td>{{ formatPhone($item['tel']) }}</td>
                                    <td>{{ $item['email'] }}</td>
                                    <td>{{ formatPostCode($item['post_code']) }}</td>
                                    <td>{{ $item['address'] }}</td>
                                    <td>{{ $item['note'] }}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        @endforeach
    @endif

    <div class="d-flex justify-content-center mt-4">
        <a href="javascript:void(0)" id="back-import_csv"
           class="btn-custom btn-custom-disabled btn-custom-large md mx-4">戻る</a>
        <button type="submit" class="btn-custom btn-custom-primary btn-custom-large md mx-4">登録</button>
    </div>
</form>
