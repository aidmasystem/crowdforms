@extends('client.layouts.master')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large mb-0"><i class="fas fa-plus main_icon"></i>送信先新規登録</h2>
    </div>
    <!--table-->
    <form action="{{ route('client.customer.create_confirm') }}" method="POST" id="frm" class="form-custom h-adr">
        @csrf
        <div class="card-custom card-table mb-30">
            <table class="table table-bordered mb-0">
                <tbody>
                <tr>
                    <th class="th-header">
                        氏名 <span class="card-table_label">必須</span> <br>
                        @if($errors->has('full_name'))
                            <small class="form-text text-error">{{ $errors->first('full_name') }}</small>
                        @endif
                    </th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-6 col-custom">
                                <input type="text" name="full_name"
                                       value="{{ str_replace('　', '', old('full_name', @$full_name)) }}"
                                       class="form-control @if($errors->has('full_name')) border-error @endif"
                                       maxlength="255" placeholder="氏名を入力してください">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">
                        フリガナ <span class="card-table_label">必須</span><br>
                        @if($errors->has('full_name_furi'))
                            <small class="form-text text-error">{{ $errors->first('full_name_furi') }}</small>
                        @endif
                    </th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-6 col-custom">
                                <input type="text" name="full_name_furi"
                                       value="{{ str_replace('　', '', old('full_name_furi', @$full_name_furi)) }}"
                                       class="form-control @if($errors->has('full_name_furi')) border-error @endif"
                                       maxlength="255" placeholder="フリガナを入力してください">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>企業名</th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-5 col-custom">
                                <input type="text" name="company_name" maxlength="255"
                                       value="{{ str_replace('　', '', old('company_name', @$company_name)) }}"
                                       class="form-control" placeholder="企業名を入力してください">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>部署名</th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-4 col-custom">
                                <input type="text" name="department_name"
                                       value="{{ str_replace('　', '', old('department_name', @$department_name)) }}"
                                       maxlength="255" class="form-control" placeholder="部署名を入力してください">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>役職名</th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-4 col-custom">
                                <input type="text" name="position" maxlength="255"
                                       value="{{ str_replace('　', '', old('position', @$position)) }}" class="form-control"
                                       placeholder="役職名を入力してください">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">
                        電話番号 <span class="card-table_label">必須</span><br>
                        @if($errors->has('tel'))
                            <small class="form-text text-error">{{ $errors->first('tel') }}</small>
                        @endif
                    </th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-4 col-custom">
                                <input type="text" name="tel" value="{{ str_replace('　', '', old('tel', @$tel)) }}"
                                       class="form-control  @if($errors->has('tel')) border-error @endif"
                                       placeholder="電話番号を入力してください" maxlength="11">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">
                        メールアドレス <span class="card-table_label">必須</span><br>
                        @if($errors->has('email'))
                            <small class="form-text text-error">{{ $errors->first('email') }}</small>
                        @endif
                    </th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-7 col-custom">
                                <input type="text" name="email" value="{{ str_replace('　', '', old('email', @$email)) }}"
                                       class="form-control @if($errors->has('email')) border-error @endif"
                                       maxlength="255" placeholder="メールアドレスを入力してください">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="th-header">
                        住所 <span class="card-table_label">必須</span><br>
                        @if($errors->has('address'))
                            <small class="form-text text-error">{{ $errors->first('address') }}</small>
                        @elseif($errors->has('post_code_1') || $errors->has('post_code_2'))
                            <small class="form-text text-error">住所を正しく形式で入力してください (3数字- 4 数字 - 住所)</small>
                        @endif
                    </th>
                    <td>
                        <div class="row row-custom align-items-center flex-wrap">
                            <span class="p-country-name" style="display:none;">Japan</span>
                            <span class="pl-2">〒</span>
                            <div class="col-1 col-custom">
                                <input type="text" name="post_code_1"
                                       value="{{ str_replace('　', '', old('post_code_1', @$post_code_1)) }}"
                                       class="p-postal-code form-control @if($errors->has('post_code_1')) border-error @endif"
                                       maxlength="3">
                            </div>
                            <span>-</span>
                            <div class="col-2 col-custom">
                                <input type="text" name="post_code_2"
                                       value="{{ str_replace('　', '', old('post_code_2', @$post_code_2)) }}"
                                       class="p-postal-code form-control @if($errors->has('post_code_2')) border-error @endif"
                                       maxlength="4">
                            </div>
                            <div class="col-6 col-custom">
                                <input type="text" name="address" maxlength="512"
                                       value="{{ str_replace('　', '', old('address', @$address)) }}"
                                       class="p-region p-locality p-street-address p-extended-address form-control @if($errors->has('address')) border-error @endif"
                                       placeholder="住所を入力してください">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>備考</th>
                    <td>
                        <div class="row row-custom align-items-center">
                            <div class="col-10 col-custom">
                                <textarea class="form-control" placeholder="備考を入力してください" name="note" id="" cols="30" row
                                          row-customs="10" maxlength="512">{{ str_replace('　', '', old('note', @$note)) }}</textarea>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="form-check form-check-inline mt-3">
                <input class="form-check-input" type="radio" name="can_duplicate" id="registerEmail"
                       {{ old('can_duplicate', @$can_duplicate) == \App\Models\Customer::CAN_DUPLICATE ? 'checked' : '' }} value="{{ \App\Models\Customer::CAN_DUPLICATE }}">
                <label class="form-check-label" for="registerEmail">
                    メールアドレスの重複をそのまま登録する
                </label>
            </div>
            <br>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="can_duplicate" id="registerCheckEmail"
                       {{ old('can_duplicate', @$can_duplicate) == \App\Models\Customer::CAN_NOT_DUPLICATE ? 'checked' : '' }} value="{{ \App\Models\Customer::CAN_NOT_DUPLICATE }}">
                <label class="form-check-label" for="registerCheckEmail">
                    メールアドレスで重複を一意にする
                </label>
            </div>
            <p class="mb-0">メールアドレスで重複を一意にする場合は、入力したメールアドレスを元にデータベースのデータを纏めます。</p>
        </div>
        <div class="d-flex justify-content-center">
            <a href="{{ route('client.customer.index') }}" class="btn-custom btn-custom-disabled md mx-4 btn-custom-large">戻る</a>
            <button type="submit" id="btn-submit" class="btn-custom btn-custom-primary md mx-4 btn-custom-large">確認</button>
        </div>
    </form>
@endsection
