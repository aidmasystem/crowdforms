@if($lists->isNotEmpty())
    @foreach($lists as $row)
        <tr>
            <td class="main_table_action">
                <div class="form-check pl-2">
                    <input class="form-check-input large select-customer" value="{{ $row->id }}" type="checkbox">
                </div>
            </td>
            <td class="main_table_action">
                <a href="javascript:void(0);" data-id="{{ $row->id }}" data-url="{{ route('client.customer.destroy', $row->id) }}" class="delete-customer"
                   data-toggle="modal" data-target="#deleteCustomer">
                    <i class="fas fa-trash-alt main_icon m-0"></i>
                </a>
            </td>
            <td class="main_table_action">
                <a href="{{ route('client.customer.edit', $row->id) }}"><i
                        class="fas fa-pen main_icon m-0"></i></a>
            </td>
            <td class="text-overflow title-survey" data-toggle="tooltip" title="{{ $row->full_name }}">{{ $row->full_name }}</td>
            <td class="large text-overflow" data-toggle="tooltip" title="{{ $row->company_name }}">{{ $row->company_name }}</td>
            <td class="text-overflow title-survey" data-toggle="tooltip" title="{{ $row->department_name }}">{{ $row->department_name }}</td>
            <td class="text-overflow title-survey" data-toggle="tooltip" title="{{ $row->position }}">{{ $row->position }}</td>
            <td class="text-nowrap">{{ $row->created_at }}</td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="8">
            <div class="text-center">データがありません。</div>
        </td>
    </tr>
@endif
