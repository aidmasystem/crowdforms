@extends('client.layouts.master')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large mb-0"><i class="fs-20 fas fa-clipboard-check main_icon"></i>アンケート結果集計</h2>
    </div>
    <!--table-->
    <div class="main_table card-custom">
        <div class="d-flex justify-content-between mb-2">
            <div class="d-flex form-custom align-items-center col-8 px-0">
                <div class="form-custom_select mr-3 col-6 px-0 multi sm">
                    <select class="selectpicker select_1"
                            data-size="10"
                            id="select_survey"
                            data-live-search="true"
                            data-actions-box="true"
                            data-live-search-placeholder="検索"
                            data-select-all-text="全て"
                            data-deselect-all-text="全て"
                            title="選択してください"
                            multiple>
                        <optgroup label="候補" class="pending">
                            @foreach($survey as $row)
                                <option class="option-select-statistic text-overflow"
                                        value="{{ $row['survey_id'] }}">{{ $row['title'] }}</option>
                            @endforeach
                        </optgroup>
                    </select>
                </div>
                <p class="fs-20 mb-0 count">件数 {{@$count}}件</p>
            </div>
            <ul class="pagination">
                {!! $statistic->appends($_GET)->links() !!}
            </ul>
        </div>
        <div class="table-responsive mb-2">
            <table class="table table-bordered text-center mb-0 ">
                <thead>
                <tr>
                    <th scope="col" class="main_table_action large border-right-0">詳細</th>
                    <th scope="col" class="border-left-0">アンケート名</th>
                    <th scope="col">アンケート詳細</th>
                    <th scope="col">送信数<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                               class="img-fluid mb-1 sort-heading" id="send" data-sort="asc"></span>
                    </th>
                    <th scope="col">送信完了件数<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                                  class="img-fluid mb-1 sort-heading" id="success" data-sort="asc"></span>
                    </th>
                    <th scope="col">送信エラー数<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                                  class="img-fluid mb-1 sort-heading"
                                                                  id="fail" data-sort="asc"></span></th>
                    <th scope="col">回答状況<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                                class="img-fluid mb-1 sort-heading"
                                                                id="reply" data-sort="asc"></span></th>
                    <th scope="col">各項目件数<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                                 class="img-fluid mb-1 sort-heading" id="question" data-sort="asc"></span>
                    </th>
                    <th scope="col">作成日時<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt=""
                                                                class="img-fluid mb-1 sort-heading"
                                                                id="date" data-sort="asc"></span></th>
                </tr>
                <tr class="total">
                    <th scope="col" class="main_table_action large border-right-0 border_less"></th>
                    <th scope="col" class="border-left-0"></th>
                    <th scope="col" class="font-family-w3 font-weight-normal">合計</th>
                    <th scope="col" class="font-family-w3 font-weight-normal" id="sum_send">{{$sum['send'] ?? 0}}</th>
                    <th scope="col" class="font-family-w3 font-weight-normal" id="sum_success">{{$sum['success'] ?? 0}}</th>
                    <th scope="col" class="font-family-w3 font-weight-normal" id="sum_fail">{{$sum['fail'] ?? 0}}</th>
                    <th scope="col" class="font-family-w3 font-weight-normal" id="sum_reply">{{$sum['reply'] ?? 0}}</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody class="table-search">
                @include('client.statistic.box_table')
                </tbody>
            </table>
        </div>
        <div class="d-flex justify-content-between mb-2">
            <div class="d-flex form-custom align-items-center col-8 px-0">
                <div class="form-custom_select mr-3 col-6 px-0 multi sm">
                    <select class="selectpicker select_2"
                            data-size="10"
                            id="select_survey"
                            data-live-search="true"
                            data-actions-box="true"
                            data-live-search-placeholder="検索"
                            data-select-all-text="全て"
                            data-deselect-all-text="全て"
                            title="選択してください"
                            multiple>
                        <optgroup label="候補" class="pending">
                            @foreach($survey as $row)
                                <option class="option-select-statistic text-overflow"
                                        value="{{ $row['survey_id'] }}">{{ $row['title'] }}</option>
                            @endforeach
                        </optgroup>
                    </select>
                </div>
                <p class="fs-20 mb-0 count">件数 {{@$count}}件</p>
            </div>
            <ul class="pagination">
                {!! $statistic->appends($_GET)->links() !!}
            </ul>
        </div>

        <input type="hidden" name="hidden_page" id="hidden_page" value="1"/>
        <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="" />
        <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="" />
        <input type="hidden" name="group_survey" id="group_survey" value="" />
    </div>

@endsection

@push('script')
    <script>
        $(document).ready(function () {
            $(".selectpicker").val('default').selectpicker("refresh");
            //add optgroup to selectepicker if it did not exist
            $('select.select_1').on('change', function () {
                if ($('.select_1 optgroup[label="選択中"]').length === 0) $('select.select_1').prepend(`<optgroup label="選択中" class="selected"></optgroup>`);
                if ($('.select_2 optgroup[label="選択中"]').length === 0) $('select.select_2').prepend(`<optgroup label="選択中" class="selected"></optgroup>`);
                if ($('.select_1 optgroup[label="候補"]').length === 0) $('select.select_1').append(`<optgroup label="候補" class="pending"></optgroup>`);
                if ($('.select_2 optgroup[label="候補"]').length === 0) $('select.select_2').append(`<optgroup label="候補" class="pending"></optgroup>`);
            })
            $('select.select_2').on('change', function () {
                if ($('.select_1 optgroup[label="選択中"]').length === 0) $('select.select_1').prepend(`<optgroup label="選択中" class="selected"></optgroup>`);
                if ($('.select_2 optgroup[label="選択中"]').length === 0) $('select.select_2').prepend(`<optgroup label="選択中" class="selected"></optgroup>`);
                if ($('.select_1 optgroup[label="候補"]').length === 0) $('select.select_1').append(`<optgroup label="候補" class="pending"></optgroup>`);
                if ($('.select_2 optgroup[label="候補"]').length === 0) $('select.select_2').append(`<optgroup label="候補" class="pending"></optgroup>`);
            })
            //search statistic
            $('.selectpicker').on('change', function () {
                check_cookie()
                let group_survey = []
                let $pending_1 = $('.select_1 optgroup.pending')
                let $selected_1 = $('.select_1 optgroup.selected');
                let $pending_2 = $('.select_2 optgroup.pending')
                let $selected_2 = $('.select_2 optgroup.selected');
                $pending_1.find("option:selected").each(function () {
                    let selected_val = $(this).val()
                    let $clone_1 = $(this).clone()
                    let $clone_2 = $pending_2.find("option[value="+ selected_val +"]").clone()
                    $pending_2.find("option[value="+ selected_val +"]").remove()
                    $(this).remove()
                    $selected_1.append($clone_1.attr('selected', true))
                    $selected_2.append($clone_2.attr('selected', true))
                })
                $selected_1.find("option:not(:selected)").each(function () {
                    if ($('.select_1 optgroup[label="候補"]').length){
                        let not_selected_val = $(this).val()
                        let $clone_1 = $(this).clone()
                        let $clone_2 = $selected_2.find("option[value="+ not_selected_val +"]").clone()
                        $(this).remove()
                        $selected_2.find("option[value="+ not_selected_val +"]").remove()
                        $pending_1.append($clone_1.attr('selected', false))
                        $pending_2.append($clone_2.attr('selected', false))
                        $('.select_1 button.bs-select-all').removeClass('d-none')
                        $('.select_1 button.bs-deselect-all').removeClass('d-block')
                        $('.select_2 button.bs-select-all').removeClass('d-none')
                        $('.select_2 button.bs-deselect-all').removeClass('d-block')
                    }
                })
                $pending_2.find("option:selected").each(function () {
                    let selected_val = $(this).val()
                    let $clone_1 = $(this).clone()
                    let $clone_2 = $pending_1.find("option[value="+ selected_val +"]").clone()
                    $pending_1.find("option[value="+ selected_val +"]").remove()
                    $(this).remove()
                    $selected_1.show().append($clone_1.attr('selected', true))
                    $selected_2.show().append($clone_2.attr('selected', true))
                })
                $selected_2.find("option:not(:selected)").each(function () {
                    if ($('.select_2 optgroup[label="候補"]').length){
                        let not_selected_val = $(this).val()
                        let $clone_1 = $(this).clone()
                        let $clone_2 = $selected_1.find("option[value="+ not_selected_val +"]").clone()
                        $(this).remove()
                        $selected_1.find("option[value="+ not_selected_val +"]").remove()
                        $pending_1.show().append($clone_1.attr('selected', false))
                        $pending_2.show().append($clone_2.attr('selected', false))
                        $('.select_1 button.bs-select-all').removeClass('d-none')
                        $('.select_1 button.bs-deselect-all').removeClass('d-block')
                        $('.select_2 button.bs-select-all').removeClass('d-none')
                        $('.select_2 button.bs-deselect-all').removeClass('d-block')
                    }
                })
                $selected_1.find("option:selected").each(function () {
                    group_survey.push($(this).val())
                })
                // if otpgroup selected is empty, remove it
                // if otpgroup pending is empty, remove it and select "select all" button
                if($selected_1.children('option').length === 0) $selected_1.remove()
                if($pending_1.children('option').length === 0){
                    $pending_1.remove()
                    $('.select_1 button.bs-select-all').addClass('d-none')
                    $('.select_1 button.bs-deselect-all').addClass('d-block')
                }
                if($selected_2.children('option').length === 0) $selected_2.remove()
                if($pending_2.children('option').length === 0){
                    $pending_2.remove()
                    $('.select_2 button.bs-select-all').addClass('d-none')
                    $('.select_2 button.bs-deselect-all').addClass('d-block')
                }
                //refresh both selectpicker
                $('.select_1').selectpicker('refresh');
                $('.select_2').selectpicker('refresh');

                $('#group_survey').val('' + group_survey);
                var sort_name = $('#hidden_column_name').val();
                var sort_by = $('#hidden_sort_type').val();
                fetchData(1, sort_name, sort_by);
            })

            $(".sort-heading").on('click', function (e) {
                e.preventDefault();
                let sort_name = $(this).attr('id');
                let sort_by = $(this).data('sort');
                clearIcon(sort_name);

                let page = 1;
                $('#hidden_column_name').val(sort_name);
                $('#hidden_sort_type').val(sort_by);

                fetchData(page, sort_name, sort_by);
            });

            $(document).on('click', '.pagination a', function(e){
                e.preventDefault();
                let page = $(this).attr('href').split('page=')[1];
                $('#hidden_page').val(page);
                var column_name = $('#hidden_column_name').val();
                var sort_by = $('#hidden_sort_type').val();

                fetchData(page, column_name, sort_by);
            });

            function fetchData(page, column_name, sort_by) {
                let getId = column_name;
                let sortBy = sort_by;
                let group_survey = $('#group_survey').val();
                $.ajax({
                    url: '{{ route('client.statistic.index') }}',
                    type: 'GET',
                    data: {
                        page: page,
                        column: column_name,
                        sortOrder: sort_by,
                        group_survey: group_survey
                    },
                    beforeSend: showLoad(),
                    success:function(data) {
                        var select_1 = $('.select_1')
                        var $title_1 = select_1.parent().find('.filter-option-inner-inner');
                        var $title_2 = $('.select_2').parent().find('.filter-option-inner-inner');
                        var selectedText = $title_1.text();
                        var text_length = select_1.find('option:selected').first().text().length
                        var first = select_1.find('option:selected').first().text().substr(0, 30) + (text_length > 30 ? "..." : '')
                        var length = select_1.find('option:selected').length
                        if (select_1.parent().find('.bs-placeholder').length === 0) {
                            var selectedCount = selectedText.split(', ').length;
                            if( selectedCount > 1) {
                                selectedText = first + ", 他" + (length - 1) + "件";
                            }
                            $title_1.text(selectedText);
                            $title_2.text(selectedText)
                        }
                        hideLoad();
                        $("p.count").each(function () {
                            $(this).text('件数 ' + data.count + '件')
                        })
                        if (sortBy == 'asc') {
                            $('#' + getId).data('sort', 'desc');
                            $('#' + getId).attr('src', '{{ asset('images/icons/sort-asc.svg') }}');
                        } else if (sortBy == 'desc') {
                            $('#' + getId).data('sort', 'all');
                            $('#' + getId).attr('src', '{{ asset('images/icons/sort-desc.svg') }}');
                        } else if (sortBy == 'all') {
                            $('#' + getId).data('sort', 'asc');
                            $('#' + getId).attr('src', '{{ asset('images/icons/sort.svg') }}');
                        }

                        $('ul.pagination').html('');
                        if (data.count > 30) {
                            $('ul.pagination').html(data.paginate);
                        }
                        $(".table tr:not(:lt(2))").remove();
                        $(".table").append(data.html);
                        var header = $('table thead tr.total')
                        header.find('th#sum_send').text(data.sum['send'])
                        header.find('th#sum_success').text(data.sum['success'])
                        header.find('th#sum_fail').text(data.sum['fail'])
                        header.find('th#sum_reply').text(data.sum['reply'])
                        // window.history.pushState("", "", this.url);
                    }
                });
            }
        });
    </script>
@endpush
