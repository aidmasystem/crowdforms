@if($statistic->isNotEmpty())
    @foreach($statistic as $row)
        <tr>
            <td class="main_table_action large px-3"><a href="{{route('client.statistic.detail', $row->survey_id)}}" class="btn-custom btn-custom-outlined">詳細</a></td>
            <td class="title-survey text-overflow" data-toggle="tooltip" title="{{ @$row->detail }}">{{@$row->title}}</td>
            <td class="large text-overflow" data-toggle="tooltip" title="{{ @$row->detail }}">{{ @$row->detail }}</td>
            <td>{{$row->total_send}}</td>
            <td>{{$row->total_success}}</td>
            <td>{{$row->total_fail}}</td>
            <td>{{$row->total_reply ?? 0}}</td>
            <td>{{$row->total_question}}</td>
            <td class="text-nowrap">{{$row->updated_at}}</td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="9">
            <div class="text-center">データがありません。</div>
        </td>
    </tr>
@endif
