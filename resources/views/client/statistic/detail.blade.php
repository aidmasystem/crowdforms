@extends('client.layouts.master')

@section('content')
    <!-- content-->
    <div class="d-flex align-items-center justify-content-between mb-30">
        <h2 class="title title-large mb-0"><i class="fs-20 fas fa-clipboard-check main_icon"></i>アンケート結果集計</h2>
    </div>
    <div class="card-custom mb-30">
        <p class="font-family-w6">アンケートタイトル</p>
        <h3 class="title-large fs-24 px-3 mb-3">{{$survey->title}}</h3>
        <p class="font-family-w6">アンケート詳細</p>
        <h4 class="fs-16 mb-0 px-3">{!! nl2br(@$survey->detail) !!}</h4>
    </div>

    <div class="card-custom form-custom main_table">
        <p class="fs-20">回答数 {{$count->total_reply ?? 0}}件</p>
        @if($survey->question->isNotEmpty())
            @foreach($survey->question as $row)
                @if($row->type == \App\Models\Question::TYPE_RADIO || $row->type == \App\Models\Question::TYPE_CHECKBOX)
                    <div class="mb-20">
                        <h4 class="fs-14 font-family-w6">{{$row->content}}</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered text-center mb-0 table-detail">
                                <thead>
                                <tr>
                                    <th scope="col">項目名</th>
                                    <th scope="col">結果<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1 sort-heading" id="asc"></span></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($row->answer as $item)
                                    <tr>
                                        <td>{{$item->content}}</td>
                                        <td>{{$item->getResultAnswer($item->question_id)[$item->id] ?? 0}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @elseif($row->type == \App\Models\Question::TYPE_ESSAY)
                    <div class="mb-2">
                        <h4 class="fs-14 font-family-w6">{{$row->content}}</h4>
                        <div class="table-responsive">
                            <table class="table mb-0 table-detail table-essay" data-slice="{{$row->id}}">
                                <thead>
                                <tr>
                                    <th scope="col">回答</th>
                                </tr>
                                </thead>
                                <tbody id="essay_table-{{$row->id}}" data-show="5">
                                    @include('client.statistic.box_essay')
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="mb-3 d-flex flex-row-reverse essay-button-{{$row->id}}">
                        <button class=" btn-custom btn-custom-outlined more_answer" value="{{$row->id}}"><i class="fas fa-edit main_icon"></i>回答をもっと表示</button>
                        <button class="btn-custom btn-custom-outlined less_answer mr-2" value="{{$row->id}}">「回答を折りたたむ」</button>
                    </div>
                @elseif($row->type == \App\Models\Question::TYPE_SELECT_BOX)
                    <div class="mb-20">
                        <h4 class="fs-14 font-family-w6">{{$row->content}}（複数回答可能）</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered text-center mb-0 table-detail">
                                <thead>
                                <tr>
                                    <th scope="col">項目名</th>
                                    <th scope="col">結果<span class="ml-1"><img src="{{ asset('images/icons/sort.svg') }}" alt="" class="img-fluid mb-1 sort-heading" id="asc"></span></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($row->answer as $item)
                                    <tr>
                                        <td>{{$item->content}}</td>
                                        <td>{{$item->getResultAnswer($item->question_id)[$item->id] ?? 0}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
            @endforeach
        @endif
    </div>
@endsection
@push('script')
    <script>
        var numShown = 5; // Initial rows shown & index
        let numMore = 10;  // Increment
        $('.table-essay').each(function() {
            let question_id = $(this).data('slice')
            let $table = $(this).find('tbody');  // tbody containing all the rows
            $table.find('tr:gt(' + (numShown - 1) + ')').hide().end()
            if ($table.find('tr').length <= numShown){
                $('.essay-button-'+question_id).find('button.more_answer').hide()
            }
            $('.essay-button-'+question_id).find("button.less_answer").hide();
        })
        $(document).ready(function(){
            $('.table-detail').each(function() {
                if ($(this).find('td').length === 0) {
                    $(this).find('tbody').append(
                            `<tr><td colspan="2">`+
                            `<div class="text-center">データがありません。</div>`+
                            `</td></tr>`
                        )
                }
            })
            $(".more_answer").click(function() {
                let button = $(this)
                let question_id = $(this).val()
                let $table = $('.table-essay').find(`tbody[id="essay_table-${question_id}"]`);  // tbody containing all the rows
                var numRows = $table.find('tr').length; // Total # rows
                var $show = parseInt($table.attr('data-show'))
                $table.attr('data-show', $show +=  numMore)
                // no more "show more" if done
                if ($show >= numRows){
                    button.hide()
                    $('.essay-button-'+question_id).find("button.less_answer").show()
                }
                $table.find('tr:lt(' + $show + ')').show();
            })
            $(".less_answer").click(function() {
                let button = $(this)
                let question_id = $(this).val()
                let $table = $('.table-essay').find(`tbody[id="essay_table-${question_id}"]`);  // tbody containing all the rows
                $table.attr('data-show', numShown)
                button.hide()
                $table.find('tr:gt(' + (numShown - 1) + ')').hide().end();
                $('.essay-button-'+question_id).find("button.more_answer").show()
            })
            $(".sort-heading").unbind('click');
            $('.sort-heading').click(function(){
                let getSortHeading = $(this);
                let splitOrder = getSortHeading.attr('id');
                var table = $(this).parents('table').eq(0)
                var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index(1)))

                if (splitOrder == 'asc') {
                    getSortHeading.attr('id', 'desc');
                    getSortHeading.attr('src', '{{ asset('images/icons/sort-asc.svg') }}');
                } if (splitOrder == 'desc') {
                    getSortHeading.attr('id', 'asc');
                    getSortHeading.attr('src', '{{ asset('images/icons/sort-desc.svg') }}');
                }
                this.asc = !this.asc
                if (!this.asc){rows = rows.reverse()}
                for (var i = 0; i < rows.length; i++){table.append(rows[i])}
            })
            function comparer(index) {
                return function(a, b) {
                    var valA = getCellValue(a, index),
                        valB = getCellValue(b, index)
                    return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.toString().localeCompare(valB)
                }
            }
            function getCellValue(row, index){ return $(row).children('td').eq(index).text() }
        })
    </script>
@endpush
