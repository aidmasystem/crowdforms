<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
        <link href="{{ asset('css/fontawesome/css/fontawesome.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('css/fontawesome/css/solid.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('css/fontawesome/css/brands.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('css/bootstrap/bootstrap-select.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('css/bootstrap/bootstrap-datetimepicker.min.css') }}" rel="stylesheet"/>
    
    </head>
    <body>
        <!--header-->
        <header class="header">
            <div class="header_content">
                <div class="container-custom">
                    <div class="header_logo"><img src="{{ asset('images/logo.svg') }}" alt="" class="img-fluid"></div>
                </div>
            </div>
            <!--navbar-->
            <nav class="nav">
                <div class="container-custom px-0 d-flex justify-content-between align-items-center">
                    <ul class="nav_list d-flex">
                        <li class="nav_item">
                            <a class="nav_link" href="/"><i class="fas fa-edit main_icon"></i>アンケート一覧</a>
                        </li>
                        <li class="nav_item">
                            <a class="nav_link" href=""><i class="fas fa-paper-plane main_icon"></i>送信先一覧</a>
                        </li>
                        <li class="nav_item">
                            <a class="nav_link" href=""><i class="fas fa-clipboard-check main_icon"></i>アンケート結果集計</a>
                        </li>
                    </ul>
                    <div class="dropdown">
                        <div class="dropdown-toggle" id="dropdownAddress" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="line-clamp">     
                            株式会社アイドマ・ホールディングス 田中 太郎
                        </div>
                            <span class="dropdown_icon"></span>
                        </div>
                        <div class="dropdown-menu" aria-labelledby="dropdownAddress">
                            <a class="dropdown-item" href="#"><i class="fas fa-user main_icon"></i>アカウント管理</a>
                            <a class="dropdown-item" href="#"><i class="fas fa-sign-out-alt main_icon"></i>ログアウト</a>
                        </div>
                    </div>
                </div>
            </nav>
        </header>

        <!--main-->
        <div class="main main-bg">
            <div class="container-custom main_content">
            @yield('content')
            </div>
        </div>
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/moment/moment.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
        <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
        <script>
            $(document).ready(function(e) {
                //active menu
                var url = window.location.href;
                url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
                url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
                url = url.substr(url.lastIndexOf("/") + 1);
                
                $('.nav_link').each(function(){
                    var href = $(this).attr('href');
                    if("/" + url == href){
                        $(this).addClass('active');
                    }
                    var res = window.location.href.split("/");
                    for(let i = 0; i < res.length; i++) {
                        if("/" + res[i] == href && href != "/") {
                            $(this).addClass('active'); 
                        }
                    }
                });

                $('.selectpicker').selectpicker();
                $('.bs-select-all').click(function() {
                    $('.bs-deselect-all').addClass('d-block');
                    $('.bs-select-all').addClass('d-none');
                });
                $('.bs-deselect-all').click(function() {
                    $('.bs-deselect-all').removeClass('d-block');
                    $('.bs-select-all').removeClass('d-none');
                });

                $('#datepicker').datetimepicker({
                    defaultDate: new Date(),
                    format: 'YYYY/MM/DD',
                    // inline: true,
                });
                $('#datepicker1').datetimepicker({
                    defaultDate: new Date(),
                    format: 'YYYY/MM/DD',
                    // inline: true,
                });

                //select type
                $('.selectType').selectpicker();
                 
                $('.selectType').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
                    var val = $(this).val();
                    if(val != '') {
                        $('.card-custom_item').each(function() {
                            $(this).addClass('d-none');
                        });
                        $('#' + val).removeClass('d-none');
                    }
                });

                //checkbox
                $("#checkAll").click(function () {
                    $('.main_table_action input:checkbox').not(this).prop('checked', this.checked);
                });
                $("input[name='chkSearch']").change(function() {
                    if($('input[name="chkSearch"]:checked').length == $('input[name="chkSearch"]').length) {
                        $("#checkAll").prop('checked', this.checked);
                    } else {
                        $("#checkAll").prop('checked', false);
                    }
                });
                //upload file
                $('#upload').change(function(e){
                    if($(this).val() != '') {
                        $(".form-custom_upload_list").append("<div class='form-custom_upload_item' data-toggle='collapse' data-target='#upload-content'>" + $(this).val().replace(/.*(\/|\\)/, '') + "<span class='close'></span></div>");
                        $('.form-custom_upload_item .close').click(function(event){
                            event.stopPropagation();
                            $(this).parent('.form-custom_upload_item').remove();
                        });
                    }
                });
                $(function () {
                    $('#sortable').sortable();
                });
                $(function () {
                    $('#sortable_answer').sortable();
                })
            });
        </script>
    </body>
</html>
