<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
        <link href="{{ asset('css/fontawesome/css/fontawesome.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('css/fontawesome/css/solid.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('css/fontawesome/css/brands.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/css/bootstrap-select.min.css">

    </head>
    <body>
        <!--header-->
        <header class="header header-admin">
            <div class="header_content">
                <div class="container-custom">
                    <div class="header_logo"><img src="{{ asset('images/logo.svg') }}" alt="" class="img-fluid"></div>
                </div>
            </div>
            <!--navbar-->
            <nav class="nav">
                <div class="container-custom px-0 d-flex justify-content-between align-items-center">
                    <ul class="nav_list d-flex">
                        <li class="nav_item">
                            <a class="nav_link" href="/admin"><i class="fas fa-user main_icon"></i>AAアカウント管理</a>
                        </li>
                        <li class="nav_item">
                            <a class="nav_link" href="/admin/manage"><i class="fas fa-building main_icon"></i>クライアント管理</a>
                        </li>
                    </ul>
                    <div class="dropdown">
                        <div class="dropdown-toggle" id="dropdownAddress" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            株式会社アイドマ・ホールディングス 田中 太郎
                            <span class="dropdown_icon"></span>
                        </div>
                        <div class="dropdown-menu" aria-labelledby="dropdownAddress">
                            <a class="dropdown-item" href="#"><i class="fas fa-sign-out-alt main_icon"></i>ログアウト</a>
                        </div>
                    </div>
                </div>
            </nav>
        </header>

        <!--main-->
        <div class="main main-bg">
            <div class="container-custom main_content">
            @yield('content')
            </div>
        </div>
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/js/bootstrap-select.min.js"></script>
        <script>
            $(document).ready(function(e) {
                 //active menu
                var url = window.location.href;
                url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
                url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));  
                url = url.substr(url.lastIndexOf("/") + 1);

                $('.nav_link').each(function(){
                    var href = $(this).attr('href');
                    if(url == "admin" && "/" + url == href) {
                        $(this).addClass('active');
                    }
                    if("/admin/" + url == href) {
                        $(this).addClass('active');
                    }
                });

                $('.selectpicker').selectpicker();
                $('.bs-select-all').click(function() {
                    $('.bs-deselect-all').addClass('d-block');
                    $('.bs-select-all').addClass('d-none');
                });
                $('.bs-deselect-all').click(function() {
                    $('.bs-deselect-all').removeClass('d-block');
                    $('.bs-select-all').removeClass('d-none');
                });
            });
        </script>
    </body>
</html>
