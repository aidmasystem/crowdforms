<?php

return [
    'roles' => [
        '1' => '管理者',
        '2' => '一般社員',
        '3' => 'バイト'
    ],
    'ITEMS_PER_PAGE' => 30,
];
