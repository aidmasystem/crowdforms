$(function() {
    $('.main_table_action .select-all').change(function () {
        var prop = $(this).prop('checked');
        if (prop) {
            $('.main_table_action input[type=checkbox]').each(function () {
                $(this).prop("checked", true).attr('checked', 'checked');
                $(this).parent().addClass('checked');
            });
        } else {
            $('.main_table_action input[type=checkbox]').each(function () {
                $(this).prop("checked", false).removeAttr('checked');
                $(this).parent().removeClass('checked');
            });
        }
    })

    $('.main_table_action .select-customer').change(function() {
        var prop = $(this).prop('checked');
        if (prop) {
            $(this).find('input[type=checkbox]').each(function(){
                $(this).prop("checked",true).attr('checked', 'checked');
                $(this).parent().addClass('checked');
            });
        } else {
            $(this).find('input[type=checkbox]').each(function(){
                $(this).prop("checked",false).removeAttr('checked');
                $(this).parent().removeClass('checked');
            });
            $('.main_table_action .select-all').prop("checked", false).removeAttr('checked');
        }
        var numberOfChecked = $('input:checkbox:checked').length;
        var totalCheckboxes = $('input:checkbox').length - 1;

        if (numberOfChecked === totalCheckboxes) {
            $('.select-all').prop("checked", true).attr('checked', 'checked');
        }
    })

});
