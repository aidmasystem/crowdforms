$(document).ready(function (e) {
    $('.selectpicker').selectpicker();
    $('.bs-select-all').click(function () {
        $('.bs-deselect-all').addClass('d-block');
        $('.bs-select-all').addClass('d-none');
    });
    $('.bs-deselect-all').click(function () {
        $('.bs-deselect-all').removeClass('d-block');
        $('.bs-select-all').removeClass('d-none');
    });

    $('#datepicker').datetimepicker({
        defaultDate: null,
        format: 'YYYY/MM/DD',
    });
    $('#datepicker1').datetimepicker({
        defaultDate: null,
        format: 'YYYY/MM/DD',
        useCurrent: false
    });
    $('#datepicker').on('dp.change', function (e) {
        $('#datepicker1').data("DateTimePicker").minDate(e.date);
    });
    $('#datepicker1').on('dp.change', function (e) {
        $('#datepicker').data("DateTimePicker").maxDate(e.date);
    });

    //select type
    $('.selectType').selectpicker();

    //checkbox
    $("#checkAll").click(function () {
        $('.main_table_action input:checkbox').not(this).prop('checked', this.checked);
    });

    // multi selectpicker not autofocus
    var blurState = false;
    $('.bs-searchbox .form-control').on('focus', function () {
        if (!blurState) {
            $(this).blur();
            blurState = true;
        }
    });
    $('.selectpicker').on('hide.bs.select', function () {
        blurState = false;
    });

    $('.selectpicker').selectpicker({
        noneResultsText:"データがありません {0}"
    })

    // create anketo
    $(function () {
        $('#sortable').sortable();
    });
    sortTableAnswer();

    $('#add-form-question').on('click', function () {
        var numberForm = $('input#length-key').val();
        $.ajax({
            type: 'GET',
            url: '/survey/form-question',
            data: {number: numberForm},
            beforeSend: showLoad(),
            success: function (data) {
                hideLoad();
                var newKey = parseInt(numberForm) + 1;
                $('div.all-form-question').append(data);
                $('input#length-key').val(newKey);
                $('.selectType').selectpicker();
                scrollQuestion(newKey);
                sortTableAnswer();
            },
            error: function (error) {
            },
        });
    });
    handleFomQuestion();

    // // Preview form anketo
    $('#preview-form-question').on('click', function (e) {
        e.preventDefault();
        $('input[type="text"]').each(function (k, v) {
            let valReplace = $(v).val().replaceAll('　', ' ');
            $(v).val(valReplace.trim());
        })
        $.ajax({
            type: 'POST',
            url: '/survey/preview',
            data: $('#form-create-survey').serialize(),
            beforeSend: showLoad(),
            success: function (data) {
                hideLoad();
                $('#box-form-create').hide();
                $('#back-index').hide();
                $('#btn-submit_survey').hide();
                $('#back-create').show();
                $('#box-form-preview').show();
                $('#box-form-preview').html(data);
                $('.selectType').selectpicker();
            },
            error: function (data) {
                hideLoad();
                var errors = data.responseJSON.errors;
                showErrorSurvey(errors);
            }
        });
    });

    // Submit form anketo
    $('#form-create-survey').on('submit', function (e) {
        e.preventDefault();
        $('input[type="text"]').each(function (k, v) {
            let valReplace = $(v).val().replaceAll('　', ' ');
            $(v).val(valReplace.trim());
        })
        var checkUpdate = $('#check-update-survey').val();
        var url = $('#form-create-survey').attr('action');
        var data = $('#form-create-survey').serialize();
        if (typeof checkUpdate !== 'undefined') {
            data = $('#form-create-survey').serialize() + '&_method=PUT';
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            beforeSend: showLoad(),
            success: function (data) {
                hideLoad();
                window.location = data.route;
            },
            error: function (data) {
                hideLoad();
                var errors = data.responseJSON.errors;
                showErrorSurvey(errors);
            }
        });
    });

    // Back form create
    $('#back-create').on('click', function () {
        $('#box-form-preview').hide();
        $('#back-create').hide();
        $('#box-form-create').show();
        $('#back-index').show();
        $('#btn-submit_survey').show();
    })

    $(document).on('click', '.select-customer', function () {
        var notCustomer = [];
        var customerChoose = $('#choose_customer').val();
        var arrChoose = customerChoose != '' ? customerChoose.split(',') : [];
        let valSelect = $(this).val();
        if ($(this).is(':checked')) {
            if (!arrChoose.includes(valSelect)) arrChoose.push(valSelect);
        } else {
            let indexCustomer = arrChoose.indexOf(valSelect);
            if (indexCustomer > -1) arrChoose.splice(indexCustomer, 1);
        }
        $('input.select-customer').each(function () {
            if (!$(this).is(':checked')) notCustomer.push(valSelect);
        })

        if (arrChoose.length > 0) $('.add-modal_send').removeClass('disable');
        else $('.add-modal_send').addClass('disable');

        if (notCustomer.length > 0) $('#select-all_customer').prop('checked', false);
        else $('#select-all_customer').prop('checked', true);

        $('#choose_customer').val(arrChoose.join());
    })

    $(document).on('click', '#select-all_customer', function () {
        var allCustomer = $('#check_all_customer').val();
        if ($(this).is(':checked')) {
            if (allCustomer != '') $('.add-modal_send').removeClass('disable');
            $('#choose_customer').val(allCustomer);
        } else {
            $('.add-modal_send').addClass('disable');
            $('#choose_customer').val('');
        }
    })

    // Show form send anketo
    $(document).on('click', '.add-modal_send', function () {
        var customer = $('#choose_customer').val();
        var arrCustomer = customer != '' ? customer.split(',') : [];

        $.ajax({
            type: 'GET',
            url: '/send-address',
            data: {arrCustomer: arrCustomer},
            beforeSend: showLoad(),
            success: function (data) {
                hideLoad();
                var config = {extraPlugins: 'confighelper'};
                $('#sendAddress .modal-content').html('');
                $('#sendAddress').modal('show');
                $('#sendAddress .modal-content').html(data);
                $('.selectType').selectpicker();
                var editor = CKEDITOR.replace('editorMess', config);
                CKEDITOR.addCss('::-webkit-scrollbar { width: 16px; background-color: #fff;}');
                CKEDITOR.addCss('::-webkit-scrollbar-thumb { background-color: #C4C4C4; border: 3px solid #fff;}');
                CKEDITOR.addCss('::-webkit-scrollbar-thumb { background-color: #C4C4C4; border: 3px solid #fff;}');
                CKEDITOR.addCss('.cke_editable p { margin: 0}');
                CKEDITOR.on('instanceReady', function () {
                    var contentSpace = editor.ui.space('contents');
                    var ckeditorFrameCollection = contentSpace.$.getElementsByTagName('iframe');
                    var ckeditorFrame = ckeditorFrameCollection[0];
                    var innerDoc = ckeditorFrame.contentDocument;

                    $('#sendAddress').on('shown.bs.modal', function () {
                        var innerDocTextAreaHeight = $(innerDoc.body).height();
                        if(innerDocTextAreaHeight > 200) {
                            $(innerDoc.body).css({"border-right":"1px solid #B0B4B9", "margin": "0", "padding":"15px 20px"});
                        }
                    });
                    $('iframe').contents().click(function(e) {
                        if(typeof e.target.href != 'undefined') {
                            window.open(e.target.href, 'new' + e.screenX);
                        }
                    });
                    editor.on('change', function (e) {
                        if($(innerDoc.body).height() > 170) {
                            $(innerDoc.body).css({"border-right":"1px solid #B0B4B9", "margin": "0", "padding":"15px 20px"});
                        }else {
                            $(innerDoc.body).css({"border-right":"0"});
                        }
                    });
                });
            },
            error: function (error) {
            },
        });
    })

    $(document).on('change', 'select#choose-survey', function () {
        console.log('change');
        $('.form-custom_select.select-survey_send .filter-option-inner-inner').css('color', '#000');
    });

    $(document).on('click', '#add-link_survey', function () {
        var surveyId = $('select#choose-survey').val();
        if (surveyId == '') return;
        $.ajax({
            type: 'GET',
            url: '/send-address/get-survey',
            data: {id: surveyId},
            beforeSend: showLoad(),
            success: function (data) {
                hideLoad();
                CKEDITOR.instances.editorMess.insertHtml(data);
            },
            error: function (error) {
            },
        });
    });

    // Submit form send
    $(document).on('click', '#send-mail_confirm', function (e) {
        e.preventDefault();
        $('input[type="text"]').each(function (k, v) {
            let valReplace = $(v).val().replaceAll('　', ' ');
            $(v).val(valReplace.trim());
        })
        var message = CKEDITOR.instances['editorMess'].getData();
        CKEDITOR.instances['editorMess'].setData(message.replace(/&nbsp;/,''))
        var data = $('#form-confirm-send').serializeArray();
        $.each(data, function (key, value) {
            if (value.name == "message") data[key]['value'] = message;
        });
        $.ajax({
            url: '/send-address/confirm',
            type: 'POST',
            data: data,
            beforeSend: showLoad(),
            success: function (data) {
                hideLoad();
                $('#sendAddress .modal-content').html('');
                $('#sendAddress .modal-content').html(data);
            },
            error: function (data) {
                hideLoad();
                var errors = data.responseJSON.errors;

                $('#form-confirm-send .error-mgs-type').html('');
                $('#form-confirm-send .error-mgs-name').html('');
                $('#form-confirm-send .error-mgs-customer').html('');
                $('#form-confirm-send .error-mgs-title').html('');
                $('#form-confirm-send .error-mgs-survey').html('');
                $('#form-confirm-send .error-mgs-message').html('');
                $('#form-confirm-send .input-mgs-type').removeClass('border-error');
                $('#form-confirm-send .input-mgs-name').removeClass('border-error');
                $('#form-confirm-send .input-mgs-customer').removeClass('border-error');
                $('#form-confirm-send .input-mgs-title').removeClass('border-error');
                $('#form-confirm-send .input-mgs-survey').removeClass('border-error');
                $('#sendAddress #cke_editorMess').removeClass('error-border');
                if (errors.send_type) {
                    $('#form-confirm-send .error-mgs-type').html(errors.send_type[0]);
                    $('#form-confirm-send .input-mgs-type').addClass('border-error');
                }
                if (errors.full_name) {
                    $('#form-confirm-send .error-mgs-name').html(errors.full_name[0]);
                    $('#form-confirm-send .input-mgs-name').addClass('border-error');
                }
                if (errors.customer_name) {
                    $('#form-confirm-send .error-mgs-customer').html(errors.customer_name[0]);
                    $('#form-confirm-send .input-mgs-customer').addClass('border-error');
                }
                if (errors.title) {
                    $('#form-confirm-send .error-mgs-title').html(errors.title[0]);
                    $('#form-confirm-send .input-mgs-title').addClass('border-error');
                }
                if (errors.question_package_id) {
                    $('#form-confirm-send .error-mgs-survey').html(errors.question_package_id[0]);
                    $('#form-confirm-send .input-mgs-survey').addClass('border-error');
                }
                if (errors.message) {
                    $('#form-confirm-send .error-mgs-message').html(errors.message[0]);
                    $('#sendAddress #cke_editorMess').addClass('error-border');
                }
            }
        });
    });

    $(document).on('click', '#send-mail_submit', function (e) {
        e.preventDefault();
        var data = $('#form-submit-send').serialize();
        $.ajax({
            url: '/send-address/send-submit',
            type: 'POST',
            data: data,
            beforeSend: showLoad(),
            success: function (data) {
                hideLoad();
                $('#sendAddress .modal-content').html('');
                $('#sendAddress .modal-content').html(data);
            },
        });
    });

    // Summary
    $('.add-modal_status').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: 'GET',
            url: '/summary/modal',
            beforeSend: showLoad(),
            success: function (data) {
                hideLoad();
                $('#checkStatus').modal('show');
                $('#checkStatus').on('shown.bs.modal', function () {
                    $('.style-scroll tbody').each(function () {
                        if ($(this)[0].scrollHeight > $(this).height() + 1) {
                            $(this).parent().addClass('scroll-cus');
                        }
                    });
                });
                $('#checkStatus .modal-content').html('');
                $('#checkStatus .modal-content').html(data);
            },
            error: function (error) {
            },
        });
    })

    $(document).on('click', '#modal-search_summary', function (e) {
        e.preventDefault();
        var searchSummary = $('#key-modal_summary').val();
        $.ajax({
            type: 'GET',
            url: '/summary/modal-search',
            data: {keyword: searchSummary},
            beforeSend: showLoad(),
            success: function (data) {
                hideLoad();
                $('tbody#body-modal_summary').html(data);
            },
            error: function (error) {
            },
        });
    });

    var ID = []
    $(".sort-modal_summary").each(function () {
        if (this.id) {
            ID.push(this.id)
        }
    });
    $(document).on('click', '.sort-modal_summary', function (e) {
        e.preventDefault();
        var getSortHeading = $(this);
        var getNextSortOrder = getSortHeading.attr('id');
        var arr = ID.filter(item => item !== getNextSortOrder);

        var splitID = getNextSortOrder.split('_');
        var splitName = splitID[0];
        var splitOrder = splitID[1];
        var search = $('#key-modal_summary').val();

        $.ajax({
            url: '/summary/modal-search',
            type: 'GET',
            data: {'column': splitName, 'sortOrder': splitOrder, keyword: search},
            success: function (response) {
                if (arr.indexOf(getNextSortOrder) < 0) {
                    $('.sort-modal_summary').each(function () {
                        if (arr.indexOf(this.id) < 0) {
                            let getId = this.id.split('_');
                            $(this).attr('id', getId[0] + '_asc');
                            $(this).attr('src', '/images/icons/sort.svg');
                        }
                    })
                }
                if (splitOrder == 'asc') {
                    getSortHeading.attr('id', splitName + '_desc');
                    getSortHeading.attr('src', '/images/icons/sort-asc.svg');
                } else if (splitOrder == 'desc') {
                    getSortHeading.attr('id', splitName + '_all');
                    getSortHeading.attr('src', '/images/icons/sort-desc.svg');
                } else {
                    getSortHeading.attr('id', splitName + '_asc');
                    getSortHeading.attr('src', '/images/icons/sort.svg');
                }

                $('tbody#body-modal_summary').html(response);
            }
        });
    });

    // resend email
    $(document).on('click', '.resend-email_survey', function (e) {
        e.preventDefault();
        let surveyId = $(this).data('id');
        if (surveyId != '') {
            $.ajax({
                type: 'GET',
                url: '/send-address/resend-email',
                data: {survey_id: surveyId},
                beforeSend: showLoad(),
                success: function (data) {
                    hideLoad();
                    $('#checkStatus .modal-content').html('');
                    $('#checkStatus .modal-content').html(data);
                },
                error: function (error) {
                },
            });
        }
    });

    var fileCsv = [];
    var nameCsv = [];

    //upload file
    $('#upload_csv').change(function (e) {
        var fileList = $(this)[0].files;
        handleFileUpload(fileList);
    });

    $(document).on('drop', '#dropContainer', function (event) {
        event.preventDefault();
        handleFileUpload(event.originalEvent.dataTransfer.files);
    })

    function handleFileUpload(filesObj) {
        if (filesObj != undefined) {
            $('#form-create_csv .error-msg_file').html('');
            $('.form-custom_upload_content').removeClass('error-file_csv');
            let totalSize = 0;
            let checkType = true;
            for (let i = 0; i < filesObj.length; i++) {
                totalSize += filesObj[i].size;
                if (filesObj[i].name.substr(-4) != '.csv') checkType = false;
            }
            if (totalSize > 10485760 || !checkType) {
                let messTxt = '最大容量は10M以下入力してください';
                if (!checkType) messTxt = '選択したファイルのフォーマットが不正です。';

                $('#form-create_csv .error-msg_file').html(messTxt);
                $('.form-custom_upload_content').addClass('error-file_csv');
                $('#upload_csv').val(null);
                return;
            }
            for (let i = 0; i < filesObj.length; i++) {
                if (nameCsv.includes(filesObj[i].name)) continue;
                fileCsv.push(filesObj[i]);
                nameCsv.push(filesObj[i].name);
            }

            $(".form-custom_upload_list").html('');
            for (let i = 0; i < fileCsv.length; i++) {
                if (typeof fileCsv[i] === 'undefined') continue;

                let boxPreview = '<div class="card-custom mb-2 main_table collapse" id="upload-content_' + i + '">';
                boxPreview += '<p class="mb-20">CSVファイルの先頭5行を表示しています</p><div class="table-responsive">';
                boxPreview += '<table class="table table-bordered table-custom-3 text-center mb-0 form-custom table-preview_csv">';
                boxPreview += '<thead><tr><th scope="col" class="main_table_action large">データ<br>開始位置</th>';
                boxPreview += '</tr></thead></table></div><input type="hidden" name="title['+i+']"/></div>';
                if (fileCsv[i].name != '') {
                    let uploadList = '<div class="box-upload_item" data-key="' + i + '">';
                    uploadList += '<div class="form-custom_upload_item" data-toggle="collapse" data-target="#upload-content_' + i + '">';
                    uploadList += '<span class="name-file_csv">' + fileCsv[i].name + '</span><span class="show-preview_csv"></span>';
                    uploadList += '<p class="error-msg_csv"></p>';
                    uploadList += '<span class="close"></span></div>' + boxPreview + '</div>';
                    $(".form-custom_upload_list").append(uploadList);
                }
            }
        }
    }

    $(document).on('click', '.form-custom_upload_item', function (e) {
        e.preventDefault();
        var boxKey = $(this).closest('.box-upload_item');
        var key = boxKey.data('key');
        $(this).find('span.show-preview_csv').html('');
        if ($(this).attr('aria-expanded') === 'true') {
            $(this).find('span.show-preview_csv').html('表示中');
        }
        if (boxKey.find('tbody').length) return;

        if (typeof (FileReader) != "undefined") {
            let reader = new FileReader();
            function readFileCsv(index) {
                if (index >= fileCsv.length || index != key) return;
                let file = fileCsv[index];
                reader.onload = function (e) {
                    let tbody = document.createElement("tbody");
                    let rows = e.target.result.split("\n");
                    let boxSelect = '';
                    for (let i = 0; i < 5; i++) {
                        if (typeof rows[i] === 'undefined') continue;
                        let cells = rows[i].split(",");
                        if (cells.length > 1) {
                            let row = tbody.insertRow(-1);
                            for (let j = -1; j < cells.length; j++) {
                                let cell = row.insertCell(-1);

                                if (i == 0 && j != (cells.length - 1)) boxSelect += '<th scope="col"><div class="form-custom_select">' +
                                    '<select name="selectTitle['+key+']['+(j+1)+']" class="selectType select-title_csv" title="選択してください">' +
                                    '<option value="">選択してください</option>' +
                                    '<option value="1">氏名</option>' +
                                    '<option value="2">フリガナ</option>' +
                                    '<option value="3">企業名</option>' +
                                    '<option value="4">部署名</option>' +
                                    '<option value="5">役職名</option>' +
                                    '<option value="6">電話番号</option>' +
                                    '<option value="7">メールアドレス</option>' +
                                    '<option value="8">郵便番号</option>' +
                                    '<option value="9">住所</option>' +
                                    '<option value="10">備考</option>' +
                                    '</select></div></th>';

                                if (j == -1) {
                                    cell.innerHTML = '<div class="form-check pl-2">\n' +
                                        '                        <input class="form-check-input large" type="radio" name="start['+key+']"\n' +
                                        '                               id="registerEmail"' + (i == 0 ? 'disabled' : ' ') + (i == 1 ? 'checked' : '' ) + ' value="' + (i + 1) + '">\n' +
                                        '                    </div>'
                                } else cell.innerHTML = cells[j].replaceAll('"', '');
                            }
                        }
                    }
                    let dvCSV = boxKey.find('table.table');
                    dvCSV.innerHTML = "";
                    dvCSV.find('thead tr').append(boxSelect);
                    dvCSV.append(tbody);
                    $('.selectType').selectpicker();
                    readFileCsv(index + 1);
                }
                reader.readAsText(file, 'SJIS');
            }

            readFileCsv(key);
        }
    });

    $(document).on('change', 'select.select-title_csv', function (e) {
        e.preventDefault();
        let prev = $(this).data('prev');
        let valSelect = $(this).val();
        $(this).data('prev', valSelect);
        let nameSelect = $(this).attr('name');
        let boxItem = $(this).closest('.table-preview_csv');

        $(boxItem).find('select.select-title_csv').each(function (key, elm) {
            if ($(elm).attr('name') == nameSelect) return;
            $(elm).find('option').each(function (k, v) {
                if (typeof prev !== 'undefined') $(elm).find('option[value="'+prev+'"]').prop('disabled', false);
                if ($(v).val() != '' && $(v).val() == valSelect) $(elm).find('option[value="'+valSelect+'"]').prop('disabled', true);
            });
        });
        $('.selectType').selectpicker('refresh');
    });

    $(document).on('submit', '#form-create_csv', function (e) {
        e.preventDefault();
        $('.form-custom_upload_content').removeClass('error-file_csv');
        $('#upload_csv').val(null);
        var formData = new FormData(this);
        for (let i = 0; i < fileCsv.length; i++) {
            if (typeof fileCsv[i] === 'undefined') continue;
            formData.append('file_upload[]', fileCsv[i]);
        }
        $.ajax({
            url: $(this).attr("action"),
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: showLoad(),
            success: function (data) {
                hideLoad();
                $('.box-import_csv').hide();
                $('.box-preview_csv').show();
                $('.box-preview_csv').html('');
                $('.box-preview_csv').append(data);
            },
            error: function (data) {
                hideLoad();
                let errors = data.responseJSON.errors;
                $('#form-create_csv .error-msg_file').html('');
                $('#form-create_csv .error-msg_checkMail').html('');
                if (errors.file_upload) {
                    $('#form-create_csv .error-msg_file').html(errors.file_upload[0]);
                    $('.form-custom_upload_content').addClass('error-file_csv');
                }
                if (errors.duplicateEmail) $('#form-create_csv .error-msg_checkMail').html(errors.duplicateEmail[0]);
                $('.box-upload_item').each(function (k, v) {
                    let k_title = $(v).data('key');
                    $(v).find('.error-msg_csv').html('');
                    if (errors['name.' + k_title]) $(v).find('.error-msg_csv').append('<small class="font-family-w3 color-red">' + (errors['name.' + k_title])[0] + '</small><br/>');
                    if (errors['name_furi.' + k_title]) $(v).find('.error-msg_csv').append('<small class="font-family-w3 color-red">' + (errors['name_furi.' + k_title])[0] + '</small><br/>');
                    if (errors['tel.' + k_title]) $(v).find('.error-msg_csv').append('<small class="font-family-w3 color-red">' + (errors['tel.' + k_title])[0] + '</small><br/>');
                    if (errors['email.' + k_title]) $(v).find('.error-msg_csv').append('<small class="font-family-w3 color-red">' + (errors['email.' + k_title])[0] + '</small><br/>');
                    if (errors['post_code.' + k_title]) $(v).find('.error-msg_csv').append('<small class="font-family-w3 color-red">' + (errors['post_code.' + k_title])[0] + '</small><br/>');
                    if (errors['address.' + k_title]) $(v).find('.error-msg_csv').append('<small class="font-family-w3 color-red">' + (errors['address.' + k_title])[0] + '</small>');
                })
            }
        });
    });

    $(document).on('submit', '#form-import_csv', function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr("action"),
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: showLoad(),
            success: function (data) {
                hideLoad();
                if (data.status) {
                    window.location = data.route;
                } else {
                    $('.box-preview_csv').show();
                    $('.box-preview_csv').html('');
                    $('.box-preview_csv').append(data);
                }
            },
        });
    });

    $(document).on('click', '.delete-error_csv', function (e) {
        e.preventDefault();
        let key = $(this).data('key');
        let item = $(this).data('item');
        if (typeof key === 'undefined' || item == '') return;
        $.ajax({
            url: '/customer/remove-item-csv',
            type: 'GET',
            data: {key: key, item: item},
            beforeSend: showLoad(),
            success: function (data) {
                hideLoad();
                if (!data.status) return;
            },
        });
        $(this).closest('tr').remove();
    });

    $(document).on('click', '#back-import_csv', function () {
        $('.box-import_csv').show();
        $('.box-preview_csv').hide();
    })

    $(document).on('click', '.form-custom_upload_item .close', function (event) {
        event.stopPropagation();
        let key = $(this).closest('.box-upload_item').data('key');
        $('#upload_csv').val(null);
        if (fileCsv.length == 1) {
            fileCsv = [];
            nameCsv = [];
        } else {
            for (let i = 0; i < fileCsv.length; ++i) {
                if (i == parseInt(key)) {
                    delete fileCsv[i];
                    nameCsv.splice(i, 1);
                }
            }
        }

        $(this).closest('.box-upload_item').remove();
    });
});

function sortTableAnswer() {
    $(function () {
        $('.sortable_answer').sortable();
    })
}

// Scroll question
function scrollQuestion(key) {
    var boxNew = $('div[data-key="' + key + '"]').offset().top;
    window.scrollTo(0, boxNew);
}

// handle form anketo
function handleFomQuestion() {
    addFormAnswer();
    // Change type question in question
    $(document).on('change', 'select.change_value_type', function (e) {
        e.preventDefault();
        var selectThis = $(this);
        var type = selectThis.val();
        var key = selectThis.closest('div.parent-question').data('key');
        if (type) {
            $.ajax({
                url: '/survey/add-form',
                method: 'GET',
                data: {type: type, key: key},
                beforeSend: showLoad(),
                success: function (data) {
                    hideLoad();
                    selectThis.closest('div.parent-question').find('div.form-answers').html(data);
                    sortTableAnswer();
                }
            });
        }
    });

    $(document).on('click', '.copy-form-question', function (e) {
        e.preventDefault();
        var keyCurrent = $(this).closest('div.parent-question').data('key');
        var numberForm = $('input#length-key').val();
        $.ajax({
            type: 'POST',
            url: '/survey/copy-form',
            data: $('#form-create-survey').serialize() + '&keyCurrent=' + keyCurrent + '&number=' + numberForm,
            beforeSend: showLoad(),
            success: function (data) {
                hideLoad();
                var newKey = parseInt(numberForm) + 1;
                $('div.all-form-question').append(data);
                $('input#length-key').val(newKey);
                $('.selectType').selectpicker();
                scrollQuestion(newKey);
                sortTableAnswer();
            },
            error: function (data) {
            }
        });
    });

    $(document).on('click', '.delete-form-question', function () {
        $(this).closest('div.card-custom-w').remove();
    });

    $(document).on('change', '.switch-question_required', function (e) {
        e.preventDefault();
        let requiredQuestion = $(this).is(':checked');
        $(this).closest('div.parent-question').find('label.font-family-w6').html('質問内容');
        if (requiredQuestion)
            $(this).closest('div.parent-question').find('label.font-family-w6').append('<span class="card-table_label font-family-w3 ml-1">必須</span>');
    });
}

// Error question
function showErrorSurvey(errors) {
    $('.error-mgs-title').html('');
    $('.input-title-survey').removeClass('border-error');
    $('.error-mgs-des').html('');
    $('.description-survey').removeClass('border-error');
    if (errors.title) {
        $('.error-mgs-title').html(errors.title[0]);
        $('.input-title-survey').addClass('border-error');
    }
    if (errors.descriptions) {
        $('.error-mgs-des').html(errors.descriptions[0]);
        $('.description-survey').addClass('border-error');
    }

    $('.parent-question').each(function (key, elm) {
        var key_q = $(this).data('key');
        $(this).find('.error-mgs-content').html('');
        $(this).find('input.input-question').removeClass('border-error');
        if (errors['question.' + key_q + '.content']) {
            $(this).find('.error-mgs-content').html((errors['question.' + key_q + '.content'])[0]);
            $(this).find('input.input-question').addClass('border-error');
        }
        $(elm).find('.box-items-answer').each(function (k, v) {
            $(this).find('small.error-mgs-answer').html('');
            $(this).find('input.input-answer').removeClass('border-error');
            if (errors['question.' + key_q + '.answer.' + k]) {
                $(this).find('small.error-mgs-answer').html((errors['question.' + key_q + '.answer.' + k])[0]);
                $(this).find('input.input-answer').addClass('border-error');
            }
        })
    })
}

// Add form answer in question
function addFormAnswer() {
    $(document).on('click', '.add-content-radio', function (e) {
        var key = $(this).closest('div.parent-question').data('key');
        var number = $(this).closest('.box-question-radio').find('button.align-items-center').length;
        var html = '<div class="box-items-answer"><span class="icon-drag move-drag-answer"></span><button class="main_card_btn d-flex align-items-center pb-2 px-4" disabled><div class="form-check">';
        html += '<input class="form-check-input large" type="radio" name="exampleRadios"></div>';
        html += '<div class="row row-custom w-100"><div class="col-5 col-custom">';
        html += '<input type="text" name="question[' + key + '][answer][]" maxlength="255" class="form-control input-answer" placeholder="選択肢' + (number + 1) + '"/></div>';
        html += '<div class="col-custom text-answer-error"><small class="font-family-w3 color-red error-mgs-answer"></small></div>';
        html += '</div><span class="close"></span></button></div>';

        $(this).closest('.box-question-radio').find('div.box-body-radio').append(html);
        $(this).closest('.box-question-radio').find('.move-drag-answer.drag-first').show();
    });

    $(document).on('click', '.add-content-checkbox', function (e) {
        var key = $(this).closest('div.parent-question').data('key');
        var number = $(this).closest('.box-question-checkbox').find('button.align-items-center').length;
        var html = '<div class="box-items-answer"><span class="icon-drag move-drag-answer"></span><button class="main_card_btn d-flex align-items-center pb-2 px-4" disabled><div class="form-check">';
        html += '<input class="form-check-input large" type="checkbox" name="exampleRadios"></div>';
        html += '<div class="row row-custom w-100"><div class="col-5 col-custom">';
        html += '<input type="text" name="question[' + key + '][answer][]" maxlength="255" class="form-control input-answer" placeholder="選択肢' + (number + 1) + '"/></div>';
        html += '<div class="col-custom text-answer-error"><small class="font-family-w3 color-red error-mgs-answer"></small></div>';
        html += '</div><span class="close"></span></button></div>';

        $(this).closest('.box-question-checkbox').find('div.box-body-checkbox').append(html);
        $(this).closest('.box-question-checkbox').find('.move-drag-answer.drag-first').show();
    });

    $(document).on('click', '.add-content-select', function () {
        var key = $(this).closest('div.parent-question').data('key');
        var number = $(this).closest('.box-question-select').find('li.mb-2').length;
        var html = '<li class="mb-2 box-items-answer">';
        html += '<div class="row row-custom w-100"><div class="col-5 col-custom">';
        html += '<input type="text" name="question[' + key + '][answer][]" maxlength="255" class="form-control input-answer" placeholder="選択肢' + (number + 1) + '"/></div>';
        html += '<div class="col-custom text-answer-error"><small class="font-family-w3 color-red error-mgs-answer"></small></div>';
        html += '</div><span class="close"></span></li>';

        $(this).closest('.box-question-select').find('ol.card-custom_list').append(html);
    });

    $(document).on('click', '.add-other-radio', function (e) {
        var key = $(this).closest('div.parent-question').data('key');
        var html = '<div class="box-items-answer"><div class="position-relative"><span class="icon-drag move-drag-answer"></span><button class="main_card_btn d-flex align-items-center pb-2 px-4" disabled><div class="form-check">';
        html += '<input class="form-check-input large" type="radio" name="exampleRadios"></div>';
        html += '<div class="row row-custom w-100"><div class="col-5 col-custom col-answer">';
        html += 'その他<input type="hidden" name="question[' + key + '][answer][]" class="input-answer" value="is_other_answer"></div>';
        html += '</div><span class="close"></span></button></div></div>';

        $(this).closest('.box-question-radio').find('div.box-body-radio').append(html);
        $(this).closest('.box-question-radio').find('.move-drag-answer.drag-first').show();
        $(this).prop('disabled', true);
    })

    $(document).on('click', '.add-other-checkbox', function (e) {
        var key = $(this).closest('div.parent-question').data('key');
        var html = '<div class="box-items-answer"><div class="position-relative"><span class="icon-drag move-drag-answer"></span><button class="main_card_btn d-flex align-items-center pb-2 px-4" disabled><div class="form-check">';
        html += '<input class="form-check-input large" type="checkbox" name="exampleRadios"></div>';
        html += '<div class="row row-custom w-100"><div class="col-5 col-custom col-answer">';
        html += 'その他<input type="hidden" name="question[' + key + '][answer][]" class="input-answer" value="is_other_answer"></div>';
        html += '</div><span class="close"></span></button></div></div>';

        $(this).closest('.box-question-checkbox').find('div.box-body-checkbox').append(html);
        $(this).closest('.box-question-checkbox').find('.move-drag-answer.drag-first').show();
        $(this).prop('disabled', true);
    })

    $(document).on('click', '.box-items-answer .close', function (e) {
        e.preventDefault();
        var boxAnswer = $(this).closest('div.parent-question');
        let checkOther = false;
        $(this).closest('.box-items-answer').remove();
        var length = boxAnswer.find('.box-items-answer').length;
        if (length == 1) boxAnswer.find('.move-drag-answer.drag-first').hide();

        boxAnswer.find('.box-items-answer').each(function (k, v) {
            $(v).find('input.input-answer').attr('placeholder', '選択肢' + (k + 1));
            if ($(v).find('input.input-answer').val() == 'is_other_answer') checkOther = true;
        });
        if (!checkOther) boxAnswer.find('.other-answer').prop('disabled', false);
    });

    $(document).on('mouseleave', '.move-drag-answer', function (e) {
        e.preventDefault();
        var boxAnswer = $(this).closest('div.parent-question');
        boxAnswer.find('.box-items-answer').each(function (k, v) {
            $(v).find('.close').remove();
            $(v).find('input.input-answer').attr('placeholder', '選択肢' + (k + 1));
            if (k > 0) $(v).find('button.align-items-center').append('<span class="close"></span>');
        });
    });
}
