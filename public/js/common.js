$(document).ready(function () {
    $(document).on('submit', '#frm', function (e) {
        var button = $('#btn-submit');
        button.prop('disabled', true);
        var valid = true;
        if (!valid) {
            e.preventDefault();
            button.prop('disabled', false);
        }
    });

    //select type
    $('.selectType').selectpicker();
    // hide messenge
    $("div.alert").delay(3000).slideUp();
});

function showLoad() {
    $('.loadingspinnerbackdop').show();
}

function hideLoad() {
    $('.loadingspinnerbackdop').hide();
}

function clearIcon(sort_name) {
    $('.sort-heading').each(function() {
        if ($(this).attr('id') != sort_name) $(this).data('sort', 'asc');
        $(this).attr('src', '/images/icons/sort.svg');
    });
}

function check_cookie() {
    var cookie = `${document.cookie}`
    if (!cookie){
        window.location.reload()
    }
}

$(document).on('click', 'body', function() {
    check_cookie()
})
